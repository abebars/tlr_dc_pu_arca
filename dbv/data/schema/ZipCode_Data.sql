CREATE TABLE `ZipCode_Data` (
  `ZipCode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Country` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `Latitude` double(10,6) NOT NULL DEFAULT '0.000000',
  `Longitude` double(10,6) NOT NULL DEFAULT '0.000000',
  KEY `ZipCode` (`ZipCode`),
  KEY `Country` (`Country`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci