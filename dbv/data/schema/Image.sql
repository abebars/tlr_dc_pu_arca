CREATE TABLE `Image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` set('JPG','GIF','SWF','PNG') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'JPG',
  `width` smallint(6) NOT NULL DEFAULT '0',
  `height` smallint(6) NOT NULL DEFAULT '0',
  `prefix` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci