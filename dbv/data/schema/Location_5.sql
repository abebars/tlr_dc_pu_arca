CREATE TABLE `Location_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_4` int(11) NOT NULL DEFAULT '0',
  `location_3` int(11) NOT NULL DEFAULT '0',
  `location_2` int(11) NOT NULL DEFAULT '0',
  `location_1` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `abbreviation` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `city_id` (`location_4`),
  KEY `friendly_url` (`friendly_url`),
  KEY `name` (`name`),
  KEY `location_4_level` (`location_1`,`location_2`,`location_3`,`location_4`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci