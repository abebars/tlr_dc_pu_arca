CREATE TABLE `Control_Export_MailApp` (
  `domain_id` int(11) NOT NULL,
  `scheduled` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `running` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `last_run_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_exportlog` int(11) NOT NULL,
  PRIMARY KEY (`domain_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci