CREATE TABLE `Cron_Log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain_id` int(11) NOT NULL,
  `cron` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `history` text COLLATE utf8_unicode_ci NOT NULL,
  `finished` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `time` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci