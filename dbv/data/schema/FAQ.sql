CREATE TABLE `FAQ` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sitemgr` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `member` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `frontend` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `answer` text COLLATE utf8_unicode_ci NOT NULL,
  `editable` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci