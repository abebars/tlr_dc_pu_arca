CREATE TABLE `PackageModules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `domain_id` int(11) DEFAULT NULL,
  `parent_domain_id` int(11) NOT NULL,
  `module` varchar(50) DEFAULT NULL,
  `module_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `module_id` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `domain_id` (`domain_id`),
  KEY `fk_PackageModules_Package1` (`package_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8