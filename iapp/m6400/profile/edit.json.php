<?

/* ==================================================================*\
  ######################################################################
  #                                                                    #
  # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
  #                                                                    #
  # This file may not be redistributed in whole or part.               #
  # eDirectory is licensed on a per-domain basis.                      #
  #                                                                    #
  # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
  #                                                                    #
  # http://www.edirectory.com | http://www.edirectory.com/license.html #
  ######################################################################
  \*================================================================== */

# ----------------------------------------------------------------------------------------------------
# LOAD CONFIG
# ----------------------------------------------------------------------------------------------------
include("../conf/configuration.inc.php");

# ----------------------------------------------------------------------------------------------------
# VALIDATION
# ----------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------
# HEADER
# ----------------------------------------------------------------------------------------------------

function escapeJsonString($value) {
    $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
    $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
    $result = str_replace($escapers, $replacements, $value);
    return $result;
}

;


header("Content-type: application/json");
$backButton = false;
$mapresultsButton = false;
$listresultsButton = false;
$backButtonLink = "";
$headerTitle = LANG_M_LISTINGHOME;
$languageButton = false;
$homeButton = true;
$searchButton = false;
$searchButtonLink = "";
if ($_SERVER['REQUEST_METHOD'] == "GET") {

    if ($_GET["account_id"] && is_numeric($_GET["account_id"])) {

        $arr_category1 = array();
        $ProfileContact = new AccountProfileContact($_GET["account_id"]);
        if (!$nonprofit_categories)
            if ($ProfileContact)
                $nonprofit_categories = $ProfileContact->getNonProfitCategories(false, false, $_GET["account_id"], true, true);
        if ($nonprofit_categories) {
            for ($i = 0; $i < count($nonprofit_categories); $i++) {
                $arr_category1[$i]["causename"] = $nonprofit_categories[$i]->getString("title");
                $arr_category1[$i]["value"] = $nonprofit_categories[$i]->getNumber("id");
            }
        }

        if (is_array($arr_category1)) {

            header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", FALSE);
            header("Pragma: no-cache");
            header("Content-Type: application/json; charset=" . EDIR_CHARSET, TRUE);

            echo json_encode($arr_category1);
        }
    } else {
        $error = "Please enter a valid account_id";
        header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", FALSE);
        header("Pragma: no-cache");
        header("Content-Type: application/json; charset=" . EDIR_CHARSET, TRUE);
        echo json_encode($error);
    }
} else {
  
    if ($_POST["account_id"] && is_numeric($_POST["account_id"])) {

        $ProfileContact = new AccountProfileContact($_POST["account_id"]);
        $return_categories_nonprofit = $_POST["return_categories_nonprofit"];
      
        $return_categories_array1 = explode(",", $return_categories_nonprofit);
        $return_categories_array1 = array_unique($return_categories_array1);
// echo $_POST["account_id"]; exit;
        if (count($return_categories_array1) > USER_MAX_CAUSE_ALLOWED)
            $errors = system_showText(LANG_MSG_MAX_OF_CATEGORIES_1) . " " . USER_MAX_CAUSE_ALLOWED . " " . system_showText(LANG_MSG_MAX_OF_CAUSE_2);
   
//        $message_account = str_replace("&#149;&nbsp;", "", $errors);
        $message_account = str_replace("<br />", "\n", $errors);
       if(!$message_account){
             if($return_categories_nonprofit!=0){
             $ProfileContact->setCategoriesNonprofit($return_categories_array1, $_POST["account_id"]);

             }
        header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", FALSE);
        header("Pragma: no-cache");
        header("Content-Type: application/json; charset=" . EDIR_CHARSET, TRUE);
        echo json_encode($return_categories_array1);
       }else{
            
        header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", FALSE);
        header("Pragma: no-cache");
        header("Content-Type: application/json; charset=" . EDIR_CHARSET, TRUE);
        echo json_encode($errors);
       }
     
       
}

       }
?>