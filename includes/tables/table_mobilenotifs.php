<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/tables/table_mobilenotifs.php
	# ----------------------------------------------------------------------------------------------------

?>
    <ul class="standard-iconDESCRIPTION">
        <li class="edit-icon"><?=system_showText(LANG_LABEL_EDIT);?></li>
        <li class="delete-icon"><?=system_showText(LANG_LABEL_DELETE);?></li>
    </ul>
       
    <table border="0" cellpadding="0" cellspacing="0" class="standard-tableTOPBLUE">

        <tr>
            <th style="width: auto;">
                <span style="width: auto;"><?=system_showText(LANG_SITEMGR_MOBILE_NOTIFTITLE);?></span>
            </th>

            <th>
                <span><?=system_showText(LANG_SITEMGR_MOBILE_EXPIRY);?></span>
            </th>

            <th style="width: 100px;">
                <span><?=system_showText(LANG_LABEL_STATUS);?></span>
            </th>

            <th style="width: 2%">
                <?=system_showText(LANG_LABEL_OPTIONS)?>
            </th>

        </tr>

        <? if ($notifs) foreach ($notifs as $notif) { $id = $notif->getNumber("id"); ?>

            <tr>
                <td>
                    <a title="<?=$notif->getString("title");?>" href="<?=$url_redirect?>/notification.php?id=<?=$id?>&screen=<?=$screen?>&letter=<?=$letter?><?=(($url_search_params) ? "&$url_search_params" : "")?>" class="link-table">
                        <?=$notif->getString("title", true, 40);?>
                    </a>
                </td>
                <td>
                    <span title="<?=format_date($notif->getString("expiration_date"))?>" style="cursor:default"><?=format_date($notif->getString("expiration_date"));?></span>
                </td>
                <td>
                    <?=$status->getStatusWithStyle($notif->getString("status"));?>
                </td>
                <td nowrap="nowrap">
                    <a href="<?=$url_redirect?>/notification.php?id=<?=$id?>&screen=<?=$screen?>&letter=<?=$letter?><?=(($url_search_params) ? "&$url_search_params" : "")?>" class="link-table">
                        <img src="<?=DEFAULT_URL?>/images/bt_edit.gif" border="0" alt="<?=system_showText(LANG_SITEMGR_MOBILE_NOTIF_CLICKEDIT)?>" title="<?=system_showText(LANG_SITEMGR_MOBILE_NOTIF_CLICKEDIT)?>" />
                    </a>

                    <a href="javascript:void(0)" onclick="dialogBox('confirm', '<?=system_showText(LANG_SITEMGR_MSGAREYOUSURE);?>', <?=$id?>, 'Notif_post', '', '<?=system_showText(LANG_SITEMGR_OK);?>', '<?=system_showText(LANG_SITEMGR_CANCEL);?>');" class="link-table">
                        <img src="<?=DEFAULT_URL?>/images/bt_delete.gif" border="0" alt="<?=system_showText(LANG_SITEMGR_MOBILE_NOTIF_CLICKDELETE)?>" title="<?=system_showText(LANG_SITEMGR_MOBILE_NOTIF_CLICKDELETE)?>" />
                    </a>
                </td>
            </tr>

        <? } ?>

    </table>

    <ul class="standard-iconDESCRIPTION">
        <li class="edit-icon"><?=system_showText(LANG_LABEL_EDIT);?></li>
        <li class="delete-icon"><?=system_showText(LANG_LABEL_DELETE);?></li>
    </ul>