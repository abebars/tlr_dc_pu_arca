<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/tables/table_mailapplist.php
	# ----------------------------------------------------------------------------------------------------

?>
    <ul class="standard-iconDESCRIPTION">
        <li class="download-icon"><?=system_showText(LANG_LABEL_DOWNLOAD);?></li>
        <li class="delete-icon"><?=system_showText(LANG_LABEL_DELETE);?></li>
    </ul>
       
    <table border="0" cellpadding="0" cellspacing="0" class="standard-tableTOPBLUE">

        <tr>
            <th style="width: auto;">
                <span style="width: auto;"><?=system_showText(LANG_LABEL_TITLE);?></span>
            </th>

            <th>
                <span><?=system_showText(LANG_SITEMGR_DATECREATED);?></span>
            </th>

            <th style="width: 100px;">
                <span><?=system_showText(LANG_LABEL_STATUS);?></span>
            </th>
            
            <th style="width: 100px;">
                <span><?=system_showText(LANG_SITEMGR_MAILAPP_PROGRESS);?></span>
            </th>

            <th style="width: 2%">
                <?=system_showText(LANG_LABEL_OPTIONS)?>
            </th>

        </tr>

        <?
        $runAjax = false;
        if ($mailappLists){
            
            $statusObj = new ImportStatus();
            
            foreach ($mailappLists as $mailappList) { 
                $id = $mailappList->getNumber("id"); 
                $status = $mailappList->getString("status");
                $filePath = EDIRECTORY_ROOT."/custom/domain_".SELECTED_DOMAIN_ID."/export_files/".$mailappList->getString("filename");
                
                if ($status == "F" && file_exists($filePath)) {
                    $src_download = "bt_download.gif";
                    $onclick_download = "linkRedirect('arcamailerexport.php?action=downFile&id=$id');";
                    $cursor_download = "pointer;";
                } else {
                    $src_download = "bt_download_off.gif";
                    $onclick_download = "javascript: void(0);";
                    $cursor_download = "default;";
                }
                
                if ($status != "R") {
                    $src_delete = "bt_delete.gif";
                    $onclick_delete = "dialogBox('confirm', '".system_showText(LANG_SITEMGR_MSGAREYOUSURE)."', $id, 'MailList_post', '', '".system_showText(LANG_SITEMGR_OK)."', '".system_showText(LANG_SITEMGR_CANCEL)."');";
                    $cursor_delete = "pointer;";
                } else {
                    $src_delete = "bt_delete_off.gif";
                    $onclick_delete = "javascript: void(0);";
                    $cursor_delete = "default;";
                }
                
                if ($status != "F") {
                    $runAjax = true;
                }
                ?>
                <tr>
                    <td><?=$mailappList->getString("title", true, 40);?></td>
                    
                    <td>
                        <span title="<?=format_date($mailappList->getString("date"))?>" style="cursor:default">
                        <?=format_date($mailappList->getString("date"));?>
                        </span>
                    </td>
                    
                    <td id="tdprogress_<?=$id?>">
                        <?
                        echo $statusObj->getStatusWithStyle(($status == "P" ? "Q" : $status), $id);
                        if ($status == "E") {
                            echo " <img src=\"".DEFAULT_URL."/images/icon_interrogation.gif\" alt=\"?\" title=\"".system_showText(LANG_SITEMGR_MAILAPP_ERROR)."\" border=\"0\" />";
                        }
                        ?>
                    </td>
                    
                    <td>
                        <span id="<?="progress_".$id?>"><?=$mailappList->getNumber("progress");?>%</span>
                    </td>
                    
                    <td nowrap="nowrap">                            
                        <img id="img_download_<?=$id?>" src="<?=DEFAULT_URL;?>/images/<?=$src_download?>" border="0" onclick="<?=$onclick_download?>" style="cursor: <?=$cursor_download?>" alt="<?=system_showText(LANG_MSG_CLICK_TO_DOWNLOAD_THIS_FILE)?>" title="<?=system_showText(LANG_MSG_CLICK_TO_DOWNLOAD_THIS_FILE)?>" />
                        <img id="img_delete_<?=$id?>" src="<?=DEFAULT_URL;?>/images/<?=$src_delete?>" border="0" onclick="<?=$onclick_delete?>" style="cursor: <?=$cursor_delete?>" alt="<?=system_showText(LANG_SITEMGR_MAILAPP_CLICK_DELETE)?>" title="<?=system_showText(LANG_SITEMGR_MAILAPP_CLICK_DELETE)?>" />
                    </td>
                </tr>

                <?
            }
        } 
        ?>
        
    </table>

    <ul class="standard-iconDESCRIPTION">
        <li class="download-icon"><?=system_showText(LANG_LABEL_DOWNLOAD);?></li>
        <li class="delete-icon"><?=system_showText(LANG_LABEL_DELETE);?></li>
    </ul>