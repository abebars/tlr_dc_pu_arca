<?
/* ==================================================================*\
  ######################################################################
  #                                                                    #
  # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
  #                                                                    #
  # This file may not be redistributed in whole or part.               #
  # eDirectory is licensed on a per-domain basis.                      #
  #                                                                    #
  # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
  #                                                                    #
  # http://www.edirectory.com | http://www.edirectory.com/license.html #
  ######################################################################
  \*================================================================== */

# ----------------------------------------------------------------------------------------------------
# * FILE: /includes/views/view_listing_summary.php
# ----------------------------------------------------------------------------------------------------
if ($mapNumber != null) {
    include_once(EDIRECTORY_ROOT . "/classes/facebook/base_facebook.php");
    include_once(EDIRECTORY_ROOT . "/classes/facebook/facebook.php");
}

if (is_object($listing)) {
    $is_object = true;
    $listingAux = $listing;
    $listing = $listing->data_in_array;
}

//Get fields according to level
unset($array_fields);
$array_fields = system_getFormFields("Listing", $listing['level']);

$detailLink = "";

if ($levelObj->getDetail(htmlspecialchars($listing["level"])) == "y") {
    if ($isMobileSummary) {
        $detailLink = "" . MOBILE_DEFAULT_URL . "/" . LISTING_FEATURE_FOLDER . "/" . htmlspecialchars($listing["friendly_url"]) . ".html";
    } else {
        $detailLink = "" . LISTING_DEFAULT_URL . "/" . htmlspecialchars($listing["friendly_url"]) . ".html";
    }
}
$listingtemplate_friendly_url = htmlspecialchars($listing["friendly_url"]);
$listingtemplate_icon_navbar = "";

if (!$isMobileSummary) {
    include(EDIRECTORY_ROOT . "/includes/views/icon_listing.php");
    $listingtemplate_icon_navbar = $icon_navbar;
    $icon_navbar = "";

    $listingtemplate_claim = "";
    if (CLAIM_FEATURE == "on") {
        if (!htmlspecialchars($listing["account_id"])) {
            if (htmlspecialchars($listing["claim_disable"]) == "n") {
                customtext_get("claim_textlink", $claim_textlink);
                if ($claim_textlink) {
                    $claim_textlink_string = $claim_textlink;
                }
                $listingtemplate_claim = "<a href=\"" . $claim_link . "\" " . $claim_style . ">" . $claim_textlink_string . "</a>";
            }
        }
    }

    $listingtemplate_video_snippet_width = "";
    $listingtemplate_video_snippet_height = "";
    $listingtemplate_video_snippet = "";
    if (htmlspecialchars($listing["video_snippet"]) && (is_array($array_fields) && in_array("video", $array_fields))) {
        $listingtemplate_video_snippet_width = IMAGE_LISTING_THUMB_WIDTH;
        $listingtemplate_video_snippet_height = IMAGE_LISTING_THUMB_HEIGHT;
        $listingtemplate_video_snippet = system_getVideoSnippetCode($listing["video_snippet"], IMAGE_LISTING_THUMB_WIDTH, IMAGE_LISTING_THUMB_HEIGHT);
    }
}

$listingtemplate_image = "";

if (is_array($array_fields) && in_array("main_image", $array_fields)) {
    unset($arrImage);

    $data = mysql_query("SELECT promotion_id  FROM Listing where id = (" . $listing['id'] . ")");
    if ($data != null) {
        $row = mysql_fetch_assoc($data);
        $promotion_id = $row['promotion_id'];
    }
//                                    print_r($promotion_id);
    if (SELECTED_DOMAIN_ID == 1 || SELECTED_DOMAIN_ID == 2) {
        if ($promotion_id) {
            ?>
            <script>
                $(document).ready(function() {
                    $(".name-tag-deal").append('<div style="height: 0px; width: 0px; position: absolute; top: 0px; border-top: 19px solid transparent; border-bottom: 19px solid transparent; left: -3px; border-left: 14px solid rgb(255, 255, 255);"> </div>');
                });</script>
            <?php
        }
    }

    if ($tPreview) {
        $listingtemplate_image = "<span class=\"no-image\" style=\"cursor: default;\"></span>";
        if ($mapNumber != null) {
            ?>
            <?php if (SELECTED_DOMAIN_ID == 1) { ?>
                <script>
                    $(document).ready(function() {
                        $(".name-tag-deal").append('<div style="height: 0px; width: 0px; position: absolute; top: 0px; border-top: 19px solid transparent; border-bottom: 19px solid transparent; left: -3px; border-left: 14px solid rgb(255, 255, 255);"> </div>');
                    });</script>


                <?php
            }
        }
    } else {
        if ($is_object) {
            $imageObj = new Image($listing["thumb_id"]);
        } else {

            $imageObjT = new Image($listing["thumb_id"]);
            $auxPrefix = $imageObjT->prefix;
            unset($imageObjT);

            $arrImage["id"] = htmlspecialchars($listing[(THEME_USE_IMAGE_BIG ? "image_id" : "thumb_id")]);
            $arrImage["type"] = htmlspecialchars($listing["thumb_type"]);
            $arrImage["width"] = htmlspecialchars($listing["thumb_width"]);
            $arrImage["height"] = htmlspecialchars($listing["thumb_height"]);
            $arrImage["prefix"] = $auxPrefix;

            $imageObj = new Image($arrImage);
        }

        if ($imageObj->imageExists()) {
            if (($user) && ($levelObj->getDetail(htmlspecialchars($listing["level"])) == "y") || $levelObj->getActive(htmlspecialchars($listing["level"])) == 'n') {
                $listingtemplate_image .= "<a href=\"" . $detailLink . "\" " . ($isMobileSummary ? "" : "class=\"image\"") . ">";
                $listingtemplate_image .= $imageObj->getTag(THEME_RESIZE_IMAGE, IMAGE_LISTING_THUMB_WIDTH, IMAGE_LISTING_THUMB_HEIGHT, $listing["title"], THEME_RESIZE_IMAGE);
                $listingtemplate_image .= "</a>";
            } else {
                $listingtemplate_image .= $imageObj->getTag(THEME_RESIZE_IMAGE, IMAGE_LISTING_THUMB_WIDTH, IMAGE_LISTING_THUMB_HEIGHT, $listing["title"], THEME_RESIZE_IMAGE);
            }
        } elseif (!$isMobileSummary) {
            if (($user) && ($levelObj->getDetail(htmlspecialchars($listing["level"])) == "y") || $levelObj->getActive(htmlspecialchars($listing["level"])) == 'n') {
                $listingtemplate_image .= "<a href=\"" . $detailLink . "\" class=\"image\">";
                $listingtemplate_image .= "<span class=\"no-image\"></span>";
                $listingtemplate_image .= "</a>";
            } else {
                $listingtemplate_image .= "<span class=\"no-image no-link\">&nbsp;</span>";
            }
        }
    }
}

$listingtemplate_title = "";
if (($user) && ($levelObj->getDetail(htmlspecialchars($listing["level"])) == "y") || $levelObj->getActive(htmlspecialchars($listing["level"])) == 'n') {
    $listingtemplate_title = "<a href=\"" . $detailLink . "\">" . htmlspecialchars($listing["title"]) . "</a>";
} else {
    $listingtemplate_title = htmlspecialchars($listing["title"]);
}
if (zipproximity_getDistanceLabel($zip, "listing", htmlspecialchars($listing["id"]), $distance_label, true, $listing)) {
    $listingtemplate_title .= " (" . $distance_label . ")";
}
$auxOriginalTitle = htmlspecialchars($listing["title"]);

$listingtemplate_title2 = "";

$listingtemplate_complementaryinfo = "";
if ($tPreview) {

    $listingtemplate_complementaryinfo .= system_showText(LANG_IN) . " ";
    $listingtemplate_complementaryinfo .= "<a href=\"javascript:void(0);\" style=\"cursor: default;\">" . system_showText(LANG_LABEL_ADVERTISE_CATEGORY1) . "</a>";
    $listingtemplate_complementaryinfo .= ", ";
    $listingtemplate_complementaryinfo .= "<a href=\"javascript:void(0);\" style=\"cursor: default;\">" . system_showText(LANG_LABEL_ADVERTISE_CATEGORY2) . "</a>";
} else {


    if (LISTING_SCALABILITY_OPTIMIZATION == "on") {
        $listingtemplate_complementaryinfo = "<a href=\"javascript: void(0);\" " . ($user ? "onclick=\"showCategory(" . htmlspecialchars($listing["id"]) . ", 'listing', " . ($user ? true : false) . ", " . $listing["account_id"] . ")\"" : "style=\"cursor: default;\"") . ">" . system_showText(LANG_VIEWCATEGORY) . "</a>";
    } else {
        $listingtemplate_complementaryinfo = system_itemRelatedCategories(htmlspecialchars($listing["id"]), "listing", $user);
    }
}
if (SELECTED_DOMAIN_ID == 3) {

    $list = new Listing($listing["id"]);

    unset($arr_return_categories1);
    unset($return_categories_nonprofit);
    $nonprofit_categories = '';
    if ($list)
        $nonprofit_categories = $list->getNonProfitCategories(false, false, $listing["id"], true, true);

    if ($nonprofit_categories) {

        for ($i = 0; $i < count($nonprofit_categories); $i++) {
            $arr_return_categories1[] = $nonprofit_categories[$i]->getString("title");
        }
        if ($arr_return_categories1)
            $return_categories_nonprofit = implode(",", $arr_return_categories1);
    }
}


if (!$isMobileSummary) {
    $listingtemplate_designations = "";
    if (is_array($array_fields) && in_array("badges", $array_fields)) {
        include(INCLUDES_DIR . "/tables/table_choice.php");
        $listingtemplate_designations = $designations;
        $designations = "";
    }

    $listingtemplate_address = "";
    if (htmlspecialchars($listing["address"])) {
        $listingtemplate_address = nl2br(htmlspecialchars($listing["address"]));
        $listingtemplate_title2 .= ", " . $listingtemplate_address;
    }

    $listingtemplate_address2 = "";
    if (htmlspecialchars($listing["address2"])) {
        $listingtemplate_address2 = nl2br(htmlspecialchars($listing["address2"]));
    }

    $locationsToshow = system_retrieveLocationsToShow();
    $locationsParam = system_formatLocation($locationsToshow . ", z");

    $listingtemplate_location = "";
    /*
     * Location default format:
     * Street
     * City, State Zipcode
     * Country
     */
    if ($tPreview) {

        $listingtemplate_location = system_getLocationStringPreview($listing);
    } else {

        if ($is_object) {
            $listingtemplate_location = $listingAux->getLocationString($locationsParam, true);
            unset($array_location_string2);
            $array_location_string2 = array();
            if ($listingAux->getNumber("location_4")) {
                $auxLocation4 = new Location4($listingAux->getNumber("location_4"));
                $array_location_string2[] = $auxLocation4->getString("name");
            }
            if ($listingAux->getNumber("location_3")) {
                $auxLocation3 = new Location3($listingAux->getNumber("location_3"));
                $array_location_string2[] = $auxLocation3->getString("abbreviation");
            }
            $listingtemplate_title2 .= ($listingtemplate_title2 ? ", " : "") . implode(", ", $array_location_string2);
            unset($listingAux);
        } else {
            unset($locationsParam_array);
            $locationsParam_array = explode(",", $locationsParam);

            unset($array_location_string2);

            $array_location_string2 = array();

            $listingtemplate_location = system_getLocationStringPreview($listing, false);

            for ($r = 0; $r < count($locationsParam_array); $r++) {
                unset($aux_field_name);
                $field_id = trim($locationsParam_array["$r"]);
                if ($field_id == "z") {
                    $aux_field_name = "zip_code";
                } else {
                    $aux_field_name = "location_" . $field_id . "_title";
                }
                if ($aux_field_name == "location_4_title") {
                    if ($listing[$aux_field_name]) {
                        $array_location_string2[] = $listing[$aux_field_name];
                    }
                    if ($listing["location_3_abbreviation"]) {
                        $array_location_string2[] = $listing["location_3_abbreviation"];
                    }
                }
            }

            if (is_array($array_location_string2) && $array_location_string2[0]) {
                $listingtemplate_title2 .= ", " . implode(", ", $array_location_string2);
            }
        }
    }
} else {
    $listingtemplate_location = system_getItemAddressString("Listing", $listing["id"]);
}

if (USING_THEME_TEMPLATE && THEME_TEMPLATE_ID > 0 && TEMPLATE_SUMMARY_FIELDS && $listing["listingtemplate_id"] == THEME_TEMPLATE_ID) {
    $themeSummaryFields = unserialize(TEMPLATE_SUMMARY_FIELDS);

    if ($listing[$themeSummaryFields["price_field"]]) {
        $listingtemplate_title2 .= " | " . CURRENCY_SYMBOL . $listing[$themeSummaryFields["price_field"]];
    }
    $array_comp_info = array();
    if ($listing[$themeSummaryFields["bedroom_field"]]) {
        $array_comp_info[] = $listing[$themeSummaryFields["bedroom_field"]] . " " . system_showText(LANG_LABEL_TEMPLATE_BEDROOM);
    }
    if ($listing[$themeSummaryFields["bathroom_field"]]) {
        $array_comp_info[] = $listing[$themeSummaryFields["bathroom_field"]] . " " . system_showText(LANG_LABEL_TEMPLATE_BATHROOM);
    }
    if ($listing[$themeSummaryFields["squarefeet_field"]]) {
        $array_comp_info[] = $listing[$themeSummaryFields["squarefeet_field"]] . " " . system_showText(LANG_LABEL_TEMPLATE_SQUARE);
    }
    $listingtemplate_complementaryinfo2 = "";
    $listingtemplate_complementaryinfo2 = implode(" | ", $array_comp_info);
}

$listingtemplate_title2 = htmlspecialchars($listingtemplate_title2);

$listingtemplate_description = "";

if (htmlspecialchars($listing["description"]) && (is_array($array_fields) && in_array("summary_description", $array_fields))) {
    $listingtemplate_description = nl2br(htmlspecialchars($listing["description"]));
}

$listingtemplate_phone = "";
if (htmlspecialchars($listing["phone"]) && is_array($array_fields) && in_array("phone", $array_fields)) {
    if ($user) {
        if ($isMobileSummary) {
            $listingtemplate_phone .= $listing["phone"];
        } else {
            $listingtemplate_phone .= "<span id=\"phoneLink" . htmlspecialchars($listing["id"]) . "\" class=\"show-inline\"><a href=\"javascript:showPhone('" . htmlspecialchars($listing["id"]) . "','" . DEFAULT_URL . "');\">" . system_showText(LANG_LISTING_VIEWPHONE) . "</a></span>";
            $listingtemplate_phone .= "<span id=\"phoneNumber" . htmlspecialchars($listing["id"]) . "\" class=\"hide\">" . system_showTruncatedText($listing["phone"], 30) . "</span>";
        }
    } else {
        $listingtemplate_phone = system_showTruncatedText($listing["phone"], 30);
    }
}

$listingtemplate_fax = "";
if (htmlspecialchars($listing["fax"]) && (is_array($array_fields) && in_array("fax", $array_fields))) {
    if ($user) {
        $listingtemplate_fax .= "<span id=\"faxLink" . htmlspecialchars($listing["id"]) . "\" class=\"show-inline\"><a href=\"javascript:showFax('" . htmlspecialchars($listing["id"]) . "','" . DEFAULT_URL . "');\">" . system_showText(LANG_LISTING_VIEWFAX) . "</a></span>";
        $listingtemplate_fax .= "<span id=\"faxNumber" . htmlspecialchars($listing["id"]) . "\" class=\"hide\">" . system_showTruncatedText($listing["fax"], 30) . "</span>";
    } else {
        $listingtemplate_fax = system_showTruncatedText($listing["fax"], 30);
    }
}

$listingtemplate_url = "";
if (htmlspecialchars($listing["url"]) && (is_array($array_fields) && in_array("url", $array_fields))) {
    $display_url = htmlspecialchars($listing["url"]);
    if (htmlspecialchars($listing["display_url"])) {
        $display_url = htmlspecialchars($listing["display_url"]);
    }
    $display_url_title = $display_url;
    $display_url = system_showTruncatedText($display_url, 29);
    if ($user) {
        if ($isMobileSummary) {
            $listingtemplate_url = "<a href=\"" . $listing["url"] . "\" target=\"_blank\">" . $display_url . "</a>";
        } else {
            $listingtemplate_url = "<a href=\"" . DEFAULT_URL . "/listing_reports.php?report=website&amp;id=" . htmlspecialchars($listing["id"]) . "\" target=\"_blank\" title=\"$display_url_title\">" . $display_url . "</a>";
        }
    } else {
        $listingtemplate_url = "<a href=\"javascript:void(0);\" title=\"$display_url_title\" style=\"cursor:default\">" . $display_url . "</a>";
    }
}

$listingtemplate_email = "";
if (htmlspecialchars($listing["email"]) && (is_array($array_fields) && in_array("email", $array_fields))) {
    $display_email = wordwrap(htmlspecialchars($listing["email"]), 30, "<br />", true);
    if ($user) {
        $listingtemplate_email = "<a rel=\"nofollow\" href=\"" . DEFAULT_URL . "/popup/popup.php?pop_type=listing_emailform&amp;id=" . htmlspecialchars($listing["id"]) . "&amp;receiver=owner\" class=\"iframe fancy_window_tofriend\">" . system_showText(LANG_SEND_AN_EMAIL) . "</a>";
    } else {
        $listingtemplate_email = "<a href=\"javascript:void(0);\" style=\"cursor:default\">" . system_showText(LANG_SEND_AN_EMAIL) . "</a>";
    }
}

$listingtemplate_attachment_file = "";
if (htmlspecialchars($listing["attachment_file"]) && (is_array($array_fields) && in_array("attachment_file", $array_fields))) {
    if (file_exists(EXTRAFILE_DIR . "/" . $listing["attachment_file"])) {
        $listingtemplate_attachment_file .= "<p>";
        if ($user) {
            $listingtemplate_attachment_file .= "<a href=\"" . EXTRAFILE_URL . "/" . htmlspecialchars($listing["attachment_file"]) . "\" target=\"_blank\">";
        } else {
            $listingtemplate_attachment_file .= "<a href=\"javascript:void(0);\" >";
        }
        if (htmlspecialchars($listing["attachment_caption"])) {
            $listingtemplate_attachment_file .= htmlspecialchars($listing["attachment_caption"]);
        } else {
            $listingtemplate_attachment_file .= system_showText(LANG_LISTING_ATTACHMENT);
        }
        $listingtemplate_attachment_file .= "</a>";
        $listingtemplate_attachment_file .= "</p>";
    }
}

$listingtemplate_long_description = "";
if (htmlspecialchars($listing["long_description"]) && (is_array($array_fields) && in_array("long_description", $array_fields))) {
    $listingtemplate_long_description = nl2br(htmlspecialchars($listing["long_description"]));
}

$listingtemplate_hours_work = "";
if (htmlspecialchars($listing["hours_work"]) && (is_array($array_fields) && in_array("hours_of_work", $array_fields))) {
    $listingtemplate_hours_work = nl2br(htmlspecialchars($listing["hours_work"]));
}

$listingtemplate_locations = "";
if (htmlspecialchars($listing["locations"]) && (is_array($array_fields) && in_array("locations", $array_fields))) {
    $listingtemplate_locations = nl2br(htmlspecialchars($listing["locations"]));
}

$listingtemplate_price = "";
$listingtemplate_price_symbol = "";
if (THEME_LISTING_PRICE && $listing["price"] && (is_array($array_fields) && in_array("price", $array_fields))) {
    $listingtemplate_price = system_showListingPrice($listing["price"]);
    if (!$listing_price_symbol) {
        setting_get("listing_price_symbol", $listing_price_symbol);
    }
    for ($k = 0; $k < $listing["price"]; $k++) {
        $listingtemplate_price_symbol .= $listing_price_symbol;
    }
}

$listingtemplate_twilioSMS = "";
$listingtemplate_twilioCall = "";

if ($levelsWithSendPhone) { // this variable is created on /listing/results_listing.php
    if (in_array($listing["level"], $levelsWithSendPhone)) {
        /*
         * Prepare link to Twilio SMS
         */
        if ($user) {
            $listingtemplate_twilioSMS = twilio_PrepareLink("Listing", $listing["id"]);
            $twilioSMS_style = "class=\"iframe fancy_window_twilio\"";
        } else {
            $listingtemplate_twilioSMS = "javascript: void(0);";
            $twilioSMS_style = "style=\"cursor: default;\"";
        }
    }
}

if ($levelsWithClicktoCall) {  // this variable is created on /listing/results_listing.php
    if (in_array($listing["level"], $levelsWithClicktoCall) && $listing["clicktocall_number"]) {
        /*
         * Prepare link to Twilio Click to Call
         */
        if ($user) {
            $listingtemplate_twilioCall = twilio_PrepareLink("Listing", $listing["id"], true);
            $twilioCall_style = "class=\"iframe fancy_window_twilio\"";
        } else {
            $listingtemplate_twilioCall = "javascript: void(0);";
            $twilioCall_style = "style=\"cursor: default;\"";
        }
    }
}

if (!$isMobileSummary) {
    $listingtemplate_review = "";

    if ($review_enabled == "on" && $commenting_edir) {
        if ($levelsWithReview) {
            if (in_array($listing["level"], $levelsWithReview)) {
                $item_type = 'listing';
                $item_id = htmlspecialchars($listing["id"]);
                $itemObj = $listing;
                $hideReviewLabel = true;
                include(INCLUDES_DIR . "/views/view_review.php");
                $listingtemplate_review .= $item_review;
                $item_review = "";
            }
        }
    }

    $listingtemplate_checkin = "";

    include(INCLUDES_DIR . "/views/view_checkin.php");
    $listingtemplate_checkin .= $item_checkin;
    $item_checkin = "";

    $moreinfo_link = "";
    $moreinfo_label = "";
    if ($levelObj->getDetail(htmlspecialchars($listing["level"])) == "y") {
        if ($user) {
            $moreinfo_link = $detailLink;
            $moreinfo_label = system_showText(LANG_LISTING_MOREINFO) . " &raquo;";
        } else {
            $moreinfo_link = "javascript:void(0);";
            $moreinfo_label = system_showText(LANG_LISTING_MOREINFO);
        }
    }

    $listingviewtype = "summary";

    /*
     * DEAL ITEM
     */
    $listing_deal = "";
    if (($levelObj->getHasPromotion($listing["level"]) == "y") && (PROMOTION_FEATURE == 'on' && CUSTOM_PROMOTION_FEATURE == 'on')) {
        if ($tPreview) {
            $listing_deal = "<div class=\"deal-tag\">" . CURRENCY_SYMBOL . "90</div>";
            $listing_deal .= "<h4><a href=\"javascript:void(0);\" style=\"cursor: default;\">" . system_showText(LANG_LABEL_ADVERTISE_DEAL_TITLE) . "</a></h4>";
            $listing_deal_link = "javascript:void(0);";
            $listing_deal_link_style = "style=\"cursor:default;\"";
        } else {
            if ($promotionObj) {
                unset($promotionObj);
                if ($promotion)
                    unset($promotion);
            }

            $hasDeal = false;
            $listing_deal = "";
            if ($listing['promotion_id'] && (PROMOTION_FEATURE == 'on' && CUSTOM_PROMOTION_FEATURE == 'on')) {

                $promotionObj = new Promotion($listing['promotion_id']);

                if ((validate_date_deal($promotionObj->getDate("start_date"), $promotionObj->getDate("end_date"))) && (validate_period_deal($promotionObj->getNumber("visibility_start"), $promotionObj->getNumber("visibility_end")))) {
                    $hasDeal = true;

                    $promotionInfo['name'] = $promotionObj->getString('name', true, 40);

                    if ($promotionObj->realvalue > 0 && $promotionObj->dealvalue > 0) {
                        $offer = CURRENCY_SYMBOL . string_substr($promotionObj->dealvalue, 0, (string_strpos($promotionObj->dealvalue, ".")));
                        $cents = string_substr($promotionObj->getNumber("dealvalue"), (string_strpos($promotionObj->getNumber("dealvalue"), ".")), 3);
                        if ($cents == ".00")
                            $cents = "";
                    } else {
                        $offer = CURRENCY_SYMBOL . "0";
                        $cents = "";
                    }
                    $promotionInfo['offer'] = $offer;
                    $promotionInfo['cents'] = $cents;

                    $promotionInfo['url'] = PROMOTION_DEFAULT_URL . '/' . $promotionObj->getString('friendly_url') . ".html";
                    if ($user) {
                        $listing_deal_link = $promotionInfo['url'];
                        $listing_deal_link_style = "";
                    } else {
                        $listing_deal_link = "javascript:void(0);";
                        $listing_deal_link_style = "style=\"cursor:default;\"";
                    }

                    if (!$user) {
                        $promotionInfo['url'] = 'javascript:void(0)';
                        $promotionInfo['style'] = " style=\"cursor:default\"";
                    }

                    $listing_deal .= "<div class=\"deal-tag\">" . $promotionInfo['offer'] . ($promotionInfo['cents'] ? "<span class=\"cents\">" . $promotionInfo['cents'] . "</span>" : "") . "</div>";
                    $listing_deal .= "<h4><a href=\"" . $promotionInfo['url'] . "\"" . $promotionInfo['style'] . ">" . $promotionInfo['name'] . "</a></h4>";
                }
            }
        }
    }
    ?>
    <style>
        .VotenowParnetresult {
            float: left;
            margin: 0 0 10px;

            right: 0;
            top: 24px;

        }
        .VotenowParnetresult .Votenowresult {
            background-image: -ms-linear-gradient(top, #B8E229 0%, #93C100 100%);
            background-image: -moz-linear-gradient(top, #B8E229 0%, #93C100 100%);
            background-image: -o-linear-gradient(top, #B8E229 0%, #93C100 100%);
            background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #B8E229), color-stop(1, #93C100));
            background-image: -webkit-linear-gradient(top, #B8E229 0%, #93C100 100%);
            background-image: linear-gradient(to bottom, #B8E229 0%, #93C100 100%);

            border-radius: 5px 5px 5px 5px;
            color: #FFFFFF !important;
            font-size: 9pt;
            height: 39px;
            padding: 4px 10px;
            width: 117px;
        }

    </style>






    <!--<div id="comment"></div>-->
    <?php
    if ($mapNumber != null) {
        $url = "http://" . $_SERVER['HTTP_HOST'] . "/listing/" . $listing['friendly_url'] . ".html";

        include(EDIRECTORY_ROOT . "/fb_share.php");
//        $data2 = mysql_query("UPDATE Listing_Summary  SET fbcount=(" . $fbcount . ")  where friendly_url = ('" . $listing['friendly_url'] . "')");
        ?>

        <script>
        <?php
        if (SELECTED_DOMAIN_ID == 1) {
           
        
                $get_url = "/frontend/view_fb_count.php";
           
            ?>
                $(document).ready(function() {
                    //  alert($(".VotenowParnetresult").length)
                    // alert(<?php echo $fbcount ?>)

                    //   if ($(".VotenowParnetresult").length <1) {

                    var summaryclass = $("#listing_summary_<?php echo $listing['id'] ?> .media-body .row-fluid.info .span8").find(".summary-address");
                    $("#listing_summary_<?php echo $listing['id'] ?> .media-body .row-fluid.info .span8").append('<div class="VotenowParnetresult"><a onClick="fbcall($(this));return false;" class="Votenowresult" href=""> <img pagespeed_url_hash="1506468634" src="http://toplocalrated.com/theme/default/images/imagery/Megaphone.png" style="width:12%">Vote For Us!</a><span style="color:#EFB653 ;border: 1px solid #EEEEEE; font-size: 8pt; padding: 0px 5px; margin-left: 10px;" id="facebaook_count"> <?php echo $listing["fbcount"]; ?> </span><input type="hidden" class="listurl" value=<?php echo $url; ?> /><input type="hidden" class="listname" value=<?php echo $listing["title"]; ?> /></div>');
                    //                    $("#listing_summary_<?php echo $listing['id'] ?> .media-body").append('<input type="hidden" class="listurl" value=<?php echo $url; ?> />')
                    //                    $("#listing_summary_<?php echo $listing['id'] ?> .media-body").append('<input type="hidden" class="listname" value=<?php echo $listing["title"]; ?> />')

                    // }
                });
        <?php } ?>

            window.fbAsyncInit = function() {

                FB.init({
                    appId: '1412617082285011',
                    status: true,
                    cookie: true,
                    xfbml: false,
                    oauth: true
                });


            };
            (function(d) {
                var js, id = 'facebook-jssdk';
                if (d.getElementById(id)) {
        //                    window.fbAsyncInit();
                    return;
                }
                js = d.createElement('script');
                js.id = id;
                js.async = true;
                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                d.getElementsByTagName('head')[0].appendChild(js);
            }(document));


            function fbcall(x) {
                
                var get_url ="<?php echo "http://" . $_SERVER['HTTP_HOST'].$get_url; ?>"
              
                var url = x.parent(".VotenowParnetresult").find(".listurl").val();
                var friendurl = url.split("/");
                var detailLink = friendurl[4];
                var name = x.parent(".VotenowParnetresult").find(".listname").val();
                var facebaook_count = x.parent(".VotenowParnetresult").find("#facebaook_count").text();

                $.get(get_url, {
                    facebaook_count: facebaook_count,
                    friendly_url: detailLink,
                }, function(data) {
                    if (data == 1) {
                        newfb = parseInt(facebaook_count, 10) + parseInt('1', 10);
                        x.parent(".VotenowParnetresult").find("#facebaook_count").text(newfb);
                    }

                });

                FB.ui({
                    method: 'feed',
                    name: name,
                    link: url,
                    description: "<?php !empty($listing["'description'"]) ? $listing["'description'"] : '' ?>"



                },
                function(response) {
                    if (response && response.post_id) {
                        //alert('Post was  published.');
                    } else {
                        //alert('Post was not published.');

                    }
                }
                );




            }

        //
        </script>




        <?php
    }
    include(INCLUDES_DIR . "/views/view_listing.php");
}
?>
