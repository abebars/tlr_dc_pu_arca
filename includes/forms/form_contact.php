<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/forms/form_contact.php
	# ----------------------------------------------------------------------------------------------------

	$item_form = true;
	$contact = true;
	$dropdown_protocol = html_protocolDropdown($url, "url_protocol", false, $protocol_replace);
          if(SELECTED_DOMAIN_ID==3){
        $ProfileContact = new AccountProfileContact($account_id);
      
        if (!$nonprofit_categories) if ($ProfileContact) $nonprofit_categories = $ProfileContact->getNonProfitCategories(false, false, $account_id, true, true);
//       print_r( $nonprofit_categories); exit;
        if ($nonprofit_categories) {
		for ($i=0; $i<count($nonprofit_categories); $i++) {
			$arr_category1[$i]["name"] = $nonprofit_categories[$i]->getString("title");
			$arr_category1[$i]["value"] = $nonprofit_categories[$i]->getNumber("id");
			$arr_return_categories1[] = $nonprofit_categories[$i]->getNumber("id");
		}
		if ($arr_return_categories1) $return_categories_nonprofit = implode(",", $arr_return_categories1);
		array_multisort($arr_category1);
		
                $feedDropDown1 = "<select name='feed1' id='feed1' multiple size='5' style=\"width:500px;border:1px solid !important;border-color: #D4D4D4 #EEEEEE #EEEEEE #D4D4D4 !important;\">";
		if ($arr_category1) foreach ($arr_category1 as $each_category) {
			$feedDropDown1 .= "<option value='".$each_category["value"]."'>".$each_category["name"]."</option>";
			$feedAjaxCategory1[] = $each_category["value"];
		}
		$feedDropDown1 .= "</select>";
	} else {
		if ($return_categories_nonprofit) {
			$return_categories_array = explode(",", $return_categories_nonprofit);
			if ($return_categories_array) {
				foreach ($return_categories_array as $each_category) {
					$nonprofit_categories[] = new NonProfitListingCategory($each_category);
				}
			}
		}
		
                $feedDropDown1 = "<select name='feed1' id='feed1' multiple size='5' style=\"width:500px;border:1px solid !important;border-color: #D4D4D4 #EEEEEE #EEEEEE #D4D4D4 !important;\">";
		if ($nonprofit_categories) {
			foreach ($nonprofit_categories as $category) {
				$name = $category->getString("title");
				$feedDropDown1 .= "<option value='".$category->getNumber("id")."'>$name</option>";
				$feedAjaxCategory1[] = $category->getNumber("id");
			}
		}
		$feedDropDown1 .= "</select>";
	}
        }
        
      
?>

<script language="javascript" type="text/javascript" src="<?=DEFAULT_URL?>/scripts/categorytree.js"></script>
<script language="javascript" type="text/javascript">
    	<?php     if(SELECTED_DOMAIN_ID==3){
            ?>	
            $(document).ready(function(){
                loadCategoryTreeNonProfit('all', 'nonprofitlisting_', 'NonProfitListingCategory', 0, 0, '<?=DEFAULT_URL."/".EDIR_CORE_FOLDER_NAME."/".LISTING_FEATURE_FOLDER?>',<?=SELECTED_DOMAIN_ID?>);
    
            })
        <?php } ?>
        
         
        function JS_addCategory1(id) {

		
               
		var  feed = document.account.feed1;
          
		
		var text = unescapeHTML($("#nonprofitliContent"+id).html());
		
			var flag=true;
			for (i=0;i<feed.length;i++)
				if (feed.options[i].value==id)
					flag=false;

			if(text && id && flag){
				feed.options[feed.length] = new Option(text, id);
				$('#nonprofitcategoryAdd'+id).after("<span class=\"categorySuccessMessage\"><?=system_showText(LANG_MSG_CATEGORY_SUCCESSFULLY_ADDED)?></span>").css('display', 'none');
				$('.categorySuccessMessage').fadeOut(5000);
			} else {
				if (!flag) $('#nonprofitcategoryAdd'+id).after("</a> <span class=\"categoryErrorMessage\"><?=system_showText(LANG_MSG_CATEGORY_ALREADY_INSERTED)?></span> </li>");
				else ('#nonprofitcategoryAdd'+id).after("</a> <span class=\"categoryErrorMessage\"><?=system_showText(LANG_MSG_SELECT_VALID_CATEGORY)?></span> </li>");
			}
		

        $('#removeCategoriesButton1').show(); 

	}
        
     function JS_removeCategory1(feed, order) {

    if (feed.selectedIndex >= 0) {
        $('#nonprofitcategoryAdd'+feed.options[feed.selectedIndex].value).after($('.categorySuccessMessage').empty());
        $('#nonprofitcategoryAdd'+feed.options[feed.selectedIndex].value ).fadeIn(500);
        feed.remove(feed.selectedIndex);
        if (order){
            orderCalculate();
        }
    }
    
	if(feed.length == 0){
    	$('#removeCategoriesButton1').hide();
    }

}
   
   
	function JS_submit() {
		
                <?php if(SELECTED_DOMAIN_ID==3){ ?>
                        feed1 = document.account.feed1;
                        return_categories_nonprofit= document.account.return_categories_nonprofit;
		if(return_categories_nonprofit.value.length > 0) return_categories_nonprofit.value="";

		for (i=0;i<feed1.length;i++) {
			if (!isNaN(feed1.options[i].value)) {
				if(return_categories_nonprofit.value.length > 0)
				return_categories_nonprofit.value = return_categories_nonprofit.value + "," + feed1.options[i].value;
				else
			return_categories_nonprofit.value = return_categories_nonprofit.value + feed1.options[i].value;
			}
		}
            
                <?php } ?>
		

		document.account.submit();
	}
</script>
	


  
      
      
            
            <?php //non profit
                if(SELECTED_DOMAIN_ID==3){ ?>
    
    <table style="margin: 25px 0 25px 141px;">
                <tr>
                    <th colspan="2" class="standard-tabletitle" >			
			<?=system_showText(LANG_CATEGORIES_SUBCATEGS1)?>
			
		</th>
	</tr>
	<tr>
		<td colspan="2" class="standard-tableContent">
                    <p class="warningBOXtext" style=" color: #494949; font-size: 14px;"><?=system_showText(LANG_MSG_ONLY_SELECT_SUBCAUSE)?> <?=system_showText(USER_MAX_CAUSE_ALLOWED);?><?=system_showText(LANG_MSG_ONLY_SELECT_SUBCAUSE1)?><br /></p>
		</td>
	</tr>
                
		<input type="hidden" name="return_categories_nonprofit" value="" />
                <tr>
			<td colspan="2" class="treeView">
				<ul id="nonprofitlisting_categorytree_id_0" class="categoryTreeview">&nbsp;</ul>

				<table width="100%" border="0" cellpadding="2" class="tableCategoriesADDED" cellspacing="0">
					<tr>
						<th colspan="2" class="tableCategoriesTITLE alignLeft"><i><span>Your Choosen Causes</span></i> </th>
					</tr>
					<tr id="msgback2" class="informationMessageShort">
						<td colspan="2" style="width: auto;"><p><img width="16" height="14" src="<?=DEFAULT_URL?>/images/icon_atention.gif" alt="<?=LANG_LABEL_ATTENTION?>" title="<?=LANG_LABEL_ATTENTION?>" /> <?=system_showText(LANG_MSG_CLICKADDTOSELECTCATEGORIES);?></p></td>
					</tr>
					<tr id="msgback" class="informationMessageShort" style="display:none">
						<td colspan="2"><i>  <span>Your Choosen Causes</span></i> <div><img width="16" height="14" src="<?=DEFAULT_URL?>/images/icon_atention.gif" alt="<?=LANG_LABEL_ATTENTION?>" title="<?=LANG_LABEL_ATTENTION?>" /></div><p><?=system_showText(LANG_MSG_CLICKADDTOADDCATEGORIES);?></p></td>
					</tr>
					<tr>
						<td colspan="2" class="tableCategoriesCONTENT"><?=$feedDropDown1?></td>
					</tr>
					<tr id="removeCategoriesButton1" <?=(($nonprofit_categories) ? "" :"style='display:none'") ?>>
						<td class="tableCategoriesBUTTONS" colspan="2">
							<center>
                                                            <button class="input-button-form"  style="background-color: #494949;" type="button" value="<?=system_showText(LANG_BUTTON_REMOVESELECTEDCATEGORY)?>" onclick="JS_removeCategory1(document.account.feed1, false);"><?=system_showText(LANG_BUTTON_REMOVESELECTEDCATEGORY)?></button>
							</center>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
                   </table>
       
        
                <? } ?>     
  
          <table id="contact_info" border="0" cellpadding="2" cellspacing="0" class="standard-table standardSIGNUPTable noMargin">      
        <tr>
            <th>* <?=system_showText(LANG_LABEL_FIRST_NAME);?>:</th>
            <td><input type="text" name="first_name" value="<?=$first_name?>" /></td>
        </tr>
        <tr>
            <th>* <?=system_showText(LANG_LABEL_LAST_NAME);?>:</th>
            <td><input type="text" name="last_name" value="<?=$last_name?>" /></td>
        </tr>
      <tr>
            <th><?=system_showText(LANG_LABEL_COMPANY);?>:</th>
            <td><input type="text" name="company" value="<?=$company?>" /></td>
        </tr>
        <tr>
            <th class="alignTop alignWithField" valign="top"><?=system_showText(LANG_LABEL_ADDRESS1)?>:</th>
            <td><input type="text" name="address" value="<?=$address?>" maxlength="50" />
                <span><?=system_showText(LANG_ADDRESS_EXAMPLE)?></span>
            </td>
        </tr>
        <tr>
            <th class="alignTop alignWithField"><?=system_showText(LANG_LABEL_ADDRESS2)?>:</th>
            <td><input type="text" name="address2" value="<?=$address2?>" maxlength="50" />
                <span><?=system_showText(LANG_ADDRESS2_EXAMPLE)?></span>
            </td>
        </tr>
        <tr>
            <th><?=system_showText(LANG_LABEL_COUNTRY)?>:</th>
            <td><input type="text" name="country" value="<?=$country?>" /></td>
        </tr>
        <tr>
            <th><?=system_showText(LANG_LABEL_STATE)?>:</th>
            <td><input type="text" name="state" value="<?=$state?>" /></td>
        </tr>
        <tr>
            <th><?=system_showText(LANG_LABEL_CITY)?>:</th>
            <td><input type="text" name="city" value="<?=$city?>" /></td>
        </tr>
        <tr>
            <th><?=string_ucwords(ZIPCODE_LABEL)?>:</th>
            <td><input type="text" name="zip" value="<?=$zip?>" /></td>
        </tr>
        <tr>
            <th><?=system_showText(LANG_LABEL_PHONE)?>:</th>
            <td><input type="text" name="phone" value="<?=$phone?>" /></td>
        </tr>
        <tr>
            <th><?=system_showText(LANG_LABEL_FAX)?>:</th>
            <td><input type="text" name="fax" value="<?=$fax?>" /></td>
        </tr>
        <? if (($id || $account_id) && $isForeignAcc) { ?>
        <tr>
            <th>* <?=system_showText(LANG_LABEL_EMAIL)?>:</th>
            <td><input type="text" name="email" id="email" value="<?=$email?>" /></td>
        </tr>
        <? } ?>
        <tr>
            <th>
                <input type="checkbox" name="notify_traffic_listing" id="notify_traffic_listing" <?=($notify_traffic_listing == "y" || $notify_traffic_listing == "on" || (!$id && $_SERVER["REQUEST_METHOD"] != "POST")) ? "checked=\"checked\"": "" ?> class="inputRadio" />
            </th>
            <td>
                <?=system_showText(LANG_LABEL_NOTIFY_TRAFFIC);?>
            </td>
        </tr>
        <tr>
            <th><?=system_showText(LANG_LABEL_URL)?>:</th>
            <td>
                <?=$dropdown_protocol;?>
                <input type="text"  class="httpInput" name="url" value="<?=str_replace($protocol_replace, "", $url)?>" <?=(string_strpos($_SERVER["PHP_SELF"], "".SITEMGR_ALIAS."") !== false ? "style=\"width:487px\"" : "style=\"width:430px\"");?>/>
            </td>
        </tr>
    </table>

    <? if (($id || $account_id) && $isForeignAcc) { ?>
        <input type="hidden" name="isforeignAcc" value="y" />
        <input type="hidden" name="foreignaccount" value="y" />
    <? } else { ?>
        <input type="hidden" name="email" id="email" value="<?=$email?>" />
    <? } ?>