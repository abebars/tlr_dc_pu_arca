<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/forms/form_advertise.php
	# ----------------------------------------------------------------------------------------------------

    include(INCLUDES_DIR."/code/newsletter.php");

    $defaultusername = $username;
    $defaultpassword = "";
    if (DEMO_MODE) {
        $defaultusername = "demo@demodirectory.com";
        $defaultpassword = "abc123";
    }
    
    $msgLogged = "";

    $defaultActionForm = $formloginaction.($advertiseItem == "banner" ? "&amp;query=type=".($_POST["type"] ? $_POST["type"] : $_GET["type"]) : "&amp;query=level=".($_POST["level"] ? $_POST["level"] : $_GET["level"]));
    
    if ($advertiseItem == "listing") {
        $defaultActionForm .= "&amp;listingtemplate_id=".($_POST["listingtemplate_id"] ? $_POST["listingtemplate_id"] : $_GET["listingtemplate_id"]);
    }
?>

    <script language="javascript" type="text/javascript">
		<!--

		function orderCalculate() {

			var xmlhttp;

			try {
				xmlhttp = new XMLHttpRequest();
			} catch (e) {
				try {
					xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				} catch (e) {
					try {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e) {
						xmlhttp = false;
					}
				}
			}

			if (document.getElementById("check_out_payment")) document.getElementById("check_out_payment").className = "isHidden";
			if (document.getElementById("check_out_payment_2")) document.getElementById("check_out_payment_2").className = "isHidden";
			if (document.getElementById("check_out_free")) document.getElementById("check_out_free").className = "isHidden";
			if (document.getElementById("check_out_free_2")) document.getElementById("check_out_free_2").className = "isHidden";
			if (document.getElementById("loadingOrderCalculate")) document.getElementById("loadingOrderCalculate").style.display = "";
			if (document.getElementById("loadingOrderCalculate")) document.getElementById("loadingOrderCalculate").innerHTML = "<?=system_showText(LANG_WAITLOADING)?>";
			if (xmlhttp) {
				xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == 4) {
						if (xmlhttp.status == 200) {
							var price = xmlhttp.responseText;
							var arrPrice = price.split("|");
							var html = "";
							var tax_status = '<?=$payment_tax_status;?>';
                            var tax_info = "";
							<? if ((PAYMENT_FEATURE == "on") && ((CREDITCARDPAYMENT_FEATURE == "on") || (INVOICEPAYMENT_FEATURE == "on"))) { ?>
								if (arrPrice[0] > 0) {
									if (tax_status == "on") {
										html += "<strong><?=system_showText(LANG_SUBTOTALAMOUNT)?>: </strong><?=CURRENCY_SYMBOL?>"+arrPrice[0].substring(0, arrPrice[0].length-2)+"."+arrPrice[0].substring(arrPrice[0].length-2, arrPrice[0].length);
										html += "<br /><strong><?=system_showText(LANG_TAXAMOUNT)?>: </strong><?=CURRENCY_SYMBOL?>"+arrPrice[1].substring(0, arrPrice[1].length-2)+"."+arrPrice[1].substring(arrPrice[1].length-2, arrPrice[1].length);
										html += "<br /><strong><?=system_showText(LANG_TOTALPRICEAMOUNT)?>: </strong><?=CURRENCY_SYMBOL?>"+arrPrice[2].substring(0, arrPrice[2].length-2)+"."+arrPrice[2].substring(arrPrice[2].length-2, arrPrice[2].length);
										tax_info = "<?="+".$payment_tax_value."% ".$payment_tax_label;?> (" + "<?=CURRENCY_SYMBOL?>"+arrPrice[2].substring(0, arrPrice[2].length-2)+"."+arrPrice[2].substring(arrPrice[2].length-2, arrPrice[2].length) + ")";
									} else {
										html += "<strong><?=system_showText(LANG_TOTALPRICEAMOUNT)?>: </strong><?=CURRENCY_SYMBOL?>"+arrPrice[0].substring(0, arrPrice[0].length-2)+"."+arrPrice[0].substring(arrPrice[0].length-2, arrPrice[0].length);
									}
									$('#divTax').addClass('isVisible');
									$('#divTax').removeClass('isHidden');
                                    $("#free_item").attr("value", "0");
									if (document.getElementById("check_out_payment")) document.getElementById("check_out_payment").className = "isVisible";
									if (document.getElementById("check_out_payment_2")) document.getElementById("check_out_payment_2").className = "isVisible";
									if (document.getElementById("payment-method")) document.getElementById("payment-method").className = "isVisible";
									if (document.getElementById("checkoutpayment_total")) document.getElementById("checkoutpayment_total").innerHTML = html;
								} else {
									$('#divTax').addClass('isHidden');
									$('#divTax').removeClass('isVisible');
                                    $("#free_item").attr("value", "1");
									if (document.getElementById("check_out_free")) document.getElementById("check_out_free").className = "isVisible";
									if (document.getElementById("check_out_free_2")) document.getElementById("check_out_free_2").className = "isVisible";
                                    if (document.getElementById("payment-method")) document.getElementById("payment-method").className = "isHidden";
									if (document.getElementById("checkoutfree_total")) document.getElementById("checkoutfree_total").innerHTML = "<strong><?=system_showText(LANG_TOTALPRICEAMOUNT)?>: </strong><?=CURRENCY_SYMBOL?><?=system_showText(LANG_FREE)?>";
								}
								if (tax_status == "on") document.getElementById("taxInfo").innerHTML = tax_info;
							<? } else { ?>
                                $("#free_item").attr("value", "1");
								if (document.getElementById("check_out_free")) document.getElementById("check_out_free").className = "isVisible";
								if (document.getElementById("check_out_free_2")) document.getElementById("check_out_free_2").className = "isVisible";
                                if (document.getElementById("payment-method")) document.getElementById("payment-method").className = "isHidden";
								if (arrPrice[0] > 0) {
									if (tax_status == "on") {
										html += "<strong><?=system_showText(LANG_SUBTOTALAMOUNT)?>: </strong><?=CURRENCY_SYMBOL?>"+arrPrice[0].substring(0, arrPrice[0].length-2)+"."+arrPrice[0].substring(arrPrice[0].length-2, arrPrice[0].length);
										html += "<br /><strong><?=system_showText(LANG_TAXAMOUNT)?>: </strong><?=CURRENCY_SYMBOL?>"+arrPrice[1].substring(0, arrPrice[1].length-2)+"."+arrPrice[1].substring(arrPrice[1].length-2, arrPrice[1].length);
										html += "<br /><strong><?=system_showText(LANG_TOTALPRICEAMOUNT)?>: </strong><?=CURRENCY_SYMBOL?>"+arrPrice[2].substring(0, arrPrice[2].length-2)+"."+arrPrice[2].substring(arrPrice[2].length-2, arrPrice[2].length);
										tax_info = "<?="+".$payment_tax_value."% ".$payment_tax_label;?> (" + "<?=CURRENCY_SYMBOL?>"+arrPrice[2].substring(0, arrPrice[2].length-2)+"."+arrPrice[2].substring(arrPrice[2].length-2, arrPrice[2].length) + ")";
									} else {
										html += "<strong><?=system_showText(LANG_TOTALPRICEAMOUNT)?>: </strong><?=CURRENCY_SYMBOL?>"+arrPrice[0].substring(0, arrPrice[0].length-2)+"."+arrPrice[0].substring(arrPrice[0].length-2, arrPrice[0].length);
									}
									$('#divTax').addClass('isVisible');
									$('#divTax').removeClass('isHidden');
									if (document.getElementById("checkoutfree_total")) document.getElementById("checkoutfree_total").innerHTML = html;
								} else {
									$('#divTax').addClass('isHidden');
									$('#divTax').removeClass('isVisible');
									if (document.getElementById("checkoutfree_total")) document.getElementById("checkoutfree_total").innerHTML = "<strong><?=system_showText(LANG_TOTALPRICEAMOUNT)?>: </strong><?=CURRENCY_SYMBOL?><?=system_showText(LANG_FREE)?>";
								}
								if (tax_status == "on") document.getElementById("taxInfo").innerHTML = tax_info;
							<? } ?>
							if (document.getElementById("loadingOrderCalculate")) document.getElementById("loadingOrderCalculate").style.display = "none";
							if (document.getElementById("loadingOrderCalculate")) document.getElementById("loadingOrderCalculate").innerHTML = "";
						}
					}
				}
				var get_level = "";
				if (document.order_item.level) get_level = "&level=" + document.order_item.level.value;
                
				var get_categories = "";
				if (document.order_item.feed) get_categories = "&categories=" + document.order_item.feed.length;
                
				var get_listingtemplate_id = "";
				if (document.order_item.select_listingtemplate_id) get_listingtemplate_id = "&listingtemplate_id=" + document.order_item.select_listingtemplate_id.value;
                    
				var get_discount_id = "";
				if (document.order_item.discount_id) get_discount_id = document.order_item.discount_id.value;
                
                var get_type = "";
				if (document.order_item.type) get_type = "&type=" + document.order_item.type.value;
                
                var get_expiration_setting = "";
				if (document.order_item.expiration_setting) get_expiration_setting = "&expiration_setting=" + document.order_item.expiration_setting.value;
                
				var get_unpaid_impressions = "";
				if (document.order_item.unpaid_impressions) get_unpaid_impressions = "&unpaid_impressions=" + document.order_item.unpaid_impressions.value;
				
                xmlhttp.open("GET", "<?=DEFAULT_URL;?>/ordercalculateprice.php?item=<?=$advertiseItem?>&item_id=<?=$unique_id;?>"+get_level+get_categories+get_listingtemplate_id+"&discount_id="+get_discount_id+get_type+get_expiration_setting+get_unpaid_impressions, true);
				xmlhttp.send(null);
			}
		}

		<? if (LISTINGTEMPLATE_FEATURE == "on" && CUSTOM_LISTINGTEMPLATE_FEATURE == "on" && $advertiseItem == "listing") { ?>
			function templateSwitch(template) {
				if (!template) template = 0;
				<?=$jsVarsType;?>
				document.order_item.listingtemplate_id.value = template;
				if (document.getElementById("title_label")) {
                    document.getElementById("title_label").innerHTML = eval("title_template_" + template);
                }
				orderCalculate();
				loadCategoryTree('template', 'listing_', 'ListingCategory', 0, template, '<?=DEFAULT_URL."/".EDIR_CORE_FOLDER_NAME."/".LISTING_FEATURE_FOLDER?>',<?=SELECTED_DOMAIN_ID?>);
				updateFormAction();
			}
		<? } ?>
        
        <? if ($advertiseItem == "banner") { ?>
        function typeSwitch(type, expiration_setting) {
			<?
			foreach ($levelValue as $value) {
				echo "var impressions_".$value."_".BANNER_EXPIRATION_RENEWAL_DATE." = 0;";
				echo "var impressions_".$value."_".BANNER_EXPIRATION_IMPRESSION." = ".$bannerLevelObj->getImpressionBlock($value).";";
			}
			?>
			document.order_item.type.value = type;
			document.order_item.expiration_setting.value = expiration_setting;
			document.order_item.unpaid_impressions.value = eval("impressions_" + type + "_" + expiration_setting);
			orderCalculate();
			updateFormAction();
		}
        <? } ?>
        
        function updateFormAction() {
            var levelValue = "";
            var titleValue = "";
            var templateValue = "";
            var categValue = "";
            var discountValue = "";
            var packageValue = "";
            var packageID = "";
            var startDateValue = "";
            var endDateValue = "";
            var expirationValue = "";
            var advertiseItem = "<?=$advertiseItem?>";
            
            //Get level/type
            if (document.order_item.level) {
                levelValue = "level=" + document.order_item.level.value;
            } else if (document.order_item.type) {
                levelValue = "type=" + document.order_item.type.value;
            }
            
            //Get Title/Caption
            if (document.order_item.title) {
                titleValue = "&title=" + urlencode(document.order_item.title.value);
            } else if (document.order_item.caption) {
                titleValue = "&caption=" + urlencode(document.order_item.caption.value);
            }
            
            //Get expiration setting (banner)
            if (document.order_item.expiration_setting) {
                expirationValue = "&expiration_setting=" + document.order_item.expiration_setting.value;
            }
            
            //Get Template ID
            if (document.order_item.select_listingtemplate_id) {
                templateValue = "&listingtemplate_id=" + document.order_item.select_listingtemplate_id.value;
            }
            
            //Get Discount
            if (document.order_item.discount_id) {
                discountValue = "&discount_id=" + document.order_item.discount_id.value;
            }
            
            //Get Start Date (event)
            if (document.order_item.start_date) {
                startDateValue = "&start_date=" + document.order_item.start_date.value;
            }
            
            //Get End Date (event)
            if (document.order_item.end_date) {
                endDateValue = "&end_date=" + document.order_item.end_date.value;
            }
            
            <? if ($advertiseItem == "listing") { ?>
                    
            //Get Categories
            feed = document.order_item.feed;
			var return_categories = "";

			for (i = 0; i < feed.length; i++) {
				if (!isNaN(feed.options[i].value)) {
					if (return_categories.length > 0) {
                        return_categories = return_categories + "," + feed.options[i].value;
                    } else {
                        return_categories = return_categories + feed.options[i].value;
                    }
				}
			}
            if (return_categories.length > 0) {
                categValue = "&return_categories=" + return_categories;
            }
            
            <? } ?>
            
            //Get package
            if ($("#using_package").val() == "y") {
                packageID = $("#aux_package_id").val();
                packageValue = "&package_id="+packageID;
            } else if (advertiseItem == "article") {
                packageID = "skipPackageOffer";
                packageValue = "&package_id="+packageID;
            }
            
            if (document.formDirectory != undefined)	document.formDirectory.action = "<?=$formloginaction?>&query=" + levelValue + templateValue + titleValue + categValue + discountValue + packageValue + startDateValue + endDateValue + expirationValue;
            if (document.formOpenID != undefined)		document.formOpenID.action = "<?=$formloginaction?>&query=" + levelValue + templateValue + titleValue + categValue + discountValue + packageValue + startDateValue + endDateValue + expirationValue;
            if (document.formCurrentUser != undefined)	document.formCurrentUser.action = "<?=$formloginaction?>&query=" + levelValue + templateValue + titleValue + categValue + discountValue + packageValue + startDateValue + endDateValue + expirationValue;
                     
            <? if ($googleEnabled || $facebookEnabled) { ?>
                
                $.get(DEFAULT_URL + "/ordercalculateprice.php", {
                    
                    item:               "<?=$advertiseItem?>",
                    
                    item_id:            "<?=$unique_id;?>",
                    
                    <? if ($advertiseItem == "banner") { ?>
                    
                    type:              document.order_item.type.value,
                    
                    caption:            document.order_item.caption.value,
                    
                    <? } else { ?>
                        
                    level:              document.order_item.level.value,
                    
                    title:              document.order_item.title.value,
                        
                    <? } ?>
                    
                    <? if ($advertiseItem == "listing") { ?>
                        
                    <? if (LISTINGTEMPLATE_FEATURE == "on" && CUSTOM_LISTINGTEMPLATE_FEATURE == "on" && !USING_THEME_TEMPLATE) { ?>
                        
                    listingtemplate_id: document.order_item.select_listingtemplate_id.value,
                    
                    <? } ?>
                    
                    return_categories:  return_categories,
                    
                    <? } elseif ($advertiseItem == "event") { ?>
                        
                    start_date:         document.order_item.start_date.value,
                    
                    end_date:           document.order_item.end_date.value,
                    
                    <? } elseif ($advertiseItem == "banner") { ?>
                        
                    expiration_setting: document.order_item.expiration_setting.value,
                    
                    unpaid_impressions: document.order_item.unpaid_impressions.value,
                    
                    <? } ?>
                        
                    discount_id:        document.order_item.discount_id.value,
                    
                    package_id:         packageID
                }, function () {});
                
            <? } ?>
        }

        <? if ($advertiseItem == "listing") { ?>

		function JS_addCategory(id) {
			seed = document.order_item.seed;
			feed = document.order_item.feed;
            var text = unescapeHTML($("#liContent"+id).html());
			var flag = true;
			for (i = 0; i < feed.length; i++) {
				if (feed.options[i].value == id) {
                    flag = false;
                }
				if (!feed.options[i].value) {
					feed.remove(feed.options[i]);
				}
			}
			if (text && id && flag) {
				feed.options[feed.length] = new Option(text, id);
				$('#categoryAdd'+id).after("<span class=\"categorySuccessMessage\"><?=system_showText(LANG_MSG_CATEGORY_SUCCESSFULLY_ADDED)?></span>").css('display', 'none');
				$('.categorySuccessMessage').fadeOut(5000);
				orderCalculate();
                updateFormAction();
			} else {
				if (!flag) {
                    $('#categoryAdd'+id).after("</a> <span class=\"categoryErrorMessage\"><?=system_showText(LANG_MSG_CATEGORY_ALREADY_INSERTED)?></span> </li>");
                } else {
                    ('#categoryAdd'+id).after("</a> <span class=\"categoryErrorMessage\"><?=system_showText(LANG_MSG_SELECT_VALID_CATEGORY)?></span> </li>");
                }
			}
			
            $('#removeCategoriesButton').show(); 

		}
        
        <? } ?>
        
		function JS_submit() {
            disableButtons();
            <? if ($advertiseItem == "listing") { ?>
			feed = document.order_item.feed;
			return_categories = document.order_item.return_categories;
			if (return_categories.value.length > 0) {
                return_categories.value = "";
            }
			for (i = 0; i < feed.length; i++) {
				if (!isNaN(feed.options[i].value)) {
					if (return_categories.value.length > 0) {
                        return_categories.value = return_categories.value + "," + feed.options[i].value;
                    } else {
                        return_categories.value = return_categories.value + feed.options[i].value;
                    }
				}
			}
            <? } ?>
		}

		//-->
	</script>

    <div style="display:none">
        
        <form id="formDirectory" name="formDirectory" method="post" action="<?=$defaultActionForm;?>">
		
            <input type="hidden" name="userform" value="directory" />
            <input type="hidden" name="advertise" value="yes" />
            <input type="hidden" name="destiny" value="<?=$destiny?>" />
            <input type="hidden" name="query" value="<?=urlencode($query)?>" />
            
            <input type="hidden" name="username" id="form_username" value="" />
            <input type="hidden" name="password" id="form_password" value="" />

        </form>
        
        <? if ($openIDEnabled) { ?>
        
        <form id="formOpenID" name="formOpenID" method="post" action="<?=$defaultActionForm;?>">
		
            <input type="hidden" name="userform" value="openid" />
            <input type="hidden" name="advertise" value="yes" />
            <input type="hidden" name="destiny" value="<?=$destiny?>" />
            <input type="hidden" name="query" value="<?=urlencode($query)?>" />

            <input type="hidden" name="openidurl" id="form_openidurl" value="" />

        </form>
        
        <? } ?>
        
        <? if (sess_isAccountLogged()) { ?>
        
        <form id="formCurrentUser" name="formCurrentUser" method="post" action="<?=$defaultActionForm;?>">
            <input type="hidden" name="userform" value="currentuser" />
            <input type="hidden" name="advertise" value="yes" />
            <input type="hidden" name="acc" value="<?=sess_getAccountIdFromSession()?>" />

            <?
            if (sess_getAccountIdFromSession()) {
                $msgLogged = system_showText(LANG_ADVERTISE_LOGGED_AS);
                $dbObjWelcome = db_getDBObject(DEFAULT_DB, true);
                $sqlWelcome = "SELECT C.first_name, C.last_name, A.has_profile, P.friendly_url, P.nickname FROM Contact C
                    LEFT JOIN Account A ON (C.account_id = A.id)
                    LEFT JOIN Profile P ON (P.account_id = A.id)
                    WHERE A.id = ".sess_getAccountIdFromSession();
                $resultWelcome = $dbObjWelcome->query($sqlWelcome);
                $contactWelcome = mysql_fetch_assoc($resultWelcome);

                if ($contactWelcome["has_profile"] == "y") {
                    $msgLogged .= " ".$contactWelcome["nickname"].".";
                } else {
                    $msgLogged .= " ". $contactWelcome["first_name"]." ".$contactWelcome["last_name"].".";
                }
                
                $msgLogged .= " <a href=\"".SOCIALNETWORK_URL."/logout.php\">".system_showText(LANG_BUTTON_LOGOUT)."</a>";
            }
            ?>
        </form>
        
        <? } ?>
        
    </div>

    <form name="order_item" action="<?=system_getFormAction($_SERVER["PHP_SELF"])?>" method="post" class="standardForm" onsubmit="JS_submit();">

        <input type="hidden" name="advertise" value="yes" />
        <input type="hidden" name="signup" value="true" />
        
        <? if ($advertiseItem == "banner") { ?>
            <input type="hidden" name="type" id="type" value="<?=$type?>" />
            <input type="hidden" name="expiration_setting" id="expiration_setting" value="<?=$expiration_setting?>" />
            <input type="hidden" name="unpaid_impressions" id="unpaid_impressions" value="<?=$unpaid_impressions?>" />
        <? } else { ?>
            <input type="hidden" name="level" id="level" value="<?=$level?>" />
        <? } ?>
        
        <? if ($advertiseItem == "listing") { ?>
            <input type="hidden" name="listingtemplate_id" id="listingtemplate_id" value="<?=(USING_THEME_TEMPLATE && THEME_TEMPLATE_ID > 0 ? THEME_TEMPLATE_ID : $listingtemplate_id)?>" />
        <? } ?>

        <div class="content-main" id="screen1" <?=($message_account || $message_contact ? "style=\"display: none;\"" : "style=\"display: block;\"")?>>

            <div class="order-head">
                <h2>
                    <?=$labelName;?>
                    <? if ($advertiseItem != "banner") { ?>
                    - <i><?=$labelPrice;?></i><?=$labelPriceRenewal;?>
                    <? } ?>
                </h2>
                <? if ($payment_tax_status == "on") { ?>
                    <div id="divTax" class="isHidden" <?=($advertiseItem == "banner" ? "style=\"display: none;\"" : "")?>>
                        <span id="taxInfo"></span>
                    </div>
                <? } ?>
            </div>

            <div class="order">

                <div id="errorMessage">&nbsp;</div>

                <div id="listing-info">

                    <div class="left textright">
                        <h3><?=system_showText(constant("LANG_".strtoupper($advertiseItem)."INFO"));?></h3>
                        <p>
                            <?=system_showText(constant("LANG_".strtoupper($advertiseItem)."INFO_TIP"));?>
                            <? if ($advertiseItem == "listing" && LISTINGTEMPLATE_FEATURE == "on" && CUSTOM_LISTINGTEMPLATE_FEATURE == "on" && !USING_THEME_TEMPLATE) {
                                echo system_showText(LANG_LISTINGINFO_TIP2);
                            } ?>
                        </p>
                    </div>

                    <div class="right">
                        
                        <div class="cont_70">
                            <label id="title_label" for="<?=$advertiseItem?>-title"><?=($template_title_field !== false && $advertiseItem == "listing") ? $template_title_field[0]["label"] : ($advertiseItem == "banner" ? system_showText(LANG_LABEL_CAPTION) : system_showText(LANG_LABEL_TITLE))?> * <span><?=system_showText(LANG_LABEL_REQUIRED_FIELD);?></span></label>
                            <? if ($advertiseItem == "banner") { ?>
                                <input type="text" name="caption" id="<?=$advertiseItem?>-title" value="<?=$caption?>" maxlength="25" onblur="updateFormAction();" />
                            <? } else { ?>
                                <input type="text" name="title" id="<?=$advertiseItem?>-title" value="<?=$title?>" maxlength="100" onblur="easyFriendlyUrl(this.value, 'friendly_url', '<?=FRIENDLYURL_VALIDCHARS?>', '<?=FRIENDLYURL_SEPARATOR?>'); updateFormAction();" />
                                <input type="hidden" name="friendly_url" id="friendly_url" value="<?=$friendly_url?>" maxlength="150" />
                            <? } ?>
                        </div>
                        
                        <? if ($advertiseItem == "banner") { ?>
                        
                        <div class="cont_70">
                            <table align="center" cellspacing="2" cellpadding="2" border="0" class="standardChooseLevel">
                                <tr>
                                    <td>
                                        <input type="radio" name="type_expiration_setting" value="<?=$type?>_<?=BANNER_EXPIRATION_RENEWAL_DATE?>" <? if (BANNER_EXPIRATION_RENEWAL_DATE == $expiration_setting) { echo "checked=\"checked\""; } ?> onclick="typeSwitch('<?=$type?>', '<?=BANNER_EXPIRATION_RENEWAL_DATE?>');" />
                                    </td>
                                    <td>
                                        <?
                                            if ($bannerLevelObj->getPrice($type) > 0) {
                                                echo CURRENCY_SYMBOL.$bannerLevelObj->getPrice($type);
                                            } else {
                                                echo CURRENCY_SYMBOL.system_showText(LANG_FREE);
                                            }
                                            echo " ".system_showText(LANG_PER)." ";
                                            if (payment_getRenewalCycle("banner") > 1) {
                                                echo payment_getRenewalCycle("banner")." ";
                                                echo payment_getRenewalUnitNamePlural("banner");
                                            } else {
                                                echo payment_getRenewalUnitName("banner");
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <input type="radio" name="type_expiration_setting" value="<?=$type?>_<?=BANNER_EXPIRATION_IMPRESSION?>" <? if (BANNER_EXPIRATION_IMPRESSION == $expiration_setting) { echo "checked=\"checked\""; } ?> onclick="typeSwitch('<?=$type?>', '<?=BANNER_EXPIRATION_IMPRESSION?>');" />
                                    </td>
                                    <td>
                                        <?
                                        if ($bannerLevelObj->getImpressionPrice($type) > 0) {
                                            echo CURRENCY_SYMBOL.$bannerLevelObj->getImpressionPrice($type);
                                        } else {
                                            echo CURRENCY_SYMBOL.system_showText(LANG_FREE);
                                        }
                                        echo " ".system_showText(LANG_PER)." ".$bannerLevelObj->getImpressionBlock($type)." ".system_showText(LANG_IMPRESSIONS);
                                        ?>
                                    </td>
                                </tr>
                              </table>
                        </div>
                        
                        <? } ?>

                        <? if (PAYMENT_FEATURE == "on" && ((CREDITCARDPAYMENT_FEATURE == "on") || (INVOICEPAYMENT_FEATURE == "on"))) { ?>

                        <div class="cont_70">
                            <label for="promocode"><?=string_ucwords(system_showText(LANG_LABEL_DISCOUNTCODE))?></label>
                            <input type="text" id="promocode" name="discount_id" value="<?=$discount_id?>" maxlength="10" onblur="orderCalculate(); updateFormAction();" />
                        </div>

                        <? } ?>

                        <? if ($advertiseItem == "listing" && LISTINGTEMPLATE_FEATURE == "on" && CUSTOM_LISTINGTEMPLATE_FEATURE == "on" && !USING_THEME_TEMPLATE) { ?>

                        <div class="cont_100">

                            <label for="listing-template"><?=system_showText(LANG_LISTING_LABELTEMPLATE)?></label>

                            <select name="select_listingtemplate_id" onchange="templateSwitch(this.value);">
                                <option value=""><?=system_showText(LANG_BUSINESS);?></option>
                                <?=$listingTypeOptions;?>
                            </select>

                        </div>

                        <? } ?>
                        
                        <? if ($advertiseItem == "event") { ?>
                        <div class="cont_100">
                            <div class="cont_30">
                                <label id="startDate_label" for="startDate_label"><?=system_showText(LANG_LABEL_STARTDATE)?> * <span><?=system_showText(LANG_LABEL_REQUIRED_FIELD);?></span></label>
                                <input type="text" name="start_date" id="start_date" value="<?=$start_date?>" /> <span>(<?=format_printDateStandard()?>)</span>
                            </div>

                            <div class="cont_30">
                                <label id="eventDate_label" for="eventDate_label"><?=system_showText(LANG_LABEL_ENDDATE)?> * <span><?=system_showText(LANG_LABEL_REQUIRED_FIELD);?></span></label>
                                <input type="text" name="end_date" id="end_date" value="<?=$end_date?>" /> <span>(<?=format_printDateStandard()?>)</span>
                            </div>
                        </div>
                        <? } ?>

                    </div>

                </div>

                <? if ($advertiseItem == "listing") { ?>
                <div id="categories">

                    <div class="left textright">

                        <h3><?=system_showText(LANG_CATEGORIES_TITLE)?></h3>
                        <p>
                            <span id="extracategory_note"><?=string_ucwords(system_showText(($listingLevelObj->getFreeCategory($level) > 1) ? LANG_CATEGORY_PLURAL : LANG_CATEGORY))?> <strong><?=system_showText(LANG_FREE)?>: <?=$listingLevelObj->getFreeCategory($level)?></strong>. <?=system_showText(LANG_CATEGORIES_PRICEDESC1)?> <strong><?=system_showText(LANG_CATEGORIES_PRICEDESC2)?> <?=CURRENCY_SYMBOL?> <?=$listingLevelObj->getCategoryPrice($level)?></strong> <?=system_showText(LANG_CATEGORIES_PRICEDESC3)?></span>
                            <?=system_showText(LANG_CATEGORIES_CATEGORIESMAXTIP1)." <strong>".system_showText(LISTING_MAX_CATEGORY_ALLOWED)."</strong> ".system_showText(LANG_CATEGORIES_CATEGORIESMAXTIP2)?>
                        </p>

                    </div>

                    <div class="right">

                        <p class="warningBOXtext"><?=system_showText(LANG_CATEGORIES_MSG1)?><br /><?=system_showText(LANG_CATEGORIES_MSG2)?></p>

                        <div class="cont_50">

                            <input type="hidden" name="return_categories" value="" />

                            <div class="treeView">

                                <ul id="listing_categorytree_id_0" class="categoryTreeview">
                                    <li>&nbsp;</li>
                                </ul>

                            </div>

                        </div>

                        <div class="cont_50">
                            <label><?=system_showText(LANG_LISTING_CATEGORIES);?> * <span><?=system_showText(LANG_LABEL_REQUIRED_FIELD);?></span></label>
                            <?=$feedDropDown?>
                            <div class="standardButton" id="removeCategoriesButton" style="display:none;">
                                <a href="javascript:void(0);" onclick="JS_removeCategory(document.order_item.feed, true);"><?=(system_showText(LANG_CATEGORY_REMOVESELECTED))?></a>
                            </div>
                        </div>

                    </div>

                </div>
                <? } ?>

                <? if (!$msgLogged) { ?>

                <div id="payment-method" class="<?=$checkoutpayment_class?>">

                    <div class="left textright">
                        <h3><?=system_showText(LANG_LABEL_PAYMENT_METHOD);?></h3>
                        <p><?=system_showText(LANG_LABEL_PAYMENT_METHOD_TIP);?></p>
                    </div>

                    <div class="right">
                        <div class="option">
                            <?
                            $isAdvertise = true;
                            include(INCLUDES_DIR."/forms/form_paymentmethod.php");
                            ?>
                        </div>
                    </div>

                </div>

                <? } else { ?>
                
                <input type="hidden" name="userLogged" id="userLogged" value="1" />
                <br class="clear"/>
                <? } ?>

                <div class="blockcontinue cont_100">

                    <div id="loadingOrderCalculate" class="loadingOrderCalculate"><?=system_showText(LANG_WAITLOADING)?></div>

                    <input type="hidden" name="free_item" id="free_item" value="" />

                    <? if (PAYMENT_FEATURE == "on" && ((CREDITCARDPAYMENT_FEATURE == "on") || (INVOICEPAYMENT_FEATURE == "on"))) { ?>

                    <div id="check_out_payment" class="<?=$checkoutpayment_class?>">

                        <div class="cont_60 ">
                            <div id="checkoutpayment_total" class="orderTotalAmount"></div>
                        </div>

                        <div class="cont_40 ">
                            <p class="checkoutButton bt-highlight">                               
                                <button type="button" id="button1" onclick="<?=($msgLogged && !$hasPackage  ? "submitForm('formCurrentUser');" : "nextStep('$advertiseItem', ".($advertiseItem == "listing" ? "document.order_item.feed" : "false").", '$advertiseItem-title', ".($hasPackage ? "true" : "false").");")?>"><?=system_showText(LANG_BUTTON_CONTINUE)?></button>
                                <a href="<?=NON_SECURE_URL?>/<?=ALIAS_ADVERTISE_URL_DIVISOR?>.php?<?=$advertiseItem?>"><?=system_showText(LANG_ADVERTISE_BACK);?></a>
                                <em><?=$msgLogged;?></em>                               
                            </p>
                        </div>
                    </div>

                    <? } ?>

                    <div id="check_out_free" class="<?=$checkoutfree_class?>">

                        <div class="cont_60 ">
                            <div id="checkoutfree_total" class="orderTotalAmount"></div>
                        </div>

                        <div class="cont_40 ">
                            <p class="checkoutButton bt-highlight">                                
                                <button type="button" id="button2" onclick="<?=($msgLogged && !$hasPackage ? "submitForm('formCurrentUser');" : "nextStep('$advertiseItem', ".($advertiseItem == "listing" ? "document.order_item.feed" : "false").", '$advertiseItem-title', ".($hasPackage ? "true" : "false").");")?>"><?=system_showText(LANG_BUTTON_CONTINUE)?></button>
                                <a href="<?=NON_SECURE_URL?>/<?=ALIAS_ADVERTISE_URL_DIVISOR?>.php?<?=$advertiseItem?>"><?=system_showText(LANG_ADVERTISE_BACK);?></a>
                           		<em><?=$msgLogged;?></em>
                            </p>
                        </div>

                    </div>

                </div>

            </div>

        </div>
           
        <? if ($hasPackage) { ?>
            
        <div class="content-main order" id="screenPackage" style="display: none;">
            
            <div class="order-package">
                
                <div class="order-head">
                    
                    <h2>
                        <?=$labelName;?> - <i><?=$labelPrice;?></i><?=$labelPriceRenewal;?>
                    </h2>
                    
                </div>
                
                <? include(EDIRECTORY_ROOT."/includes/forms/form_advertise_package.php")?>	
                
            </div>				

            <div class="blockcontinue cont_100">
  
                <div class="cont_60 ">&nbsp;</div>
                
                <div class="cont_40">
                    
                    <p class="bt-highlight checkoutButton">
                        <button type="button" onclick="<?="acceptPackage('n'); ".($msgLogged ? "submitForm('formCurrentUser');" : "nextStep('$advertiseItem', ".($advertiseItem == "listing" ? "document.order_item.feed" : "false").", '$advertiseItem-title', false, true);")?>"><?=system_showText(LANG_BUTTON_CONTINUE)?></button>
                        <a href="javascript: void(0);" onclick="backStep(true);"><?=system_showText(LANG_ADVERTISE_BACK);?></a>
                    </p>
                    
                </div>

            </div>

        </div>
            
        <? } ?>

        <div class="content-main" id="screen2" <?=($message_account || $message_contact ? "style=\"display: block;\"" : "style=\"display: none;\"")?>>

            <div class="order-head">
                <ol>
                    <li class="textleft active">1 - <?=system_showText(LANG_ADVERTISE_IDENTIFICATION);?></li>
                    <li class="textcenter">2 - <?=system_showText(LANG_CHECKOUT);?></li>
                    <li class="textright">3 - <?=system_showText(LANG_ADVERTISE_CONFIRMATION);?></li>
                </ol>
            </div>

            <div class="order">

                    <div id="identification">

                        <div class="left textright">
                            <h3><?=system_showText(LANG_ALREADYHAVEACCOUNT);?></h3>
                            <p><?=system_showText(LANG_ADVERTISE_SIGNUP);?></p>
                        </div>

                        <div class="right">

                            <div class="content-custom cont_100 textcenter">
                                <h3 class="cont_50"><?=system_showText(LANG_ADVERTISE_SIGNUP_ALREADYUSER);?></h3>
                                <h3 class="cont_50"><?=system_showText(LANG_ADVERTISE_SIGNUP_NEWUSER);?></h3>
                            </div>

                            <div class="clear">&nbsp;</div>

                            <div class="cont_100 identification">

                                <div class="signup">
                                    <label><?=system_showText(LANG_ACCOUNTDIRECTORYUSER);?></label>

                                    <div class="inputimg large">
                                        <i class="inpemail"></i><input type="email" name="dir_username" id="dir_username" class="" placeholder="<?=system_showText(LANG_LABEL_USERNAME);?>" value="<?=$defaultusername?>" />
                                    </div>

                                    <div class="inputimg large">
                                        <i class="inppassword"></i><input type="password"  name="dir_password" id="dir_password" class="" placeholder="<?=system_showText(LANG_LABEL_PASSWORD);?>" value="<?=$defaultpassword?>" />
                                    </div>
                                    
                                    <p class="standardButton buttoncenter">
                                        <button type="button" onclick="submitForm('formDirectory');"><?=system_showText(LANG_BUTTON_LOGIN);?></button>
                                    </p>

                                    <? if ($facebookEnabled || $googleEnabled) { ?>

                                    <label><?=system_showText(LANG_ACCOUNTFBGOOGLEUSER);?></label>

                                    <p>
                                        <? 
                                        if ($facebookEnabled) {
                                            $summaryForm = true;
                                            $urlRedirect = "?advertise=yes&advertise_item=$advertiseItem&item_id=".$unique_id."&destiny=".urlencode(DEFAULT_URL."/".MEMBERS_ALIAS."/".constant(strtoupper($advertiseItem)."_FEATURE_FOLDER")."/".($advertiseItem == "banner" ? "add" : $advertiseItem).".php");
                                            include(INCLUDES_DIR."/forms/form_facebooklogin.php");
                                        }

                                        if ($googleEnabled) {
                                            $summaryForm = true;
                                            $urlRedirect = "&advertise=yes&advertise_item=$advertiseItem&item_id=".$unique_id."&destiny=".urlencode(DEFAULT_URL."/".MEMBERS_ALIAS."/".constant(strtoupper($advertiseItem)."_FEATURE_FOLDER")."/".($advertiseItem == "banner" ? "add" : $advertiseItem).".php");
                                            include(INCLUDES_DIR."/forms/form_googlelogin.php");
                                        }
                                        ?>
                                    </p>

                                    <? } ?>

                                    <? if ($openIDEnabled) { ?>

                                    <label><?=system_showText(LANG_ACCOUNTOPENIDUSER);?></label>

                                    <div class="standardButton btn-openid">
                                        <input type="text" class="openid" name="openidurl" id="openidurl" placeholder="<?=system_showText(LANG_LABEL_PROFILE_YOUROPENID);?>" value="<?=$openidurl?>" />
                                    	<button type="button" onclick="submitForm('formOpenID');"><?=system_showText(LANG_BUTTON_LOGIN);?></button>
                                    </div>


                                    <? } ?>
                                </div>

                                <div class="create">

                                    <?
                                    if ($message_account || $message_contact) {
                                        $msgError .= $message_contact;
                                        $msgError .= (string_strlen($msgError) ? "<br />" : "").$message_account;

                                        echo "<p class=\"errorMessage\">$msgError</p>";
                                    }
                                    ?>

                                    <div>
                                        <label><?=system_showText(LANG_ADVERTISE_CREATE_ACC);?></label>

                                        <div>
                                        	<div class="inputimg">
                                                <i class="inpname"></i><input type="text" name="first_name" placeholder="<?=system_showText(LANG_LABEL_FIRST_NAME);?>" value="<?=$first_name?>" />
                                            </div>
                                            <div class="inputimg">
                                                <i class="inpname"></i><input type="text" name="last_name" placeholder="<?=system_showText(LANG_LABEL_LAST_NAME);?>" value="<?=$last_name?>" />
                                            </div>
                                        </div>

                                        
                                        <div>
                                            <div class="inputimg large">
                                                <i class="inpemail"></i><input type="text" name="username" id="usernameSugar" placeholder="<?=system_showText(LANG_LABEL_USERNAME)?>" value="<?=$username?>" maxlength="<?=USERNAME_MAX_LEN?>" onblur="checkUsername(this.value, '<?=DEFAULT_URL;?>', 'members', 0); populateField(this.value, 'email');"/>
                                            </div>
                                            <input type="hidden" name="email" id="email" value="<?=$email?>" />
                                            <label id="checkUsername">&nbsp;</label>
                                        </div>

                                        <div class="cont_50">
                                            <div class="inputimg">
                                                <i class="inppassword"></i><input type="password" class="" placeholder="<?=system_showText(LANG_LABEL_CREATE_PASSWORD)?>" name="password" maxlength="<?=PASSWORD_MAX_LEN?>" />
                                            </div>
                                            <div class="inputimg">
                                                <i class="inppassword"></i><input type="password" class="" placeholder="<?=system_showText(LANG_LABEL_RETYPE_PASSWORD);?>" name="retype_password" />
                                            </div>
                                        </div>
                                        
                                        <? if ($showNewsletter) { ?>
                                        <div class="option">
                                            <input type="checkbox" class="checkbox" name="newsletter" value="y" <?=($newsletter || (!$newsletter && $_SERVER["REQUEST_METHOD"] != "POST")) ? "checked" : ""?> />
                                            <label><?=$signupLabel?></label>
                                        </div>
                                        <? } ?>

                                        <div class="option">
                                            <input id="checkterms" type="checkbox" class="checkbox" name="agree_tou" value="1" <?=($agree_tou) ? "checked" : ""?> />
                                            <label for="checkterms"><a rel="nofollow" href="<?=DEFAULT_URL?>/popup/popup.php?pop_type=terms" class="fancy_window_iframe"><?=system_showText(LANG_IGREETERMS)?></a></label>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>
                
                    <div class="blockcontinue cont_100">

                        <? if (PAYMENT_FEATURE == "on" && ((CREDITCARDPAYMENT_FEATURE == "on") || (INVOICEPAYMENT_FEATURE == "on"))) { ?>

                        <div id="check_out_payment_2">

                            <div class="cont_60 ">&nbsp;</div>

                            <div class="cont_40 ">
                                <p class="checkoutButton bt-highlight">
                                   <button type="submit" id="button3" name="continue" value=""><?=system_showText(LANG_BUTTON_CONTINUE)?></button>
                                   <a href="javascript: void(0);" onclick="backStep(false, <?=($hasPackage ? "true" : "false")?>);"><?=system_showText(LANG_ADVERTISE_BACK);?></a>
                               </p>
                            </div>

                        </div>

                        <? } ?>

                        <div id="check_out_free_2">

                            <div class="cont_60 ">&nbsp;</div>

                            <div class="cont_40 ">
                                <p class="checkoutButton bt-highlight">
                                     <button type="submit" id="button4" name="checkout" value="<?=system_showText(LANG_BUTTON_CONTINUE)?>"><?=system_showText(LANG_BUTTON_CONTINUE)?></button>
                              		 <a href="javascript: void(0);" onclick="backStep(false, <?=($hasPackage ? "true" : "false")?>);"><?=system_showText(LANG_ADVERTISE_BACK);?></a>
                                </p>
                            </div>

                        </div>

                    </div>

            </div>

        </div>

    </form>