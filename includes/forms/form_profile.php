<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /inclues/forms/form_profile.php
	# ----------------------------------------------------------------------------------------------------

	Facebook::getFBInstance($facebook);

    if ($accountObj->getString("is_sponsor") == "y" || SOCIALNETWORK_FEATURE == "off") {
        $statusRedirect = "&action=check_session&type=change_account&destiny=".urlencode(DEFAULT_URL."/".MEMBERS_ALIAS."/account/account.php?changeAcc");
        $urlRedirect = "?attach_account=true&is_sponsor=y&edir_account=".sess_getAccountIdFromSession()."&destiny=".urlencode(DEFAULT_URL."/".MEMBERS_ALIAS."/account/account.php?facebookattached");
    } else {
        $statusRedirect = "&action=check_session&type=change_account&destiny=".urlencode(DEFAULT_URL."/".SOCIALNETWORK_FEATURE_NAME."/edit.php?changeAcc");
        $urlRedirect = "?attach_account=true&is_sponsor=n&edir_account=".sess_getAccountIdFromSession()."&destiny=".urlencode(DEFAULT_URL."/".SOCIALNETWORK_FEATURE_NAME."/edit.php?facebookattached");
    }

    $params = array(
      'redirect_uri' => FACEBOOK_REDIRECT_URI."?fb_session=ok".$statusRedirect
    );

    $changeFBAccLink = $facebook->getLoginUrl($params); 

	if (isset($_GET["changeAcc"])) {
		$changeAccStyle =  ""; 
	} else {
		$changeAccStyle = "style=\"display: none;\"";
	}
?>

    <script type="text/javascript">
        //<![CDATA[
        
      		     
                  
        function getFacebookImage() {
            
            $('#image_fb').html("<img src=\"" + DEFAULT_URL + "/images/img_loading_big.gif\" alt=\"\" />");
            
            $.get(DEFAULT_URL + "/<?=MEMBERS_ALIAS?>/account/facebookimage.php", {
                id: '<?=sess_getAccountIdFromSession();?>'
            }, function(newImage) {
                var eURL = /http(s)?:\/\/([\w-]+\.)+[\w-]+(\/[\w- ./?%&=]*)?/
                var arrInfo = newImage.split("[FBIMG]");
                var imgSize = "";
                
                if (arrInfo[0] && eURL.exec(arrInfo[0])) {
                    $('#facebook_image').val(arrInfo[0]);
                    if (arrInfo[1] && arrInfo[2]) {
                        var w = parseInt(arrInfo[1]);
                        var h = parseInt(arrInfo[2]);
                        $('#facebook_image_height').val(h);
                        $('#facebook_image_width').val(w);

                        imgSize = " width=\"" + w + "\" ";
                        imgSize += " height=\"" + h + "\" ";
                    } else {
                        $('#facebook_image_height').val("<?=PROFILE_MEMBERS_IMAGE_HEIGHT?>");
                        $('#facebook_image_widht').val("<?=PROFILE_MEMBERS_IMAGE_WIDTH?>");
                        imgSize = " width=\"<?=PROFILE_MEMBERS_IMAGE_WIDTH?>\" ";
                        imgSize += " height=\"<?=PROFILE_MEMBERS_IMAGE_HEIGHT?>\" ";
                    }
                    $('#image_fb').html("<img src=\"" + arrInfo[0] + "\" " + imgSize + " alt=\"\" />");
                    if ($('#message').text() == "<?=system_showText(LANG_MSGERROR_ERRORUPLOADINGIMAGE);?>") {
                        $('#message').removeClass("errorMessage");
                        $('#message').text('');
                    }
                } else if (!eURL.exec(arrInfo[0])) {
                    $('#facebook_image').val("");
                    $('#image_fb').html("<img src=\"<?=DEFAULT_URL;?>/images/profile_noimage.gif\" width=\"<?=PROFILE_MEMBERS_IMAGE_WIDTH?>\" height=\"<?=PROFILE_MEMBERS_IMAGE_HEIGHT?>\" alt=\"No Image\" />");
                    $('#message').removeClass("successMessage");
                    $('#message').removeClass("informationMessage");
                    $('#message').addClass("errorMessage");
                    $('#message').text("<?=system_showText(LANG_MSGERROR_ERRORUPLOADINGIMAGE);?>");
                }
            });
        }

        function profileStatus(check_id) {
            var check = $('#' + check_id).attr('checked');

            $.post(DEFAULT_URL + "/includes/code/profile.php", {
                action: 'changeStatus',
                has_profile: check,
                account_id: '<?=sess_getAccountIdFromSession();?>',
                ajax: true
            });
            
        }

        function validateFriendlyURL(friendly_url, current_acc) {
        
            $("#URL_ok").css("display", "none");
            $("#URL_notok").css("display", "none");
        
            if (friendly_url) {

                $("#loadingURL").css("display", "");

                $.get(DEFAULT_URL + "/check_friendlyurl.php", {
                    type: 'profile',
                    friendly_url: friendly_url,
                    current_acc : current_acc
                }, function (response) {
                    if (response == "ok") {
                        $("#urlSample").html(friendly_url);
                        $("#URL_ok").css("display", "");
                        $("#URL_notok").css("display", "none");
                    } else {
                        $("#URL_ok").css("display", "none");
                        $("#URL_notok").css("display", "");
                    }
                    $("#loadingURL").css("display", "none");
                });
            } else {
                $("#URL_ok").css("display", "none");
                $("#URL_notok").css("display", "none");
            }
        }
        
        function removePhoto() {
            $.post(DEFAULT_URL + "/includes/code/profile.php", {
                action: 'removePhoto',
                account_id: '<?=sess_getAccountIdFromSession();?>',
                ajax: true
            }, function(){
                $("#linkRemovePhoto").css("display", "none");
                $('#image_fb').html("<img src=\"<?=DEFAULT_URL;?>/images/profile_noimage.gif\" width=\"<?=PROFILE_MEMBERS_IMAGE_WIDTH?>\" height=\"<?=PROFILE_MEMBERS_IMAGE_HEIGHT?>\" alt=\"No Image\" />");
            });
        }
        //]]>
    </script>
    
    <?

    $validate_demodirectoryDotCom = true;

    if (DEMO_LIVE_MODE) {
        $validate_demodirectoryDotCom = validate_demodirectoryDotCom($username, $message_demoDotCom);
    }
    
    if (!$facebook_image) {
        if ($image_id) {
            
            $imageObj = new Image($image_id, true);
            if ($imageObj->imageExists()) {
                $imgTag = $imageObj->getTag(true, PROFILE_MEMBERS_IMAGE_WIDTH, PROFILE_MEMBERS_IMAGE_HEIGHT);
            } else {
                $imgTag = "<img src=\"".DEFAULT_URL."/images/profile_noimage.gif\" width=\"".PROFILE_MEMBERS_IMAGE_WIDTH."\" height=\"".PROFILE_MEMBERS_IMAGE_HEIGHT."\" alt=\"No Image\" />";
            }
        } else {
            $imgTag = "<img src=\"".DEFAULT_URL."/images/profile_noimage.gif\" width=\"".PROFILE_MEMBERS_IMAGE_WIDTH."\" height=\"".PROFILE_MEMBERS_IMAGE_HEIGHT."\" alt=\"No Image\" />";
        }
    } else {
        if ($facebook_image) {
            if (HTTPS_MODE == "on") {
                $facebook_image = str_replace("http://", "https://", $facebook_image);
            }
            $imgTag = "<img src=\"".$facebook_image."\" width=\"".($facebook_image_width ? $facebook_image_width : PROFILE_MEMBERS_IMAGE_WIDTH)."\" height=\"".($facebook_image_height ? $facebook_image_height : PROFILE_MEMBERS_IMAGE_HEIGHT)."\" alt=\"Facebook Image\" />";
        } else {
            $imgTag = "<img src=\"".DEFAULT_URL."/images/profile_noimage.gif\" width=\"".PROFILE_MEMBERS_IMAGE_WIDTH."\" height=\"".PROFILE_MEMBERS_IMAGE_HEIGHT."\" alt=\"No Image\" />";
        }
    }
    
    $domain = new Domain(SELECTED_DOMAIN_ID);
	$domain_url = (SSL_ENABLED == "on" && FORCE_PROFILE_SSL == "on" ? "https://" : "http://").$domain->getString("url").EDIRECTORY_FOLDER."/".SOCIALNETWORK_FEATURE_NAME;
    
    include(EDIRECTORY_ROOT."/includes/code/thumbnail.php");
    ?>
        
    <div id="hiddenFields" style="display: none;">
        <input type="hidden" id="facebook_image" name="facebook_image" value="<?=$facebook_image?>" />
        <input type="hidden" id="facebook_image_height" name="facebook_image_height" value="<?=$facebook_image_height?>" />
        <input type="hidden" id="facebook_image_width" name="facebook_image_width" value="<?=$facebook_image_width?>" />
        <input type="hidden" name="image_id" value="<?=$image_id?>" />

        <!--Crop Tool Inputs-->
        <input type="hidden" name="x" id="x" />
        <input type="hidden" name="y" id="y" />
        <input type="hidden" name="x2" id="x2" />
        <input type="hidden" name="y2" id="y2" />
        <input type="hidden" name="w" id="w" />
        <input type="hidden" name="h" id="h" />
        <input type="hidden" name="image_width" id="image_width" />
        <input type="hidden" name="image_height" id="image_height" />
        <input type="hidden" name="image_type" id="image_type" />
        <input type="hidden" name="crop_submit" id="crop_submit" />

        <? if ($twitterSupport) { ?>

        <input type="hidden" name="tw_oauth_token" value="<?=$tw_oauth_token?>"/>
        <input type="hidden" name="tw_oauth_token_secret" value="<?=$tw_oauth_token_secret?>"/>
        <input type="hidden" name="tw_screen_name" value="<?=$tw_screen_name?>"/>

        <? } ?>
    </div>
    
    <div id="personal-info">
                    
        <div class="left textright">

            <h2><?=system_showText(LANG_LABEL_PROFILE_INFORMATION);?></h2>
            
            <div id="image_fb">
                <?=$imgTag;?>
            </div>
            
            
            
            <div class="hiddenFile-box">
                                                       
                <span class="hiddenFile">
                    <button type="button" id="buttonfile"><?=system_showText(LANG_LABEL_PROFILE_CHANGEPHOTO);?></button>
                    <input type="file" name="image" id="image" size="1" onchange="UploadImage('account');"/>
                </span>

            </div>
            
            <br />
            
            <? if ($image_id || $facebook_image) { ?>
                <div id="linkRemovePhoto">
                    <a href="javascript: void(0);" onclick="removePhoto();"><?=system_showText(LANG_LABEL_PROFILE_REMOVEPHOTO);?></a>
                    <br />
                </div>
            <? } ?>
            
            <? if ($accountObj->getString("facebook_username")) { ?>
            
                <a href="javascript:void(0);" onclick="getFacebookImage();">
                    <img src="<?=DEFAULT_URL?>/images/icon_facebook.gif" class="alignIMGtxt" alt=""/>
                    <?=system_showText(LANG_LABEL_IMAGE_FROM_FACEBOOK);?>
                </a>
                
            <? } ?>

        </div>

        <div class="right">
            
            <div class="cont_70">
                <label><?=system_showText(LANG_LABEL_PROFILE_DISPLAYNAME);?> <a href="javascript: void(0);">* <span><?=system_showText(LANG_LABEL_REQUIRED_FIELD);?></span></a></label>
                <input type="text" name="nickname" value="<?=$nickname?>" />	
            </div>

            <div class="cont_100">
                <label><?=system_showText(LANG_LABEL_ABOUT_ME);?></label>
                <textarea id="personal_message" name="personal_message" rows="7" cols="1"><?=$personal_message?></textarea>			
            </div>

        </div>

    </div>
        
    <div id="personal-page">

        <div class="left textright">

            <h2><?=system_showText(LANG_LABEL_PROFILE_PERSONALPAGE);?></h2> 				
            <span><?=system_showText(LANG_MSG_FRIENDLY_URL_PROFILE_TIP);?></span>

        </div>

        <div class="right">
            
            <? if ($validate_demodirectoryDotCom && $is_sponsor == 'y') { ?>
                <div class="cont_checkbox">
                    <input type="checkbox" id="has_profile" name="has_profile" <?=($has_profile == "y") ? "checked=\"checked\"": "" ?> onclick="profileStatus(this.id);" />
                    <label for="has_profile"><?=system_showText(LANG_LABEL_CREATE_PERSONAL_PAGE);?></label>
                </div>
            <? } ?>

            <div class="cont_100">
                <label><?=system_showText(LANG_LABEL_YOUR_URL);?> <a href="javascript: void(0);">* <span><?=system_showText(LANG_LABEL_REQUIRED_FIELD);?></span></a></label>
                
                <div class="checking">
                    <input type="text" name="friendly_url" id="friendly_url" value="<?=$friendly_url?>" onblur="easyFriendlyUrl(this.value, 'friendly_url', '<?=FRIENDLYURL_VALIDCHARS?>', '<?=FRIENDLYURL_SEPARATOR?>'); validateFriendlyURL(this.value, <?=(sess_getAccountIdFromSession() ? sess_getAccountIdFromSession() : 0)?>);" />
                   
                    <img id="loadingURL" src="<?=DEFAULT_URL?>/images/img_loading.gif" width="15px;" style="display: none;" />
                    <span id="URL_ok" class="positive" style="display: none;"> <img src="<?=DEFAULT_URL?>/images/ico-approve.png" border="0" alt="" /> <?=system_showText(LANG_LABEL_URLOK);?></span>
                    <span id="URL_notok" class="negative" style="display: none;"> <img src="<?=DEFAULT_URL?>/images/ico-deny.png" border="0" alt="" /> <?=system_showText(LANG_LABEL_URLNOTOK);?></span>
                </div>	 					 				
            </div>

            <div class="cont_100">
                <label class="label_100"><?=$domain_url;?>/<b id="urlSample"><?=($friendly_url ? $friendly_url : system_showText(LANG_LABEL_YOUR_URLTIP))?></b>.html</label>
            </div>

        </div>

    </div>
       
    <? if (($twitterSupport) || (FACEBOOK_APP_ENABLED == "on") && $accountObj->getString("username") != $accountObj->getString("facebook_username")) { ?>
        
    <div id="socialNetworking">

        <div class="left textright">

            <h2><?=system_showText(LANG_LABEL_FOREIGN_ACCOUNTS);?></h2>
            <span><?=system_showText(LANG_LABEL_PROFILE_SNTIP);?></span>

        </div>

        <div class="right">

            <? if ($accountObj->getString("username") != $accountObj->getString("facebook_username") && FACEBOOK_APP_ENABLED == "on") { ?>
                <div class="cont_100">                      

                    <? if ($profileObj && $profileObj->facebook_uid != '') { ?>

                        <div  class="btn-facebook" >
                        	<a href="<?=DEFAULT_URL?>/profile/edit.php?signoffFacebook"> <?=system_showText(LANG_LABEL_UNLINK_FB);?>: </a>
                        	<span class="btn-facebook-change">
									<?=$accountObj->getString("facebook_firstname")?> <?=$accountObj->getString("facebook_lastname")?>
						    </span>
                        	<br/>						
							<a href="<?=$changeFBAccLink;?>"><?=LANG_LABEL_CHANGE_ACCOUNT?> 
								
							</a>
						</div>

                        <? if (isset($_GET["facebookattached"])) { ?>
                            <p class="successMessage"><?=system_showText(LANG_LABEL_FB_SIGNFB_CONN);?></p>
                        <? } ?>

                    <? } else { ?>
                            <a class="btn-facebook" href="javascript:void(0);" id="open_facebook_form_connection"><?=system_showText(LANG_LABEL_LINK_FACEBOOK);?></a>

                            <? if ($facebookMessage) { ?>
                                <p class="successMessage"><?=$facebookMessage?></p>
                            <? } ?>

                    <? } ?>

                    <div id="option_fb_1" <?=$changeAccStyle;?>>

                        <input type="radio" name="join_facebook_option" id="join_facebook_option_import" value="facebook_import" checked="checked" onclick="setFBCookie('facebook_import')" />
                        <?=system_showText(LANG_LABEL_CONNECT_WITH_FB_AND_IMPORT)?>

                    </div>

                    <div id="option_fb_2" <?=$changeAccStyle;?>>

                        <input type="radio" name="join_facebook_option" id="join_facebook_option_link" value="facebook_connect" onclick="setFBCookie('facebook_link')" />
                        <?=system_showText(LANG_LABEL_CONNECT_WITH_FB)?>

                    </div>

                    <div id="option_fb_3" <?=$changeAccStyle;?>>
                        <div id="lFacebook">
                            <? include(INCLUDES_DIR."/forms/form_facebooklogin.php"); ?>
                        </div>
                        <div id="loadingFB" style="display:none">
                            <img src="<?=DEFAULT_URL?>/images/img_loading.gif" border="0" alt="" />
                        </div>
                    </div>

                </div>

            <? } ?>

            <? if ($twitterSupport) { ?>

                <div class="cont_100">
                    <? if (!isset($twitterInfo)) {

                        if (!isset($EpiTwitter)) {
                            $EpiTwitter = new EpiTwitter($foreignaccount_twitter_apikey, $foreignaccount_twitter_apisecret);
                        } ?>						 
                        <a class="btn-twitter" href="<?=$EpiTwitter->getAuthorizationUrl()?>"><?=system_showText(LANG_LABEL_TW_LINK)?></a>
						<? if ($twitterMessage) { ?>
                            <p class="<?=$_GET["signofftwitter"] == "success"? "successMessage": "errorMessage"?>"><?=$twitterMessage?></p>
                        <? } ?>

                        <p class="informationMessage">
                            <?=system_showText(LANG_PROFILE_TWITTER_TIP1);?>
                        </p>

                    <? } else { ?>

                        <div class="btn-twitter" > 
                        	<a  href="<?=DEFAULT_URL?>/twitter.php?signoffTwitter"><?=system_showText(LANG_LABEL_UNLINK_TW)?></a> 
                        	: <span ><?=$tw_screen_name?></span>
                        </div>
                        <? if ($twitterMessage) { ?>
                            <p class="<?=$_GET["twitter"] == "success"? "successMessage": "errorMessage"?>"><?=$twitterMessage?></p>
                        <? } ?>
				
                        <div class="cont_checkbox">
                        	<input type="checkbox" id="tw_post" name="tw_post" value="1" <?=$twpost_checked?> />
                        	<label for ="tw_post"><?=system_showText(LANG_LABEL_POSTRED)?></label>
						</div>
						
                    <? } ?>
                </div>

            <? } ?>

        </div>
        
    </div>
        
    <? }
    
    if (GEOIP_FEATURE == "on") {
    $location_GeoIP = geo_GeoIP();

    if ($location && $location_GeoIP && $facebook_uid) { ?>
        
    <div id="locationPrefs">
        
        <div class="left textright">
            <h2><?=system_showText(LANG_LABEL_LOCATIONPREF);?></h2> 				
            <span><?=system_showText(LANG_LABEL_CHOOSELOCATIONPREF);?></span>
        </div>
        
        <div class="right">
            <div class="cont_checkbox">
                
                <input id="radio_1" type="radio" name="usefacebooklocation" value="1" style="width:auto" <?=$usefacebooklocation?" checked=\"checked\" ":""?> /> <label for="radio_1"><?=system_showText(LANG_LABEL_USEFACEBOOKLOCATION)?>: <strong> <?=$location?></strong></label>
                <br />
                
                <input id="radio_2" type="radio" name="usefacebooklocation" value="0" style="width:auto" <?=!$usefacebooklocation?" checked=\"checked\" ":""?> /> <label for="radio_2" > <?=system_showText(LANG_LABEL_USECURRENTLOCATION)?>: <strong> <?=$location_GeoIP?></strong></label>
                <input type="hidden" name="location" id="location" value="<?=$location?>" />
            </div>     
        </div>
        
    </div>
    <? }
    } ?>

    <script language="javascript" type="text/javascript">
        var openformOptions = true;

        function setFBCookie(option){
            $.cookie('fb_attachOption', option, {expires: 7, path: '/'});
        }

        $(document).ready(function() {
            <?
                if ( DEFAULT_DATE_FORMAT == "m/d/Y" ) $date_format = "mm/dd/yy";
                elseif ( DEFAULT_DATE_FORMAT == "d/m/Y" ) $date_format = "dd/mm/yy";
            ?>

            $("#open_facebook_form_connection").click(function () {
                if (openformOptions){
                    $("#option_fb_1").show();
                    $("#option_fb_2").show();
                    $("#option_fb_3").show();
                    openformOptions = false;
                } else {
                    $("#option_fb_1").hide();
                    $("#option_fb_2").hide();
                    $("#option_fb_3").hide();
                    openformOptions = true;
                }
            });

            <? if (isset($_GET["changeAcc"]) || isset($_GET["signoffFacebook"]) || isset($_GET["facebookattached"]) || isset($_GET["twitter"]) || isset($_GET["signofftwitter"])) { ?>
                $('html, body, div').animate({
                    scrollTop: $("#socialNetworking").offset().top
                }, 500);
            <? } ?>

            var field_name = 'personal_message';
            var count_field_name = 'personal_message_remLen';

            var options = {
                        'maxCharacterSize': 250,
                        'originalStyle': 'originalTextareaInfo',
                        'warningStyle' : 'warningTextareaInfo',
                        'warningNumber': 40,
                        'displayFormat' : '<span><input readonly="readonly" type="text" id="'+count_field_name+'" name="'+count_field_name+'" size="3" maxlength="3" value="#left" class="textcounter" disabled="disabled" /> <?=system_showText(LANG_MSG_CHARS_LEFT)?> <?=system_showText(LANG_MSG_INCLUDING_SPACES_LINE_BREAKS)?></span>' 
                };
            $('#'+field_name).textareaCount(options);

        });
    </script>