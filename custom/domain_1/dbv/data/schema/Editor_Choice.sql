CREATE TABLE `Editor_Choice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `available` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `image_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `available` (`available`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci