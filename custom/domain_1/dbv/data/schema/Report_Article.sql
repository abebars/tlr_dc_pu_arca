CREATE TABLE `Report_Article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL DEFAULT '0',
  `report_type` tinyint(1) NOT NULL DEFAULT '0',
  `report_amount` int(11) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `report_info` (`article_id`,`report_type`,`date`),
  KEY `article_id` (`article_id`),
  KEY `report_type` (`report_type`),
  KEY `date` (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci