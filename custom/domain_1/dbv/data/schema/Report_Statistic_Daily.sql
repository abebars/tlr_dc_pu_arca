CREATE TABLE `Report_Statistic_Daily` (
  `day` date NOT NULL,
  `module` char(1) CHARACTER SET utf8 NOT NULL,
  `key` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 NOT NULL,
  `quantity` int(11) NOT NULL,
  KEY `key` (`key`),
  KEY `module` (`module`),
  KEY `day` (`day`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci