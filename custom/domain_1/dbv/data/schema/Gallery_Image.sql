CREATE TABLE `Gallery_Image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL DEFAULT '0',
  `image_id` int(11) NOT NULL DEFAULT '0',
  `image_caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumb_id` int(11) NOT NULL DEFAULT '0',
  `thumb_caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_default` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `gallery_id` (`gallery_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci