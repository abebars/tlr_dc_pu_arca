<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /theme/diningguide/body/index.php
	# ----------------------------------------------------------------------------------------------------
   
?>

    <div class="three-columns">
        
        <div class="row-fluid">
            
            <? include(system_getFrontendPath("top_items.php")); ?>
            
            <div class="row-fluid list-home">
                <? include(system_getFrontendPath("top_locations.php")); ?>
                <? include(system_getFrontendPath("top_categories.php")); ?>
            </div>
            
        </div>
        
    </div>