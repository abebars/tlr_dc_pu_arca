<?
/* ==================================================================*\
  ######################################################################
  #                                                                    #
  # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
  #                                                                    #
  # This file may not be redistributed in whole or part.               #
  # eDirectory is licensed on a per-domain basis.                      #
  #                                                                    #
  # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
  #                                                                    #
  # http://www.edirectory.com | http://www.edirectory.com/license.html #
  ######################################################################
  \*================================================================== */

# ----------------------------------------------------------------------------------------------------
# * FILE: /theme/default/frontend/search.php
# ----------------------------------------------------------------------------------------------------
//echo "est"; print_r($searchResponsive);exit;
 ?>

<form class="form search_adform" name="search_form" method="get" action="<?= $action; ?>" style="">
<?php if($searchResponsive){ ?>
    <div class="search-advanced mobi <?php if (!empty($aux_array_url[$searchPos_1]) && ($aux_array_url[$searchPos_1] != "index.php")) {
    echo 'module';
}
if (empty($aux_array_url[$searchPos_1]) || ($aux_array_url[$searchPos_1] == "index.php")) {
    echo ' at_home';
}
?>"> 

        <div class="row-fluid">

            <div class="search-button text-center">
                <button class="btn btn-info btn-search" type="submit"><?= system_showText(LANG_BUTTON_SEARCH); ?></button>
            </div>

            <? if ($hasWhereSearch) { ?>
                <div class="search-location">
                    <label class="title" for="where"><?= system_showText(LANG_LABEL_SEARCHINGFOR_WHERE); ?></label>
                    <input type="text" name="where" id="where_resp" placeholder="<?= system_showText(LANG_LABEL_LOCATIONSEARCH); ?>" value="" <?= ($waitGeoIP ? "class=\"ac_loading\" disabled=\"disabled\"" : "class=\" \"") ?> />
                </div>
<? } ?>

            <div class="search-keyword">
                <label class="title" for="keyword"><?= system_showText(LANG_LABEL_SEARCHINGFOR); ?></label>
                <input type="text" name="keyword" id="keyword_resp" placeholder="<?= system_showText(LANG_LABEL_KEYWORDSEARCH); ?>" value="<?= $keyword; ?>" />
            </div>

        </div>

    </div>

<? }if ($hasAdvancedSearch && !$searchResponsive) { ?>

        <div id="divAdvSearchFields" class="hidden-phone advanced-search">

            <div class="btn-advanced-search text-right">

                <a id="advanced-search-button" href="javascript:void(0);" onclick="showAdvancedSearch('<?= $advancedSearchItem ?>', '', true, <?= $category_id ? $category_id : 0; ?>);" class="btn-advanced">
                    <span id="advanced-search-label"><?= system_showText(LANG_SEARCH_ADVANCEDSEARCH); ?></span>
                    <span id="advanced-search-label-close" style="display:none"><?= system_showText(LANG_CLOSE); ?></span>
                </a>

            </div>

            <div class="search-options row-fluid">

                <div class="span12">

                    <div id="advanced-search" class="advanced-search-box" style="display:none;">

    <?
    //Prepare code for advanced search form
    $skipIncludeSearch = true;
    include(EDIRECTORY_ROOT . "/advancedsearch.php");
    ?>

                        <div class="row-fluid">

                            <div class="span2">
                                <label><?= system_showText(LANG_SEARCH_LABELMATCH) ?></label>
                                <div><input type="radio" name="match" value="exactmatch" class="radio" /> <?= system_showText(LANG_SEARCH_LABELMATCH_EXACTMATCH) ?></div>
                                <div><input type="radio" name="match" value="anyword" class="radio" /> <?= system_showText(LANG_SEARCH_LABELMATCH_ANYWORD) ?></div>
                                <div><input type="radio" name="match" value="allwords" class="radio" /> <?= system_showText(LANG_SEARCH_LABELMATCH_ALLWORDS) ?></div>
                            </div>

                            <div class="span10 row-fluid selectpicker">
                                <div class="span4">
                                    <label><?= system_showText(LANG_SEARCH_LABELCATEGORY) ?></label>
                                    <li style="display: table-cell;float: left;">
                                        <input type="text" style=" margin-right: 29px;width: 230px" name="keyword" id="keyword" value="<?= $keyword ?>" onkeypress="return submitenter(this, event)" <? if ((MODREWRITE_FEATURE == "on" && !$searchNotFriendly)) echo "onkeyup=\"changeFormAction('" . $action . "', this.value, " . $auxScript . ")\"" ?>  />
                                        <?
                                        if (string_strpos($_SERVER["PHP_SELF"], PROMOTION_FEATURE_FOLDER) === false || (string_strpos($_SERVER["PHP_SELF"], PROMOTION_FEATURE_FOLDER) !== false && PROMOTION_SCALABILITY_USE_AUTOCOMPLETE == "on")) {
//htmlencde($autocomplete_keyword_url,1);
                                            $js_fileLoader = system_scriptColectorOnReady("

                        $('#keyword, #keyword_resp').autocomplete(

                            '$autocomplete_keyword_url',

                                    {
                                        delay:100,

                                        dataType: 'html',

                                        minChars:" . AUTOCOMPLETE_MINCHARS . ",

                                        matchSubset:0,

                                        selectFirst:0,

                                        matchContains:1,

                                        cacheLength:" . AUTOCOMPLETE_MAXITENS . ",

                                        autoFill:false,

                                        maxItemsToShow:" . AUTOCOMPLETE_MAXITENS . ",

                                        max:" . AUTOCOMPLETE_MAXITENS . "

                                    }

                            );



                ", $js_fileLoader);
                                        }
                                        ?>
                                </div>

                                <?
                                unset($showLoc);
                                if ($_default_locations_info) {
                                    foreach ($_default_locations_info as $info) {
                                        if ($info["show"] == "y") {
                                            $showLoc = true;
                                            break;
                                        }
                                    }
                                }

                                if (${"locations" . $_non_default_locations[0]} || $showLoc) {
                                    if ($hasWhereSearch) {
                                        ?>
                                        <div class="span4">
                                            <div id="LocationbaseAdvancedSearch">
                                                <label><?= system_showText(LANG_SEARCH_LABELLOCATION) ?></label>
                                                <input type="text" style="display: inline;width: 230px; float: inherit;" name="where" id="where" value="" onfocus="emptyzip();" <?= ($waitGeoIP ? "class=\"ac_loading\" disabled=\"disabled\"" : "") ?> />
                                                <!--<input type="hidden"  name="zip" id="zip" value="" />-->

                                        <?
                                        $advanced_search = true;
                                        $newLocStyle = true;
//                                                include(EDIRECTORY_ROOT."/includes/code/load_location.php");
                                        ?>
                                            </div>
                                        </div>
            <?
        }
    }

    if ($hasWhereSearch) {
        ?>

                                    <div class="span4">
                                        <label><?= string_ucwords(ZIPCODE_LABEL) ?></label>
                                        <div class="row-fluid">
      
                                            <div class="span5">
                                                <input type="text" name="zip" value="" class="span6" />
                                   Near by 
                                            </div>
                                            
                                              <? if (ZIPCODE_PROXIMITY == "on") { ?>
                                                    <div class="span5">
                                                       
                                                    <select id="miles" class="span6" style="margin-top:5px;width:56px" name="dist">
                                                        <option value="5">5 </option>
                                                        <option value="20">20 </option>
                                                        <option value="50" selected="">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                    miles
                                                 </div>
                                    <? } ?>
                                            
                                            
                                        </div>
                                    </div>
                                    <?
                                    $js_fileLoader = system_scriptColectorOnReady("

            $('#where, #where_resp').autocomplete(
                '" . AUTOCOMPLETE_LOCATION_URL . "',
                    {
                        delay:1000,
                        minChars:" . AUTOCOMPLETE_MINCHARS . ",
                        matchSubset:0,
                        selectFirst:0,
                        matchContains:1,
                        cacheLength:" . AUTOCOMPLETE_MAXITENS . ",
                        autoFill:false,
                        maxItemsToShow:" . AUTOCOMPLETE_MAXITENS . ",
                        max:" . AUTOCOMPLETE_MAXITENS . "
                    }
                );

        ", $js_fileLoader);
                                }
                                ?>

                            </div>

                        </div>
                        <div class="search-button text-center">
                            <button class="btn btn-info btn-search" type="submit"><?= system_showText(LANG_BUTTON_SEARCH); ?></button>
                        </div>
                    </div>

                </div>

            </div>

        </div>

<? } ?>
</form>
<script>
                $(document).ready(function() {
                    $('#where, #where_resp').val($('#where, #where_resp').val().replace(/[^\x20-\xFF]/g, ''));

                    $('#where, #where_resp').bind('keyup blur', function() {
                        $(this).val($(this).val().replace(/[^\x20-\xFF]/g, ''));
                    }
                    );
                });
                function emptyzip() {
                    showAdvancedSearch('<?= $advancedSearchItem ?>', '<?= $action ?>', '<?= $action_adv ?>', '<?= $aux_template_id ?>', true);
                    if (document.getElementById("zip")) {
                        document.getElementById("zip").value = "";
                    }
                }

                function submitenter(myfield, e) {
                    var keycode;

                    if (window.event) {

                        keycode = window.event.keyCode;

                    } else if (e) {

                        keycode = e.which;

                    } else {

                        return true;

                    }



                    if (keycode == 13) {

                        submitformSearch('<?= $action ?>');

                        return false;

                    } else
                        return true;

                }
                
</script>
<?php
$js_fileLoader = system_scriptColectorOnReady("

				$.ajax({
				   type: \"GET\",
				   url: \"" . DEFAULT_URL . "/getGeoIP.php\",
				   success: function(msg){
					    $('#where, #where_resp').removeClass('ac_loading');
					    $('#where, #where_resp').prop('disabled', '');
//                                             $('#zip').val(msg.split('/')[1]);
                                           $('#where, #where_resp').val(msg.trim());
					  
                                            
                                         $('#where, #where_resp').val($('#where, #where_resp').val().replace(/[^\x20-\xFF]/g, ''));                                                

				   }
				 });

			", $js_fileLoader);
?>