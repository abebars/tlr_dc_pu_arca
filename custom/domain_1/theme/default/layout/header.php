<?php
$aux_array_url = explode("/", $_SERVER["REQUEST_URI"]);
$searchPos_1=1;
$searchPos_2 = 2;
    $searchPos_3 = 3;
    $searchPos_4 = 4;
    
    if (EDIRECTORY_FOLDER) {
        $auxFolder = explode("/", EDIRECTORY_FOLDER);
        $searchPos = count($auxFolder) - 1;
        $searchPos_1 += $searchPos;
        $searchPos_2 += $searchPos;
        $searchPos_3 += $searchPos;
        $searchPos_4 += $searchPos;
    }
/* ==================================================================*\
  ######################################################################
  #                                                                    #
  # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
  #                                                                    #
  # This file may not be redistributed in whole or part.               #
  # eDirectory is licensed on a per-domain basis.                      #
  #                                                                    #
  # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
  #                                                                    #
  # http://www.edirectory.com | http://www.edirectory.com/license.html #
  ######################################################################
  \*================================================================== */

# ----------------------------------------------------------------------------------------------------
# * FILE: /theme/default/layout/header.php
# ----------------------------------------------------------------------------------------------------

header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", FALSE);
header("Pragma: no-cache");
header("Content-Type: text/html; charset=" . EDIR_CHARSET, TRUE);

//This function returns the variables to fill in the meta tags content below. Do not change this line.
front_getHeaderTag($headertag_title, $headertag_author, $headertag_description, $headertag_keywords);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xml:lang="en" lang="en">

    <head>

        <title><?= $headertag_title ?></title>
        <meta name="author" content="<?= $headertag_author ?>" />
        <meta name="description" content="<?= $headertag_description ?>" />
        <meta name="keywords" content="<?= $headertag_keywords ?>" />
        <meta http-equiv="Content-Type" content="text/html; charset=<?= EDIR_CHARSET; ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="http://<?php echo $_SERVER[HTTP_HOST] ?>/custom/domain_1/theme/default/customStyle.css" rel="stylesheet" type="text/css" media="all">
        <!--<link href="http://toplocal.local/edirectory-migration/custom/domain_1/theme/default/customStyle.css" rel="stylesheet" type="text/css" media="all">-->


            <?
            $metatagHead = true;
            include(INCLUDES_DIR . "/code/smartbanner.php");
            ?>

            <!-- This function returns the favicon tag. Do not change this line. -->
<?= system_getFavicon(); ?>

            <!-- This function returns the search engine meta tags. Do not change this line. -->
<?= front_searchMetaTag(); ?>

            <!-- This function returns the meta tags rel="next"/rel="prev" to improve SEO on results pages. Do not change this line. -->
<?= front_paginationTags($array_pages_code, $aux_items_per_page, $hideResults, $blogHome); ?>

            <meta name="ROBOTS" content="index, follow" />

            <!-- This function includes all css files. Do not change this line. -->
            <!-- To change any style, it's better to edit the stylesheet files. -->
<? front_themeFiles(); ?>

            <!-- This function returns the Default Image style. Do not change this line. -->
<?= system_getNoImageStyle($cssfile = true); ?>

            <!-- This function reads and includes all js and css files (minimized). Do not change this line. -->
<? script_loader($js_fileLoader, $pag_content, $aux_module_per_page, $id, $aux_show_twitter); ?>

            <!--[if lt IE 9]>
            <script src="<?= DEFAULT_URL . "/scripts/front/html5shiv.js" ?>"></script>
            <![endif]-->

    </head>

    <!--[if IE 7]><body class="ie ie7"><![endif]-->
    <!--[if lt IE 9]><body class="ie"><![endif]-->
    <!-- [if false]><body><![endif]-->

    <?
    if (DEMO_LIVE_MODE && file_exists(EDIRECTORY_ROOT . "/frontend/livebar.php")) {
        include(EDIRECTORY_ROOT . "/frontend/livebar.php");
    }
    ?>

    <!-- This function returns the code warning users to upgrade their browser if they are using Internet Explorer 6. Do not change this line.  -->
<? front_includeFile("IE6alert.php", "layout", $js_fileLoader); ?>

    <div class="navbar navbar-static-top">

        <div class="header-brand container <?php if(empty($aux_array_url[$searchPos_1]) || ($aux_array_url[$searchPos_1] == 'index.php') ) { echo 'homeheader';} ?>" style="position: relative;">

            <!-- The function "system_getHeaderLogo()" returns a inline style, like style="background-image: url(YOUR LOGO URL HERE)" -->
           
 <?php if(empty($aux_array_url[$searchPos_1]) || ($aux_array_url[$searchPos_1] == "index.php") ) { ?>
            <div class="brand-logo" style=" height: auto;
    margin-left: 360px;
    margin-top:51px;
    width: auto;">
               
                 <!--<a class="brand logo" id="logo-link" href="<?= NON_SECURE_URL ?>/" target="_parent" <?= (trim(EDIRECTORY_TITLE) ? "title=\"" . EDIRECTORY_TITLE . "\"" : "") ?> style='background-image: url(" http://toplocal.local/edirectory-migration/custom/domain_1/content_files/big-logo.png");height: 241px; max-width: 343px;width: 343px;'<?//= system_getHeaderLogo(); ?>>-->

                <a class="brand logo" id="logo-link" href="<?= NON_SECURE_URL ?>/" target="_parent" <?= (trim(EDIRECTORY_TITLE) ? "title=\"" . EDIRECTORY_TITLE . "\"" : "") ?> style='background-image: url("http://<?php echo $_SERVER[HTTP_HOST] ?>/custom/domain_1/content_files/big-logo.png"); width: 343px; height: 241px; max-width: 343px;'<?//= system_getHeaderLogo(); ?>>
<?= (trim(EDIRECTORY_TITLE) ? EDIRECTORY_TITLE : "&nbsp;") ?>
                </a>
            </div>
 <?php }else{ ?>
<div class="brand-logo" >
                <a class="brand logo" id="logo-link" href="<?= NON_SECURE_URL ?>/" target="_parent" <?= (trim(EDIRECTORY_TITLE) ? "title=\"" . EDIRECTORY_TITLE . "\"" : "") ?> <?= system_getHeaderLogo(); ?>>
<?= (trim(EDIRECTORY_TITLE) ? EDIRECTORY_TITLE : "&nbsp;") ?>
                </a>
</div>
                
 <?php 
 include(system_getFrontendPath("search.php"));
 }
 if(!empty($aux_array_url[$searchPos_1]) && ($aux_array_url[$searchPos_1] != "index.php") ) { ?>
<!--            <style>
                .navbar-static-top{min-height:561px;}
            </style>-->
            <div class="navbar-inner" style="position:relative;top:90px;z-index:999;border-radius:5px;background:#3d3f41;height:50px;">

                <div class="container" style="height:50px">

                    <a class="hidden-desktop brand logo" href="<?= NON_SECURE_URL ?>/" <?= system_getHeaderMobileLogo(true); ?>>
<?= (trim(EDIRECTORY_TITLE) ? EDIRECTORY_TITLE : "&nbsp;") ?>
                    </a>

                    <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse" onclick="collapseMenu('menu');">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>


<? if ($addSearchCollapse) { ?>
                        <a class="btn btn-navbar" data-toggle="collapse" data-target=".search-collapse" onclick="collapseMenu('search');">
                            <span class="icon-search"></span>
                        </a>
<? } ?>

                    <div id="nav-collapse" class="nav-collapse collapse">
<? include(system_getFrontendPath("header_menu.php", "layout")); ?>
                    </div>

                    <div id="search-collapse" class="search-collapse collapse">
                        <?
                        if ($addSearchCollapse) {

                            $searchResponsive = true;
                            include(system_getFrontendPath("search.php"));
                            $searchResponsive = false;
                        }
                        ?>
                    </div>

                </div>

            </div>    
 <?php } ?>
            <?
            if (ACTUAL_MODULE_FOLDER == ARTICLE_FEATURE_FOLDER) {
                if ($aux_array_url[$searchPos_2]) {
                    ?>

<!--                    <div class="banner">
                        <img src="http://toplocal.local/edirectory-migration/custom/domain_1/content_files/banner_txt.jpg">
                            <p>
                                Find the best
                                <span>category</span>
                                businesses ranked by customer recommendations servicing the
                                <span>city</span>
                                ,
                                <span>state</span>
                                area here on TopLocalRated.com. Read recommendations and ratings on the best
                                <span>category</span>
                                in
                                <span>city</span>
                                . Get real customer reviews, phone numbers, maps and more!
                            </p>
                    </div>         -->
 
                <?php
                } else {
                    $addSearchCollapse = false;
                    if (string_strpos($_SERVER['REQUEST_URI'], ALIAS_LISTING_ALLCATEGORIES_URL_DIVISOR . "/") === false &&
                            string_strpos($_SERVER['REQUEST_URI'], ALIAS_CONTACTUS_URL_DIVISOR . ".php") === false &&
                            string_strpos($_SERVER['REQUEST_URI'], ALIAS_ADVERTISE_URL_DIVISOR . ".php") === false &&
                            string_strpos($_SERVER['REQUEST_URI'], "/order_") === false &&
                            string_strpos($_SERVER['REQUEST_URI'], ALIAS_FAQ_URL_DIVISOR . ".php") === false && !$hide_search) {

                        $addSearchCollapse = true;
                        include(EDIRECTORY_ROOT . "/searchfront.php");
                    }
                }
            }else {
                    $addSearchCollapse = false;
                    if (string_strpos($_SERVER['REQUEST_URI'], ALIAS_LISTING_ALLCATEGORIES_URL_DIVISOR . "/") === false &&
                            string_strpos($_SERVER['REQUEST_URI'], ALIAS_CONTACTUS_URL_DIVISOR . ".php") === false &&
                            string_strpos($_SERVER['REQUEST_URI'], ALIAS_ADVERTISE_URL_DIVISOR . ".php") === false &&
                            string_strpos($_SERVER['REQUEST_URI'], "/order_") === false &&
                            string_strpos($_SERVER['REQUEST_URI'], ALIAS_FAQ_URL_DIVISOR . ".php") === false && !$hide_search) {

                        $addSearchCollapse = true;
                        include(EDIRECTORY_ROOT . "/searchfront.php");
                    }
                }
            ?>




            <!--                        <div class="banner">
                                        <img src="http://toplocal.local/edirectory-migration/custom/domain_1/content_files/banner_txt.jpg">
                                            <p>Find the best <span>category</span> businesses ranked by customer recommendations servicing the <span>city</span>, <span>state</span> area here on TopLocalRated.com. Read recommendations and ratings on the best <span>category</span> in <span>city</span>. Get real customer reviews, phone numbers, maps and more!</p>
                                    </div>-->
        </div>
        
 <div class="navbar-inner mobi<?php if(empty($aux_array_url[$searchPos_1]) || ($aux_array_url[$searchPos_1] == "index.php")) { echo ' at_home'; } ?>">
                 
                <div class="container">
                    
                    <a class="hidden-desktop brand logo" href="<?=NON_SECURE_URL?>/" <?=system_getHeaderMobileLogo(true);?>>
                        <?=(trim(EDIRECTORY_TITLE) ? EDIRECTORY_TITLE : "&nbsp;")?>
                    </a>
                    
                    <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse" onclick="collapseMenu('menu');">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    
                    <? if ($addSearchCollapse) { ?>
                    <a class="btn btn-navbar" data-toggle="" data-target=".search-collapse" onclick="return collapseMenu('search');">
                        <span class="icon-search"></span>
                    </a>
                    <? } ?>
                    
                    <div id="nav-collapse" class="nav-collapse collapse">
                        <? include(system_getFrontendPath("header_menu.php", "layout")); ?>
                    </div>
                    
                    <div id="search-collapse" class="search-collapse collapse">
                        <? if ($addSearchCollapse) {

                            $searchResponsive =true;
                            include(system_getFrontendPath("search.php"));
                            $searchResponsive = false;
                        } ?>
                    </div>
                    
                </div>
                
            </div>
        </div>

    </div>

    <div class="image-bg">
        <?php //echo front_getBackground($customimage);   ?>

    </div>

    <div class="well container <?php if(empty($aux_array_url[$searchPos_1]) || ($aux_array_url[$searchPos_1] == "index.php")) { echo ' well_containerHome'; } ?> <?php if(!empty($aux_array_url[$searchPos_1]) && ($aux_array_url[$searchPos_1] == "contactus.php")) { echo ' well_containercontactus'; } ?> <?php if(!empty($aux_array_url[$searchPos_1]) && ($aux_array_url[$searchPos_1] == "advertise.php")) { echo ' well_containeradvertise'; } ?> <?php if(!empty($aux_array_url[$searchPos_1]) && ($aux_array_url[$searchPos_1] == "profile")) { echo ' well_containerprofile'; } ?>  <?php if(!empty($aux_array_url[$searchPos_1]) && ($aux_array_url[$searchPos_1] == "order_listing.php?level=70")) { echo ' well_containerorder_listing'; } ?> <?php if(!empty($aux_array_url[$searchPos_1]) && ($aux_array_url[$searchPos_1] == "article" && ($aux_array_url[$searchPos_2] == "results.php"))) { echo ' well_containerarticleResults'; } ?> ">

        <div class="container-fluid">

            <?
            //Breadcrumb
            front_addBreadcrumb();

            //Don't show banners for advertise pages, maintenance page and error page
            if (
                    string_strpos($_SERVER["PHP_SELF"], "/order_") === false &&
                    string_strpos($_SERVER["REQUEST_URI"], ALIAS_ADVERTISE_URL_DIVISOR . ".php") === false &&
                    string_strpos($_SERVER["PHP_SELF"], "/maintenancepage.php") === false &&
                    ACTUAL_PAGE_NAME != "errorpage.php"
            ) {
                front_includeBanner($category_id, $banner_section);
            }
            include(EDIRECTORY_ROOT . "/gtst2.php");
            $_SESSION["state"] = $GEOIP_REGION_NAME[$record->country_code][$record->region];
            $_SESSION["city"] = $record->city;
            $zipcode=$record->postal_code;
            ?>


