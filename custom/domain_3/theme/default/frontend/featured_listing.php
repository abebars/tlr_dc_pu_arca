<?
/* ==================================================================*\
  ######################################################################
  #                                                                    #
  # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
  #                                                                    #
  # This file may not be redistributed in whole or part.               #
  # eDirectory is licensed on a per-domain basis.                      #
  #                                                                    #
  # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
  #                                                                    #
  # http://www.edirectory.com | http://www.edirectory.com/license.html #
  ######################################################################
  \*================================================================== */

# ----------------------------------------------------------------------------------------------------
# * FILE: /theme/default/frontend/featured_listing.php
# ----------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------
# CODE
# ----------------------------------------------------------------------------------------------------
// Preparing markers to Full Cache
?>
<!--cachemarkerFeaturedListing-->
<?
//          include(EDIRECTORY_ROOT . "/gtst2.php");
//            $_SESSION["state"] = $GEOIP_REGION_NAME[$record->country_code][$record->region];
//            $_SESSION["city"] = $record->city;
//            $_SESSION['country']=$record->country_name;

$maxItems = 1;

$level = implode(",", system_getLevelDetail("ListingLevel"));

if ($level) {
    unset($searchReturn);
  
    $searchReturn = search_frontListingSearch($_GET, "random");
    if ($_SESSION["country"]) {
        $searchReturn["where_clause"].=" and Listing_Summary.location_1_title ='" . $_SESSION["country"] . "'";
    }
    if ($_SESSION["city"]) {
        $searchReturn["where_clause"].=" and Listing_Summary.location_4_title ='" . $_SESSION["city"] . "'";
    }
    if ($_SESSION["state"]) {
        $searchReturn["where_clause"].=" and Listing_Summary.location_3_title ='" . $_SESSION["state"] . "'";
    }
    $sql = "SELECT " . $searchReturn["select_columns"] . " FROM " . $searchReturn["from_tables"] . " WHERE " . ($searchReturn["where_clause"] ? $searchReturn["where_clause"] . " AND" : "") . " (Listing_Summary.level IN (" . $level . ")) " . ($searchReturn["group_by"] ? "GROUP BY " . $searchReturn["group_by"] : "") . " ORDER BY " . ($searchReturn["order_by"] ? $searchReturn["order_by"] : " `Listing_FeaturedTemp`.`random_number` ") . " LIMIT " . $maxItems;
      $row['Zipcode'] = '14445';  $miles = 100;
    if ($_SESSION["state"] && $_SESSION["city"]) {
         $sqll = "SELECT Zipcode FROM `zip` WHERE State = '" . $_SESSION["state"] . "' AND City = '" . $_SESSION["city"] . "' order by Zipcode desc";
        $resultzip = mysql_query($sqll);
        if ($resultzip) {
            $row = mysql_fetch_assoc($resultzip);
        }
      
    } 
    zipproximity_getWhereZipCodeProximity($row['Zipcode'], $miles, $whereZipCodeProximity, $order_by_zipcode_score);
       if($whereZipCodeProximity){
       $sql.=" and " . $whereZipCodeProximity;
       
       }
 
    $front_featured_listings = db_getFromDBBySQL("listing", $sql, "array");
}

if ($front_featured_listings) {

    $ids_report_lote = "";

    foreach ($front_featured_listings as $listing) {

        $ids_report_lote .= $listing["id"] . ",";

        $item_detail = LISTING_DEFAULT_URL . "/" . $listing["friendly_url"] . ".html";
        $item_title = $listing["title"];
        $item_description = system_showTruncatedText($listing["description"], 130);

        $imageObj = new Image($listing["image_id"]);

        if ($imageObj->imageExists()) {
            $item_image = $imageObj->getTag(false, "", "", $listing["title"], false);
        } else {
            $item_image = "";
        }
        ?>

        <div class="span6 flex-box color-3">

            <h2>
        <?= system_showText(LANG_FEATURED_LISTING_SING) ?>
                <a class="view-more" href="<?= LISTING_DEFAULT_URL ?>/"><?= system_showText(LANG_LABEL_SEE_ALL); ?></a>
            </h2>

            <a href="<?= $item_detail ?>" class="image">
        <? if ($item_image) { ?>
            <?= $item_image ?>
        <? } else { ?>
                    <span class="no-image"></span>
                <? } ?>
            </a>

            <section>
                <h5>
                    <a href="<?= $item_detail ?>">
                <?= $item_title ?>
                    </a>
                </h5>

                <p><?= $item_description ?></p>
            </section>

        </div>

                        <?
                    }
                    $ids_report_lote = string_substr($ids_report_lote, 0, -1);
                    report_newRecord("listing", $ids_report_lote, LISTING_REPORT_SUMMARY_VIEW, true);
                }

                // Preparing markers to full cache
                ?>
<!--cachemarkerFeaturedListing-->