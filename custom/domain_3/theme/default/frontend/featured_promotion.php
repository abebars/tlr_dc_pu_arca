    <?

    /*==================================================================*\
    ######################################################################
    #                                                                    #
    # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
    #                                                                    #
    # This file may not be redistributed in whole or part.               #
    # eDirectory is licensed on a per-domain basis.                      #
    #                                                                    #
    # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
    #                                                                    #
    # http://www.edirectory.com | http://www.edirectory.com/license.html #
    ######################################################################
    \*==================================================================*/

    # ----------------------------------------------------------------------------------------------------
    # * FILE: /theme/default/frontend/featured_promotion.php
    # ----------------------------------------------------------------------------------------------------

    // Preparing markers to Full Cache
    ?>
    <!--cachemarkerFeaturedDeal-->
    <?
    
    # ----------------------------------------------------------------------------------------------------
    # VALIDATE FEATURE
    # ----------------------------------------------------------------------------------------------------
    if (PROMOTION_FEATURE == "on" && CUSTOM_PROMOTION_FEATURE == "on" && CUSTOM_HAS_PROMOTION) {

        $maxItems = 1;
 if(  $_SESSION["country"]){
            $ctda = "select id from " . _DIRECTORYDB_NAME . ".Location_1  where name='" . $_SESSION['country'] . "'";
          
            $country_id = @mysql_result(mysql_query($ctda), 0);
            
           }
            if(  $_SESSION["state"]){
            $ctda = "select id from " . _DIRECTORYDB_NAME . ".Location_3  where name='" . $_SESSION['state'] . "'and location_1=".$country_id;
        
            $state_id = @mysql_result(mysql_query($ctda), 0);
             }
            if(  $_SESSION["city"]){
           $ctda = "select id from " . _DIRECTORYDB_NAME . ".Location_4  where name='" .  $_SESSION["city"] . "' and location_3=".$state_id ." and location_1=".$country_id;;
        
           $city_id = @mysql_result(mysql_query($ctda), 0);
           
            }
        unset($searchReturn);
        $searchReturn = search_frontPromotionsearch($_GET, "random");
         if( !$country_id){
                              $country_id=-1;
                  }
              $searchReturn["where_clause"].=" and  Promotion.listing_location1 =". $country_id;

                if( !$city_id){
               $city_id=-1;
                }
                                $searchReturn["where_clause"].=" and  Promotion.listing_location4 =". $city_id;

                  if( !$state_id){
         $state_id=-1;
                  }
         $searchReturn["where_clause"].=" and  Promotion.listing_location3 =". $state_id;

        $sql = "SELECT ".$searchReturn["select_columns"]." FROM ".$searchReturn["from_tables"]." ".($searchReturn["where_clause"] ? "WHERE ".$searchReturn["where_clause"] : "")." ".($searchReturn["group_by"] ? "GROUP BY ".$searchReturn["group_by"] : "")." ".($searchReturn["order_by"] ? "ORDER BY ".$searchReturn["order_by"] : "")." LIMIT ".$maxItems;
        $row['Zipcode'] = '14445';  $miles = 100;
    if ($_SESSION["state"] && $_SESSION["city"]) {
        $sqll = "SELECT Zipcode FROM `zip` WHERE State = '" . $_SESSION["state"] . "' AND City = '" . $_SESSION["city"] . "' order by Zipcode desc";
        $resultzip = mysql_query($sqll);
        if ($resultzip) {
            $row = mysql_fetch_assoc($resultzip);
        }
        } 
    zipproximity_getWhereZipCodeProximity($row['Zipcode'], $miles, $whereZipCodeProximity, $order_by_zipcode_score);
       if($whereZipCodeProximity){
       $sql.=" and " . $whereZipCodeProximity;
       }
   
        $front_featured_promotions = db_getFromDBBySQL("promotion", $sql, "array");

        if ($front_featured_promotions) {
            
            $ids_report_lote = "";

            foreach ($front_featured_promotions as $promotion) {

                $ids_report_lote .= $promotion["id"].",";

                $item_price = string_substr($promotion["dealvalue"], 0, (string_strpos($promotion["dealvalue"], ".")));
                $item_cents = string_substr($promotion["dealvalue"], (string_strpos($promotion["dealvalue"], ".")), 3);
                if ($item_cents == ".00") {
                    $item_cents = "";
                }
                if (!$item_price && !$item_cents) {
                    $item_price = system_showText(LANG_FREE);
                }
                
                $item_detail = PROMOTION_DEFAULT_URL."/".$promotion["friendly_url"].".html";
                $item_title = $promotion["name"];
                
                $item_description = system_showTruncatedText($promotion["description"], 130);
                
                $imageObj = new Image($promotion["image_id"]);
            
                if ($imageObj->imageExists()) {
                    $item_image = $imageObj->getTag(false, "", "", $promotion["name"], false);
                } else {
                    $item_image = "";
                }
                
                ?>
                    
                <div class="span6 flex-box color-1">
                    
                    <h2><?=system_showText(LANG_FEATURED_PROMOTION_SING)?> <span><?=((is_numeric($item_price) ? CURRENCY_SYMBOL : "").$item_price.$item_cents)?></span></h2>
                    
                    <a href="<?=$item_detail?>" class="image">
                        <? if ($item_image) { ?>
                            <?=$item_image?>
                        <? } else { ?>
                            <span class="no-image"></span>
                        <? } ?>
                    </a>
                    
                    <section>
                        <h5>
                            <a href="<?=$item_detail?>">
                                <?=$item_title?>
                            </a>
                        </h5>
                        
                        <p><?=$item_description?></p>
                    </section>
                </div>
    
                <?
                
            }
            $ids_report_lote = string_substr($ids_report_lote, 0, -1);
            report_newRecord("promotion", $ids_report_lote, PROMOTION_REPORT_SUMMARY_VIEW, true);
                
        }
    }
    // Preparing markers to full cache
    ?>
    <!--cachemarkerFeaturedDeal-->