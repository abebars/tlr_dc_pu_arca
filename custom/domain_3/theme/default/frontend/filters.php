<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /theme/default/frontend/filters.php
	# ----------------------------------------------------------------------------------------------------

?>    
    <script type="text/javascript">
        
        $(document).ready( function () {
            
            <? foreach ($availableFilters as $avFilter) { ?>
                 
                //Show/Hide Filter 
                $("#filter_<?=$avFilter?>").click(function() {
              
                    if ($("#list_<?=$avFilter?>").css("display") == "none") {
                        $(this).find("a").removeClass("icon-caret-right");
                        $(this).find("a").addClass("icon-caret-down");
                        $("#list_<?=$avFilter?>").slideDown("slow");
                    } else {
                        $(this).find("a").removeClass("icon-caret-down");
                        $(this).find("a").addClass("icon-caret-right");
                        $("#list_<?=$avFilter?>").slideUp("slow");
                    }
                });
            <? } ?>
        
        });
        
        function CloseFilters() {

            if ($("#return_filter").css("display") == "none") {
                
                <? foreach ($availableFilters as $avFilter) { ?>
                            
                    $("#filter_<?=$avFilter?>").find("a").removeClass("icon-caret-down");
                    $("#filter_<?=$avFilter?>").find("a").addClass("icon-caret-right");
                    $("#list_<?=$avFilter?>").css("display", "none");
                <? } ?>
                    
                $("#return_filter").slideDown("slow");
                
            } else {
                
                $("#return_filter").slideUp("slow");
            }
            
        }
        
    </script>
    
    <? if (is_array($filters) && count($filters)) { ?>

        <h2><?=system_showText(LANG_LABEL_REFINEBY);?></h2>

        <ul>

        <?  foreach ($filters as $filter) {  ?>
           
            <li class="item-filter">
                
            <? if ($filter["type"] == "location") { ?>
                
                <h3 id="filter_location">
                    <b class="title-filter"><?=system_showText(LANG_SEARCH_LABELLOCATION);?></b>
                    <a class="icon-caret-down" href="javascript:void(0);"></a>
                </h3>
                
                <div id="postLocations" style="display:none">
                    
                    <? foreach ($aux_array_filters as $key => $value) { ?>
                        <input type="hidden" name="<?=$key;?>" value="<?=htmlspecialchars($value);?>" />
                    <? } ?>
                </div>
                
                <ul id="list_location" class="item-select">
                    
                <? foreach ($filter["filters"] as $filterLoc) { ?>
                    
                    <li>
                        <input type="checkbox" class="checkbox" value="<?=$filterLoc["id"]?>" id="locations_<?=$filterLoc["id"]?>" onclick="filter_redirect(this, 'filter_location_<?=$filterLoc["level"]?>', <?=$filterLoc["id"]?>, '<?=$filterLinkLocationsJS?>',true);" <?=(in_array($filterLoc["id"], $array_locationsSelected) ? "checked=\"checked\"" : "")?>>
                        <label for="locations_<?=$filterLoc["id"]?>"><?=$filterLoc["title"]?></label>
                    </li>
                <? } ?>
                    
                </ul>
                
            <? } elseif ($filter["type"] == "category") { ?>
                
                <h3 id="filter_category">
                    <b class="title-filter"><?=system_showText(LANG_LABEL_CATEGORY);?> </b>
                    <a class="icon-caret-down" href="javascript:void(0);"></a>
                </h3>
                
                <ul id="list_category" class="item-select item-close">
                    <?=system_buildCategoriesFilter($arrayCategories, $arrayTotal, $categories, $filterLinkCategoriesJS, $aux_array_filters, false, 0, $filter_item);?>
                </ul>
                
            <? } elseif ($filter["type"] == "nonprofitcategory") { ?>
                
                <h3 id="filter_nonprofitcategory">
                    <b class="title-filter"><? echo "Cause";?> </b>
                    <a class="icon-caret-down" href="javascript:void(0);"></a>
                </h3>
                
                <ul id="list_nonprofitcategory" class="item-select item-close" style="display: block;">
                    <?=system_buildCategoriesFilterNonProfit($arrayCategories1, $arrayTotal1, $categories1, $filterLinkCategoriesJSNonprofit, $aux_array_filters, false, 0, $filter_item);?>
                </ul>
                
            <? }elseif ($filter["type"] == "deal") { ?>
                
                <h3 id="filter_deal">
                    <b class="title-filter"><?=system_showText(LANG_LABEL_DEAL_FILTER);?> </b>
                    <a class="icon-caret-down" href="javascript:void(0);"></a>
                </h3>
                
                <ul id="list_deal" class="item-select">
                    <li>
                        <label>
                            <input type="checkbox" class="checkbox" onclick="filter_redirect(this, 'filter_deal', 'yes', '<?=$filterLinkJS?>');" <?=($filter_deal == "yes" ? "checked=\"checked\"" : "")?>><?=system_showText(LANG_LABEL_FILTER_DEAL);?>
                        </label>
                    </li>
                </ul>
                
            <? } elseif ($filter["type"] == "price") { ?>
                
                <h3 id="filter_price">
                    <b class="title-filter"><?=ucwords(system_showText(LANG_LABEL_PRICE_RANGE));?> </b>
                    <a class="icon-caret-down" href="javascript:void(0);"></a>
                </h3>

                <ul id="list_price" class="item-select price">

                <? foreach ($filter["filters"] as $filterPrice) { ?>
                    
                    <li>
                        <label>
                            <input type="checkbox" class="checkbox" value="<?=$filterPrice["price"]?>" onclick="filter_redirect(this, 'filter_price', '<?=$filterPrice["price"];?>', '<?=$filterLinkPriceJS?>',true);" <?=(in_array($filterPrice["price"], $aux_filter_price) ? "checked=\"checked\"" : "")?>>
                            <? 
                            for ($k = 0; $k < $filterPrice["price"]; $k++) {
                                echo $listing_price_symbol;
                            } 
                            ?>
                            <span>(<?=$listing_price_symbol.system_showListingPrice($filterPrice["price"]);?>)</span>
                        </label>
                    </li>
                <? } ?>
                    
                </ul>

            <? } elseif ($filter["type"] == "rating") {
                
                $ratingArray = array();
                if ($rating) {
                    $ratingArray = explode("-", $rating);
                }               
                $count = 0;
                ?>

                <h3 id="filter_rating">
                    <b class="title-filter"><?=ucfirst(system_showText(LANG_LABEL_RATING));?> </b>
                    <a class="icon-caret-down" href="javascript:void(0);"></a>
                </h3>

                <ul id="list_rating" class="item-select">

                <? foreach ($filter["filters"] as $filterRating) { ?>
                    
                    <li>
                        <label>
                            <input type="checkbox" class="checkbox" id="rating_<?=$count;?>" value="<?=$filterRating["rating"];?>" onclick="filter_redirect(this, 'rating', '<?=$filterRating["rating"];?>', '<?=$filterLinkRatingJS?>', true);" <?=(in_array($filterRating["rating"], $ratingArray) ? "checked=\"checked\"" : "")?>>
                            <div class="stars-rating">
                                <div class="rate-<?=($filterRating["rating"])?>"></div>
                            </div>
                        </label>
                    </li>
                    <? 
                    $count++;
                } 
                ?>
                </ul>
                
            <? } elseif ($filter["type"] == "valid_for") { ?>
                
                <h3 id="filter_week_filter">
                    <b class="title-filter"><?=ucfirst(system_showText(LANG_LABEL_FILTER_VALID_FOR));?></b>
                    <a class="icon-caret-down" href="javascript:void(0);"></a>
                </h3>
                
                <ul id="list_week_filter" class="item-select">
                    
                <? foreach($filter["filters"] as $filter_valid) { ?>
                    <li>
                        <label>
                            <input type="checkbox" class="checkbox" value="<?=$filter_valid[0]?>" onclick="filter_redirect(this, 'filter_valid_for', '<?=$filter_valid[0]?>', '<?=$filterLinkJS?>');" <?=($filter_valid_for == $filter_valid[0] ? "checked=\"checked\"" : "")?>>
                            <?=system_showText($filter_valid[1]);?>
                        </label>
                    </li>
                <? } ?>
                    
                </ul>
                
            <? } ?>
                
            </li>
        <? } ?>
            
        </ul>
        
    <? } ?>