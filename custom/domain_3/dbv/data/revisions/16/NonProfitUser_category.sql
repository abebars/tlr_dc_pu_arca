CREATE TABLE `edirectory_domain_3`.`NonProfitUser_Category` (
`id` int( 11 ) NOT NULL AUTO_INCREMENT ,
`account_id` int( 11 ) NOT NULL ,
`category_id` int( 11 ) NOT NULL ,
`category_root_id` int( 11 ) NOT NULL ,
`category_node_left` int( 11 ) NOT NULL ,
`category_node_right` int( 11 ) NOT NULL ,
PRIMARY KEY ( `id` ) ,
KEY `account_id` ( `account_id` ) ,
KEY `category_id` ( `category_id` ) 
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;

