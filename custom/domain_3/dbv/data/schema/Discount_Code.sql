CREATE TABLE `Discount_Code` (
  `id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `expire_date` date NOT NULL,
  `recurring` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `listing` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `event` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `banner` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `classified` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `article` char(3) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci