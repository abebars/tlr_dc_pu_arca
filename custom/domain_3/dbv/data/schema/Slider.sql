CREATE TABLE `Slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `summary` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alternative_text` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title_text` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slide_order` int(11) NOT NULL,
  `target` varchar(10) CHARACTER SET utf8 NOT NULL DEFAULT 'self',
  PRIMARY KEY (`id`),
  KEY `order` (`slide_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci