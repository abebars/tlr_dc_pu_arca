CREATE TABLE `Payment_Banner_Log` (
  `payment_log_id` int(11) NOT NULL DEFAULT '0',
  `banner_id` int(11) NOT NULL DEFAULT '0',
  `banner_caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discount_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '0',
  `level_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `renewal_date` date NOT NULL DEFAULT '0000-00-00',
  `impressions` mediumint(9) NOT NULL DEFAULT '0',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  KEY `payment_log_id` (`payment_log_id`),
  KEY `banner_id` (`banner_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci