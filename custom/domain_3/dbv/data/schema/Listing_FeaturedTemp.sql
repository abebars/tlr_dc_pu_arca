CREATE TABLE `Listing_FeaturedTemp` (
  `listing_id` int(11) NOT NULL,
  `listing_level` int(11) NOT NULL,
  `random_number` bigint(15) NOT NULL,
  `status` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  KEY `Listing_FeaturedTemp` (`listing_level`,`status`,`listing_id`,`random_number`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci