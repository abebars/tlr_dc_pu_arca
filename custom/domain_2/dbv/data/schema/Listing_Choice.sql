CREATE TABLE `Listing_Choice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `editor_choice_id` int(11) NOT NULL DEFAULT '0',
  `listing_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `EditorChoice_has_Listing_FKIndex1` (`editor_choice_id`),
  KEY `EditorChoice_has_Listing_FKIndex2` (`listing_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci