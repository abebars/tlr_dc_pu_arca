CREATE TABLE `Report_Statistic` (
  `search_date` datetime NOT NULL,
  `module` char(1) CHARACTER SET utf8 NOT NULL,
  `keyword` varchar(50) CHARACTER SET utf8 NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `location_1` int(11) NOT NULL DEFAULT '0',
  `location_2` int(11) NOT NULL DEFAULT '0',
  `location_3` int(11) NOT NULL DEFAULT '0',
  `location_4` int(11) NOT NULL DEFAULT '0',
  `location_5` int(11) NOT NULL DEFAULT '0',
  `search_where` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `search_date` (`search_date`),
  KEY `module` (`module`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci