CREATE TABLE `Promotion_Redeem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `profile_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `promotion_id` int(11) NOT NULL,
  `twittered` int(11) NOT NULL,
  `facebooked` int(11) NOT NULL,
  `network_response` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `redeem_code` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `datetime` datetime NOT NULL,
  `used` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `promotion_info` (`account_id`,`promotion_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci