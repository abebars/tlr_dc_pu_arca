CREATE TABLE `Report_Listing_Monthly` (
  `listing_id` int(11) NOT NULL DEFAULT '0',
  `day` date NOT NULL,
  `summary_view` int(11) NOT NULL DEFAULT '0',
  `detail_view` int(11) NOT NULL DEFAULT '0',
  `click_thru` int(11) NOT NULL DEFAULT '0',
  `email_sent` int(11) NOT NULL DEFAULT '0',
  `phone_view` int(11) NOT NULL DEFAULT '0',
  `fax_view` int(11) NOT NULL DEFAULT '0',
  `send_phone` int(11) NOT NULL DEFAULT '0',
  `click_call` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`listing_id`,`day`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci