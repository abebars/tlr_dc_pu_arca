CREATE TABLE `Setting_Location` (
  `id` tinyint(4) NOT NULL,
  `default_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_plural` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `show` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci