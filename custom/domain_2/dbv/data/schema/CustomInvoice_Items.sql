CREATE TABLE `CustomInvoice_Items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `custominvoice_id` int(11) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `custominvoice_id` (`custominvoice_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci