CREATE TABLE `MailApp_Subscribers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `subscriber_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `subscriber_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `subscriber_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `list_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci