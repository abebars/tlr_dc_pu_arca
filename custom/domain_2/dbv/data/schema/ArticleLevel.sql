CREATE TABLE `ArticleLevel` (
  `value` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `defaultlevel` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `detail` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `images` int(3) NOT NULL DEFAULT '0',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `active` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `theme` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  PRIMARY KEY (`value`,`theme`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci