
<div class = "content-top">
    <?
    $_SESSION["kind"] = "HOME";
    if ($_SESSION["kind"] && $_SESSION["state"] && $_SESSION["city"]) {
        include(INCLUDES_DIR . "/code/slider_front.php");
        include(system_getFrontendPath("slider.php"));
    }
    ?>

</div>
<?
/* ==================================================================*\
  ######################################################################
  #                                                                    #
  # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
  #                                                                    #
  # This file may not be redistributed in whole or part.               #
  # eDirectory is licensed on a per-domain basis.                      #
  #                                                                    #
  # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
  #                                                                    #
  # http://www.edirectory.com | http://www.edirectory.com/license.html #
  ######################################################################
  \*================================================================== */

# ----------------------------------------------------------------------------------------------------
# * FILE: /theme/default/body/index.php
# ----------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------
# CODE
# ----------------------------------------------------------------------------------------------------       
//Slider
// include(EDIRECTORY_ROOT."/includes/code/slider_front.php");
//Newsletter
include(EDIRECTORY_ROOT . "/includes/code/newsletter.php");

//Facebook page
setting_get("setting_facebook_link", $setting_facebook_link);
?>

<div class="row-fluid">

<? if ($showSlider) { ?>

        <div class="span<?= ($showNewsletter ? "9" : "12") ?>">
    <? //include(system_getFrontendPath("slider.php"));  ?>
        </div>

    <? } ?>

        <? if ($showNewsletter) { ?>

        <div class="span<?= ($showSlider ? "3" : "12") ?>">
        <? include(system_getFrontendPath("newsletter.php")); ?>
        </div>

    <? } ?>

</div>

<? include(system_getFrontendPath("sitecontent_top.php")); ?>

<div class="row-fluid">

    <div class="span9">
        
   <?php     
          $getListingCateg = true;
    include(EDIRECTORY_ROOT."/includes/code/featured_promotion.php");
//    print_r($array_show_promotions);
	if (is_array($array_show_promotions)) {

        $countSpecialItem = 0;
        $lastItemStyle = 0;
        $countRowFluid = 0;
        
        for ($i = 0; $i < count($array_show_promotions); $i++) {

            $lastItemStyle++;

            if ($countSpecialItem < $specialItem) { ?>
                
            <div class="row-fluid">
                        
                <div class="span12 flex-box color-1">
                    
                    <h2>
                        <?=system_showText(LANG_FEATURED_PROMOTION_SING);?>
                        <span><?=(is_numeric($array_show_promotions[$i]["deal_price"]) ? CURRENCY_SYMBOL : "").$array_show_promotions[$i]["deal_price"].$array_show_promotions[$i]["deal_cents"];?> <b class="divisor"></b><?=$array_show_promotions[$i]["offer"]." ".system_showText(LANG_DEAL_OFF);?></span>
                    </h2>
                    
            		<div class="row-fluid">
                        
	            		<div class="span12 row-fluid">
                            
		            		<aside class="span8">
                                
		            			<a href="<?=$array_show_promotions[$i]["detailLink"]?>">
                                    <? if ($array_show_promotions[$i]["image_tag"]) { ?>
                                        <?=$array_show_promotions[$i]["image_tag"]?>
                                    <? } else { ?>
                                        <span class="no-image"></span>
                                    <? } ?>                               
    		            			<span>
                                        <h4><?=$array_show_promotions[$i]["title"]?></h4>
                                    </span>
                                </a>

		            		</aside>
                            
		            		<section>
                                
		            			<h5>
                                    <a href="<?=$array_show_promotions[$i]["detailLink"]?>">
                                        <?=$array_show_promotions[$i]["listing_title"]?>
                                    </a>
                                </h5>
                                
		            			<p><?=$array_show_promotions[$i]["description"]?></p>
                                
		            			<footer>
		            				<p><?=$array_show_promotions[$i]["categories"]?></p>
		            			</footer>
                                
		            		</section>
                            
	            		</div>
                        
            		</div>

                </div>

            </div>

            <?

                $countSpecialItem++;

            } else {
                
                $countRowFluid++;

                 if ($lastItemStyle == ($countSpecialItem + 1) || $countRowFluid == 4) { $countRowFluid = 1; ?>

                <div class="row-fluid">

                <? } ?>

                    <div class="span4 flex-box color-1">
                        <h2>
                            <?=system_showText(LANG_FEATURED_PROMOTION_SING);?>
                            <span><?=(is_numeric($array_show_promotions[$i]["deal_price"]) ? CURRENCY_SYMBOL : "").$array_show_promotions[$i]["deal_price"].$array_show_promotions[$i]["deal_cents"];?></span>
                        </h2>
                        
                        <a href="<?=$array_show_promotions[$i]["detailLink"]?>">
                            <? if ($array_show_promotions[$i]["image_tag"]) { ?>
                                <?=$array_show_promotions[$i]["image_tag"]?>
                            <? } else { ?>
                                <span class="no-image"></span>
                            <? } ?>
                        </a>
                        
                        <section>
                            <h5>
                                <a href="<?=$array_show_promotions[$i]["detailLink"]?>">
                                    <?=$array_show_promotions[$i]["title"]?>
                                </a>
                            </h5>
                            <p><?=$array_show_promotions[$i]["description"]?></p>
                        </section>
                    </div>
                    
                <? if ($lastItemStyle >= count($array_show_promotions) || $lastItemStyle == $numberOfPromotions || $countRowFluid == 3) { ?>

                </div>

                <? }
                }
        }
    } 
        
?>
<!--        <div class="row-fluid">

<? // include(system_getFrontendPath("top_categories.php", "frontend", false, LISTING_EDIRECTORY_ROOT)); ?>

        </div>-->

<!--        <div class="row-fluid">

            <? // include(system_getFrontendPath("featured_listing.php", "frontend", false, LISTING_EDIRECTORY_ROOT)); ?>


        </div>-->

<!--        <div class="row-fluid">
            <? // include(system_getFrontendPath("top_locations.php", "frontend")); ?>

        </div>-->

<!--        <div class="row-fluid">

            <? // include(system_getFrontendPath("featured_promotion.php", "frontend", false, PROMOTION_EDIRECTORY_ROOT)); ?>

        </div>-->



        <div class="row-fluid">


        </div>

    </div>

    <div class="span3">

<?php include(system_getFrontendPath("top_categories.php", "frontend", false, LISTING_EDIRECTORY_ROOT)); ?>
<div class="row-fluid visible-desktop">
<a class="btn span12 btn-success" href="http://<?php echo $_SERVER[HTTP_HOST] ?>/deal/results.php"> View all View Deals </a>
</div>
    </div>

</div>

<? include(system_getFrontendPath("sitecontent_bottom.php")); ?>