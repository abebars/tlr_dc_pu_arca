<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /theme/default/body/profile_welcome.php
	# ----------------------------------------------------------------------------------------------------

?>

    <div class="row-fluid">

        <div class="span12 flex-box-group color-4">
            
            <? include(system_getFrontendPath("socialnetwork/welcome.php")); ?>
            
            <section>
                <? include(system_getFrontendPath("sitecontent_top.php")); ?>
            </section>
            
        </div>
        
    </div>