<?

/* ==================================================================*\
  ######################################################################
  #                                                                    #
  # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
  #                                                                    #
  # This file may not be redistributed in whole or part.               #
  # eDirectory is licensed on a per-domain basis.                      #
  #                                                                    #
  # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
  #                                                                    #
  # http://www.edirectory.com | http://www.edirectory.com/license.html #
  ######################################################################
  \*================================================================== */

# ----------------------------------------------------------------------------------------------------
# * FILE: /theme/default/frontend/featured_promotion.php
# ----------------------------------------------------------------------------------------------------
// Preparing markers to Full Cache
?>
<!--cachemarkerFeaturedDeal-->
<?
# ----------------------------------------------------------------------------------------------------
# VALIDATE FEATURE
# ----------------------------------------------------------------------------------------------------
if (PROMOTION_FEATURE == "on" && CUSTOM_PROMOTION_FEATURE == "on" && CUSTOM_HAS_PROMOTION) {

    $maxItems = 1;
$getData=$_GET;
    unset($searchReturn);
    $_SESSION["country"]=$record->country_name;
   
      if ($_SESSION["country"] && $_SESSION["state"] && $_SESSION["city"]) {
   
    $country=trim($_SESSION["country"]);
    $state=trim($_SESSION["state"]);
    $city=trim($_SESSION["city"]);
   
    if($country){
            $ctda = "select id from " . _DIRECTORYDB_NAME . ".Location_1  where name='" .$country . "'";
         
            $country_id = @mysql_result(mysql_query($ctda), 0);
          
           }
      if($state){
            $ctda = "select id from " . _DIRECTORYDB_NAME . ".Location_3  where name='" . $state . "'and location_1=".$country_id;
        
            $state_id = @mysql_result(mysql_query($ctda), 0);
           
             }
            if($city){
           $ctda = "select id from " . _DIRECTORYDB_NAME . ".Location_4  where name='" .$city . "' and location_3=".$state_id ." and location_1=".$country_id;;
     
           
           $city_id = @mysql_result(mysql_query($ctda), 0);
         
            }
            if ($city_id && $state_id) {
            $dataa="select abbreviation from "._DIRECTORYDB_NAME.".Location_3 where id=".$state_id."";
	    $st=@mysql_result(mysql_query($dataa),0);
           
            $sqll = "SELECT Zipcode FROM `zip` WHERE State = '" . $st . "' AND City = '" . $city . "' order by Zipcode desc";
         //echo $sqll;exit;
            $resultzip = mysql_query($sqll);
            $row = mysql_fetch_assoc($resultzip);
            $getData["zip"]=$row['Zipcode'];
       
                    }} 
    
    $getData['dist']="50";
    $searchReturn = search_frontPromotionsearch($getData, "random");
//    print_r($searchReturn);exit;
    $sql = "SELECT " . $searchReturn["select_columns"] . " FROM " . $searchReturn["from_tables"] . " " . ($searchReturn["where_clause"] ? "WHERE " . $searchReturn["where_clause"] : "") . " " . ($searchReturn["group_by"] ? "GROUP BY " . $searchReturn["group_by"] : "") . " " . ($searchReturn["order_by"] ? "ORDER BY " . $searchReturn["order_by"] : "") . " LIMIT " . $maxItems;
    $front_featured_promotions = db_getFromDBBySQL("promotion", $sql, "array");

    if ($front_featured_promotions) {

        $ids_report_lote = "";

        foreach ($front_featured_promotions as $promotion) {

            $ids_report_lote .= $promotion["id"] . ",";

            $item_price = string_substr($promotion["dealvalue"], 0, (string_strpos($promotion["dealvalue"], ".")));
            $item_cents = string_substr($promotion["dealvalue"], (string_strpos($promotion["dealvalue"], ".")), 3);
            if ($item_cents == ".00") {
                $item_cents = "";
            }
            if (!$item_price && !$item_cents) {
                $item_price = system_showText(LANG_FREE);
            }

            $item_detail = PROMOTION_DEFAULT_URL . "/" . $promotion["friendly_url"] . ".html";
            $item_title = $promotion["name"];

            $item_description = system_showTruncatedText($promotion["description"], 130);

            $imageObj = new Image($promotion["image_id"]);

            if ($imageObj->imageExists()) {
                $item_image = $imageObj->getTag(false, "", "", $promotion["name"], false);
            } else {
                $item_image = "";
            }
            ?>

            <div class="span12 flex-box color-1">

                <h2><?= system_showText(LANG_FEATURED_PROMOTION_SING) ?> <span><?= ((is_numeric($item_price) ? CURRENCY_SYMBOL : "") . $item_price . $item_cents) ?></span></h2>
                <div class="clearfix"></div>
                <div class="row-fluid">
                    <div class="row-fluid">
                        <section>
                            <div class="span6" style="float:left">


                                <a href="<?= $item_detail ?>" class="image">
            <? if ($item_image) { ?>
                <?= $item_image ?>
            <? } else { ?>
                                        <span class="no-image"></span>
                                    <? } ?>
                                </a>
                                <span style=" background: none repeat scroll 0 0 rgba(0, 0, 0, 0.698);left: 0; padding: 12px 0; pointer-events: none;position: absolute;text-align: center;top: 47%;width: 50%;">
                                        <h4 style="color: #FFFFFF;font-size: 17px;padding: 1px 4px;text-transform: uppercase;"><?= $item_title?></h4>
                                </span>
                               
                                
                                	                               
                            </div>  
                            <div class="span6" style="float:right">    
                                <h5>
                                    <a href="<?= $item_detail ?>">
            <?= $item_title ?>
                                    </a>
                                </h5>

                                <p><?= $item_description ?></p>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <?
        }
        $ids_report_lote = string_substr($ids_report_lote, 0, -1);
        report_newRecord("promotion", $ids_report_lote, PROMOTION_REPORT_SUMMARY_VIEW, true);
    }
}
// Preparing markers to full cache
?>
<!--cachemarkerFeaturedDeal-->