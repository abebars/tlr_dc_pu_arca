<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /loadlocationfilter.php
	# ----------------------------------------------------------------------------------------------------
    
	# ----------------------------------------------------------------------------------------------------
	# LOAD CONFIG
	# ----------------------------------------------------------------------------------------------------
	include("./conf/loadconfig.inc.php");

	# ----------------------------------------------------------------------------------------------------
	# VALIDATION
	# ----------------------------------------------------------------------------------------------------
	include(EDIRECTORY_ROOT."/includes/code/validate_querystring.php");
	include(EDIRECTORY_ROOT."/includes/code/validate_frontrequest.php");

	# ----------------------------------------------------------------------------------------------------
	# CODE
	# ----------------------------------------------------------------------------------------------------

	header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", FALSE);
	header("Pragma: no-cache");
	header("Content-Type: text/html; charset=".EDIR_CHARSET, TRUE);
        
    if ($arrayGet) {
        foreach ($arrayGet as $key => $value) {
            $valInfo = explode(",", $value);
            if (get_magic_quotes_gpc()) {
                $valInfo[1] = stripslashes($valInfo[1]);
            }
            $postLocations[$valInfo[0]] = $valInfo[1];
        }
        unset($_POST["arrayGet"]);
    }
    
    //Array with all parameters within the query string
    $parametersLocations = array();

    foreach ($postLocations as $key => $value) {
        //Removing useless parameters
        if ($key != "screen" && $key != "letter" && $key != "filter_item") {
            $parametersLocations[] = $key."=".$value;
        }
    }

    if ($filter_item == LISTING_FEATURE_FOLDER) {
        
        //Search with all parameters
        $searchReturn = search_frontListingSearch($postLocations, "listing_results");

        //URL to concat filters with all parameters
        $filterLink = LISTING_DEFAULT_URL."/results.php?".str_replace("'", "\'", implode("&", $parametersLocations));
        
        $locationField = "location_";
    
    } elseif ($filter_item == PROMOTION_FEATURE_FOLDER) {
        
        //Search with all parameters
        $searchReturn = search_frontPromotionSearch($postLocations, "promotion_results");

        //URL to concat filters with all parameters
        $filterLink = PROMOTION_DEFAULT_URL."/results.php?".str_replace("'", "\'", implode("&", $parametersLocations));
        
        $locationField = "listing_location";
        
    }
    //Get location ids according to search params
    
    $_locations = explode(",", EDIR_LOCATIONS);
    system_retrieveLocationRelationship($_locations, $_location_level, $_location_father_level, $_location_child_level);
    
    $db = db_getDBObject();   
    $sql = "    SELECT ".$locationField.$_location_level." AS location_id";
                
    if ($_location_child_level) {
        $sql .= ", ".$locationField.$_location_child_level." AS location_child_id";
    }

    $sql .= "
                FROM ".$searchReturn["from_tables"]." 

                WHERE ".$searchReturn["where_clause"]." AND ".$locationField.$_location_level." > 0";

    $result = $db->unbuffered_query($sql);

    $locIds = array();
    $locChildIds = array();
    $locTotal = array();
    while ($row = mysql_fetch_assoc($result)) {
        if (!in_array($row["location_id"], $locIds )) {
            $locIds[] = $row["location_id"];
        }
//        $locTotal[$row["location_id"]]++;
        if ($row["location_child_id"]) {
            $locChildIds[$row["location_id"]][] = $row["location_child_id"];
        }
    }
    mysql_free_result($result);
    $locIds = array_unique($locIds);
    
    $selectedLoc = $postLocations["filter_location_".$_location_level];

    $return = system_buildLocationsFilter($_location_level, $_location_father_level_id, $locIds, $locTotal, $filterLink, $selectedLoc, $postLocations, $locChildIds, $filter_item);
   
	echo $return;

?>