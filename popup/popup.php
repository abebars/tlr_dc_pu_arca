<?
	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /popup/popup.php
	# ----------------------------------------------------------------------------------------------------

    if ($_GET["domain_id"] || $_POST["domain_id"]) {
        define("SELECTED_DOMAIN_ID", $_GET["domain_id"] ? $_GET["domain_id"] : $_POST["domain_id"]);
    }
    
	# ----------------------------------------------------------------------------------------------------
	# LOAD CONFIG
	# ----------------------------------------------------------------------------------------------------
	include("../conf/loadconfig.inc.php");
    
    header("Content-Type: text/html; charset=utf-8", TRUE);
	header("Accept-Encoding: gzip, deflate");
    header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check", FALSE);
    header("Pragma: no-cache");

	# ----------------------------------------------------------------------------------------------------
	# SESSION
	# ----------------------------------------------------------------------------------------------------
    session_start();
    
	# ----------------------------------------------------------------------------------------------------
	# VALIDATION
	# ----------------------------------------------------------------------------------------------------
	include(EDIRECTORY_ROOT."/includes/code/validate_querystring.php");
    $pop_type = $_GET["pop_type"] ? $_GET["pop_type"] : $_POST["pop_type"];
    if ($pop_type != "reviewformpopup") {
        include(EDIRECTORY_ROOT."/includes/code/validate_frontrequest.php");
    }      

	# ----------------------------------------------------------------------------------------------------
	# CODE
	# ----------------------------------------------------------------------------------------------------
    extract($_GET);
    extract($_POST);
    
    if (file_exists(EDIRECTORY_ROOT."/includes/code/$pop_type.php")) {
        include(EDIRECTORY_ROOT."/includes/code/$pop_type.php");
    }
    
    $extraStyle = "";
    $aux_modal_box = "";
    
    if (string_strpos($pop_type, "clicktocall") !== false || string_strpos($pop_type, "sendtophone") !== false) {
       $extraStyle = "modal-content-small";
    } elseif (string_strpos($pop_type, "profile_login") !== false) {
        $extraStyle = "login";
        $aux_modal_box = "profileLogin"; 
    } elseif ($pop_type == "uploadimage") {
        $extraStyle = "modal-content-upload";
    } elseif ($pop_type == "deal_redeem") {
        $extraStyle = "modal-deal-redeem";
    }
    
    $loadMembersCss = false;
    $isPopup = true;
    $arrayMembersCss = array(0 => "uploadimage", 1 => "custominvoice_items", 2 => "package_items", 3 => "twilio_report");
    if (in_array($pop_type, $arrayMembersCss)) {
        $loadMembersCss = true;
    }

	header("Content-Type: text/html; charset=".EDIR_CHARSET, TRUE);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

	<head>

		<meta http-equiv="Content-Type" content="text/html; charset=<?=EDIR_CHARSET;?>" />
        
        <meta name="ROBOTS" content="noindex, nofollow" />

		<?
		include(THEMEFILE_DIR."/".EDIR_THEME."/".EDIR_THEME.".php");
		
        if (string_strpos($pop_type, "review") !== false || $pop_type == "deal_redeem" || $pop_type == "uploadimage") { ?>
            <script src="<?=DEFAULT_URL?>/scripts/jquery.js" language="javascript" type="text/javascript"></script>
            
        <? } elseif ($pop_type == "profile_login") { ?>
            
            <script src="<?=DEFAULT_URL?>/scripts/front/jquery-1.8.3.min.js" language="javascript" type="text/javascript"></script>
            
            <? if (THEME_USE_BOOTSTRAP) { ?>
                
                <script src="<?=DEFAULT_URL?>/scripts/front/bootstrap.min.js" language="javascript" type="text/javascript"></script>
                <script src="<?=DEFAULT_URL?>/scripts/jquery/bootstrap-select/bootstrap-select.min.js" language="javascript" type="text/javascript"></script>
                
                <script type="text/javascript" language="javascript">
                
                $('document').ready(function() {
                    $('.selectpicker .select').selectpicker();
                });
                
                </script>

            <? } ?>
            
        <? }
        
        if ($pop_type == "uploadimage") { ?>
            
            <script type="text/javascript" src="<?=DEFAULT_URL?>/scripts/jquery/jcrop/js/jquery.Jcrop.js"></script>
            <link rel="stylesheet" href="<?=DEFAULT_URL?>/scripts/jquery/jcrop/css/jquery.Jcrop.css" type="text/css" />
            
        <? }
        
        if (string_strpos($pop_type, "review") !== false) { ?>
        
            <script type="text/javascript" language="javascript">
		
                function setDisplayRatingLevel(level) {
                    for(i = 1; i <= 5; i++) {
                        var starImg = "img_rate_star_off.gif";
                        if( i <= level ) {
                            starImg = "img_rate_star_on.gif";
                        }
                        var imgName = 'star'+i;
                        document.images[imgName].src="<?=DEFAULT_URL?>/images/content/"+starImg;
                    }
                }
                
                function resetRatingLevel() {
                    setDisplayRatingLevel(document.rate_form.rating.value);
                }
                
                function setRatingLevel(level) {
                    document.rate_form.rating.value = level;
                }
                
                function disabledReviewButton(disable) {
                    if (disable) {
                        $("#submitReview").css("cursor", "default");
                        $("#submitReview").attr("disabled", "disabled");
                        document.getElementById('submitReview').innerHTML = "<?=system_showText(LANG_WAITLOADING);?>";
                    } else {
                        document.getElementById('submitReview').innerHTML = "<?=system_showText(LANG_BUTTON_SEND);?>";
                        $("#submitReview").attr("disabled", "");
                        $("#submitReview").css("cursor", "pointer");
                    }
                }
                
                $('img[name=star]').bind('click', function(){
                    $(this).fadeOut(50);
                    $(this).fadeIn(50);
                });

                $('document').ready(function() {

                    $('form').submit(function() {

                        <? setting_get("review_manditory", $reviewMandatory);?>
                        var reviewMandatory = "<?=$reviewMandatory?>";
                        var valid_email = new RegExp('^.+@.+\\..+$');
                        var top = 50;
                        var position = 400;

                        $('#JS_errorMessage').empty();
                        $('.errorMessage').css('display', 'none');

                        if ($('#rating').val() == '') {
                            $('#JS_errorMessage').append('<?=str_replace("'", "\\'", system_showText(LANG_MSG_REVIEW_SELECTRATING))?><br />\n');
                            position +=15;
                            top -=1;    
                        }
                        if (reviewMandatory == "on") {
                            if ($('#reviewer_name').val() == '' || $('#reviewer_email').val() == '') {
                                $('#JS_errorMessage').append('<?=str_replace("'", "\\'", system_showText(LANG_MSG_REVIEW_NAMEEMAILREQUIRED))?><br />\n');
                                position +=15;
                                top -=1;
                            } else if ($('#reviewer_email').val().search(valid_email) == -1) {
                                $('#JS_errorMessage').append('<?=str_replace("'", "\\'", system_showText(LANG_MSG_REVIEW_TYPEVALIDEMAIL))?><br />\n');
                            }
                        }
                        if ($('#reviewer_location').val() == '') {
                            $('#JS_errorMessage').append('<?=str_replace("'", "\\'", system_showText(LANG_MSG_REVIEW_CITYSTATEREQUIRED))?><br />\n');
                            position +=15;
                            top -=1;
                        }
                        if ($('#review_title').val() == '' || $('#review').val() == '') {
                            $('#JS_errorMessage').append('<?=str_replace("'", "\\'", system_showText(LANG_MSG_REVIEW_COMMENTREQUIRED))?><br />\n');
                            position +=15;
                            top -=1;
                        }

                        if ($('#JS_errorMessage').text() == "") {
                            $('#JS_errorMessage').css('display', 'none');    
                        } else {
                            $('#JS_errorMessage').css('display', '');
                            $('#TB_ajaxContent').css('height', position);
                            $('#TB_window').css('top', top+'%');
                            disabledReviewButton(false);
                            return false;
                        }
                        disabledReviewButton(true);
                        return true;

                    });    

                });

            </script>
        
        <? } elseif ($return_message && (string_strpos($pop_type, "clicktocall") !== false || string_strpos($pop_type, "sendtophone") !== false)) { ?>
            
			<script language="javascript" type="text/javascript">
				setTimeout(function(){
                    parent.$.fancybox.close();
				}, 6000)
			</script>
            
		<? } elseif (string_strpos($pop_type, "profile_login") !== false) {
                $aux_modal_box = true;
        ?>
            <script type="text/javascript">

                function changeForm<?=$randomId?>(value) {
                    if (value == "edirectory") {
                        $("#popLEdirectory<?=$randomId?>").addClass('isVisible');
                        $("#popLEdirectory<?=$randomId?>").removeClass('isHidden');

                        $("#popLOpenid<?=$randomId?>").addClass('isHidden');
                        $("#popLOpenid<?=$randomId?>").removeClass('isVisible');

                        $("#popLFacebook<?=$randomId?>").addClass('isHidden');
                        $("#popLFacebook<?=$randomId?>").removeClass('isVisible');

                        $("#popLGoogle<?=$randomId?>").addClass('isHidden');
                        $("#popLGoogle<?=$randomId?>").removeClass('isVisible');
                    } else if (value == "openid") {
                        $("#popLEdirectory<?=$randomId?>").addClass('isHidden');
                        $("#popLEdirectory<?=$randomId?>").removeClass('isVisible');

                        $("#popLOpenid<?=$randomId?>").addClass('isVisible');
                        $("#popLOpenid<?=$randomId?>").removeClass('isHidden');

                        $("#popLFacebook<?=$randomId?>").addClass('isHidden');
                        $("#popLFacebook<?=$randomId?>").removeClass('isVisible');

                        $("#popLGoogle<?=$randomId?>").addClass('isHidden');
                        $("#popLGoogle<?=$randomId?>").removeClass('isVisible');
                    } else if (value == "facebook") {
                        $("#popLEdirectory<?=$randomId?>").addClass('isHidden');
                        $("#popLEdirectory<?=$randomId?>").removeClass('isVisible');

                        $("#popLOpenid<?=$randomId?>").addClass('isHidden');
                        $("#popLOpenid<?=$randomId?>").removeClass('isVisible');

                        $("#popLFacebook<?=$randomId?>").addClass('isVisible');
                        $("#popLFacebook<?=$randomId?>").removeClass('isHidden');

                        $("#popLGoogle<?=$randomId?>").addClass('isHidden');
                        $("#popLGoogle<?=$randomId?>").removeClass('isVisible');
                    } else if (value == "google") {
                        $("#popLEdirectory<?=$randomId?>").addClass('isHidden');
                        $("#popLEdirectory<?=$randomId?>").removeClass('isVisible');

                        $("#popLOpenid<?=$randomId?>").addClass('isHidden');
                        $("#popLOpenid<?=$randomId?>").removeClass('isVisible');

                        $("#popLFacebook<?=$randomId?>").addClass('isHidden');
                        $("#popLFacebook<?=$randomId?>").removeClass('isVisible');

                        $("#popLGoogle<?=$randomId?>").addClass('isVisible');
                        $("#popLGoogle<?=$randomId?>").removeClass('isHidden');
                    }
                }

                function urlRedirect(url) {
                    window.parent.location = url;
                }
            </script>        
            
         <? } elseif ($pop_type == "deal_redeem") { ?>
                    
            <script language="javascript" type="text/javascript">
                function print_page() {
                    $("#bt_print").hide();
                    $("#errorMessage").hide();
                    window.print();
                    window.onfocus = function() { $("#bt_print").show();  $("#errorMessage").show(); }
                }
                
                <? if ($newdealsDone) { ?>
                    parent.updateDeals(<?=$newdealsDone?>, <?=$newdealsLeft?>);
                <? } ?>
            </script>      
                    
         <? } elseif ($pop_type == "uploadimage") {
            
                if ($upload_image == "failed") { ?>
                    <script language="javascript" type="text/javascript">
                        setTimeout(function(){
                             parent.$.fancybox.close();
                        }, 1500);

                    </script>
                <? } else {
                    
                        if (($onlyMainImage) || ($main == "false")) {

                            if ($uploadImageUpdate == "y") { ?>
                                <script language="javascript" type="text/javascript">
                                    parent.loadGallery(<?=$_POST["item_id"]?>,'y','members','editFe', 'false');
                                    setTimeout(function(){
                                         parent.$.fancybox.close();
                                    }, 1500)
                                </script>
                            <? } elseif ($uploadImageUpdate == "n") { ?>
                                <script language="javascript" type="text/javascript">
                                    parent.loadGallery(<?=$_POST["item_id"]?>, 'y', 'members', 'n', 'false');
                                    setTimeout(function(){
                                         parent.$.fancybox.close();
                                    }, 1500)
                                </script>
                            <? }
                        } else {

                            if (($uploadImageUpdate == "y") || ($uploadImageUpdate == "n")) { ?>
                                <script language="javascript" type="text/javascript">
                                    parent.loadGallery(<?=$_POST["item_id"]?>,'y','members', '', 'true');
                                    setTimeout(function(){
                                         parent.$.fancybox.close();
                                    }, 1500)
                                </script>
                            <? }
                    }
                }
          } ?>  
        
	</head>

	<body>
	
		<? include(system_getFrontendPath("popup.php", "frontend")); ?>

	</body>
    
</html>