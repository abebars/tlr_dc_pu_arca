    <?

            /*==================================================================*\
            ######################################################################
            #                                                                    #
            # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
            #                                                                    #
            # This file may not be redistributed in whole or part.               #
            # eDirectory is licensed on a per-domain basis.                      #
            #                                                                    #
            # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
            #                                                                    #
            # http://www.edirectory.com | http://www.edirectory.com/license.html #
            ######################################################################
            \*==================================================================*/

            # ----------------------------------------------------------------------------------------------------
            # * FILE: /frontend/socialnetwork/add_account.php
            # ----------------------------------------------------------------------------------------------------

    ?>

<?php $listingLevelObj = new ListingLevel();

		
                $feedDropDown1 = "<select name='feed1' id='feed1' multiple size='5' style=\"width:375px\">";
		
		$feedDropDown1 .= "</select>";
	
    ?>
<style>.categoryTreeview li{
  list-style:none;  
}</style>
	<script language="javascript" type="text/javascript" src="<?=DEFAULT_URL?>/scripts/categorytree.js"></script>
<script language="javascript" type="text/javascript">
    	<?php     if(SELECTED_DOMAIN_ID==3){
            ?>	
            $(document).ready(function(){
                loadCategoryTreeNonProfit('all', 'nonprofitlisting_', 'NonProfitListingCategory', 0, 0, '<?=DEFAULT_URL."/".EDIR_CORE_FOLDER_NAME."/".LISTING_FEATURE_FOLDER?>',<?=SELECTED_DOMAIN_ID?>);
    
            })
        <?php } ?>
        
         
        function JS_addCategory1(id) {

		
               
		var  feed = document.add_account.feed1;
          
		
		var text = unescapeHTML($("#nonprofitliContent"+id).html());
		
			var flag=true;
			for (i=0;i<feed.length;i++)
				if (feed.options[i].value==id)
					flag=false;

			if(text && id && flag){
				feed.options[feed.length] = new Option(text, id);
				$('#nonprofitcategoryAdd'+id).after("<span class=\"categorySuccessMessage\"><?=system_showText(LANG_MSG_CATEGORY_SUCCESSFULLY_ADDED)?></span>").css('display', 'none');
				$('.categorySuccessMessage').fadeOut(5000);
			} else {
				if (!flag) $('#nonprofitcategoryAdd'+id).after("</a> <span class=\"categoryErrorMessage\"><?=system_showText(LANG_MSG_CATEGORY_ALREADY_INSERTED)?></span> </li>");
				else ('#nonprofitcategoryAdd'+id).after("</a> <span class=\"categoryErrorMessage\"><?=system_showText(LANG_MSG_SELECT_VALID_CATEGORY)?></span> </li>");
			}
		

        $('#removeCategoriesButton1').show(); 

	}
        
     function JS_removeCategory1(feed, order) {

    if (feed.selectedIndex >= 0) {
        $('#nonprofitcategoryAdd'+feed.options[feed.selectedIndex].value).after($('.categorySuccessMessage').empty());
        $('#nonprofitcategoryAdd'+feed.options[feed.selectedIndex].value ).fadeIn(500);
        feed.remove(feed.selectedIndex);
        if (order){
            orderCalculate();
        }
    }
    
	if(feed.length == 0){
    	$('#removeCategoriesButton1').hide();
    }

}
   
   
	function JS_submit() {
		
                <?php if(SELECTED_DOMAIN_ID==3){ ?>
                        feed1 = document.add_account.feed1;
                        return_categories_nonprofit= document.add_account.return_categories_nonprofit;
		if(return_categories_nonprofit.value.length > 0) return_categories_nonprofit.value="";

		for (i=0;i<feed1.length;i++) {
			if (!isNaN(feed1.options[i].value)) {
				if(return_categories_nonprofit.value.length > 0)
				return_categories_nonprofit.value = return_categories_nonprofit.value + "," + feed1.options[i].value;
				else
			return_categories_nonprofit.value = return_categories_nonprofit.value + feed1.options[i].value;
			}
		}
            
                <?php } ?>
		

		document.add_account.submit();
	}
</script>

            <?php 
        if (!$hideAddLabel) { ?>

        <h2><?=LANG_CREATE_MEMBER_PROFILE?></h2>

        <? }

        include(INCLUDES_DIR."/code/newsletter.php");

            $formloginaction = ((SSL_ENABLED == "on" && FORCE_MEMBERS_SSL == "on") ? SECURE_URL : NON_SECURE_URL)."/".MEMBERS_ALIAS."/login.php?destiny=".EDIRECTORY_FOLDER."/".SOCIALNETWORK_FEATURE_NAME."/";
//print_r($formloginaction);exit;
            unset($openIDEnabled, $facebookEnabled, $googleEnabled);

            setting_get("foreignaccount_openid", $foreignaccount_openid);
            if ($foreignaccount_openid == "on") {
                    $openIDEnabled = true;
            }

            setting_get("foreignaccount_google", $foreignaccount_google);
            if ($foreignaccount_google == "on") {
                    $googleEnabled = true;
            }

            if (FACEBOOK_APP_ENABLED == "on") {
                    $facebookEnabled = true;
            }
       
            if ((string_strlen(trim($message_account)) > 0) || (string_strlen(trim($message_contact)) > 0) ) { ?>
                    <p class="errorMessage">
                <? if (string_strlen(trim($message_contact)) > 0) { ?>
                                    <?=$message_contact?>
                            <? } ?>
                <? if ((string_strlen(trim($message_contact)) > 0) && (string_strlen(trim($message_account)) > 0)) { ?>
                                    <br />
                            <? } ?>
                            <? if (string_strlen(trim($message_account)) > 0) { ?>
                                    <?=$message_account?>
                            <? } ?>
                    </p>
            <? } ?>

        <script language="javascript" type="text/javascript" src="<?=DEFAULT_URL?>/scripts/checkusername.js"></script>

        <? if (FACEBOOK_APP_ENABLED == "on" || $openIDEnabled == "on" || $googleEnabled == "on") { ?>
        <div class="left create-profile content-custom title textcenter">

            <h3 class="left signup"><?=system_showText(LANG_LABEL_PROFILE_SIGNUP);?></h3>      
            <h3 class="left or"><?=system_showText(LANG_LABEL_PROFILE_OR);?></h3>
            <h3 class="left signin"><?=system_showText(LANG_LABEL_PROFILE_FOREIGNACC);?></h3>

        </div>

        <? } ?>

        <div class="clear">&nbsp;</div>

        <div class="content-custom create-profile cont_100">

            <div class="<?=(FACEBOOK_APP_ENABLED == "on" || $openIDEnabled == "on" || $googleEnabled == "on" ? "cont_50" : "cont_100")?>">

                <form class="" id="add_account" name="add_account" action="<?=system_getFormAction($_SERVER["PHP_SELF"])?>" method="post">
                    <div>
                        <div class="inputimg"><i class="inpname"></i><input type="text" name="first_name" placeholder="<?=system_showText(LANG_LABEL_FIRST_NAME);?>" value="<?=$first_name?>" /></div>
                        <div class="inputimg"><i class="inpname"></i><input type="text" name="last_name"  placeholder="<?=system_showText(LANG_LABEL_LAST_NAME);?>" value="<?=$last_name?>" /></div>
                    </div>

                    <div>
                        <div class="inputimg large"><i class="inpemail"></i><input type="text" name="username" placeholder="<?=system_showText(LANG_LABEL_USERNAME)?>" id="username" value="<?=$username?>" maxlength="<?=USERNAME_MAX_LEN?>" onblur="checkUsername(this.value, '<?=DEFAULT_URL;?>', 'members', 0); populateField(this.value,'email');"/></div>
                        <input type="hidden" name="email" id="email" value="<?=$email?>" />
                        <label id="checkUsername"></label>
                    </div>

                    <div>
                        <div class="inputimg"><i class="inppassword"></i><input type="password"  placeholder="<?=system_showText(LANG_LABEL_CREATE_PASSWORD)?>" name="password" maxlength="<?=PASSWORD_MAX_LEN?>" /></div>
                        <div class="inputimg"><i class="inppassword"></i><input type="password"  placeholder="<?=system_showText(LANG_LABEL_RETYPE_PASSWORD);?>" name="retype_password" /></div>
                    </div>

                    <? if ($showNewsletter) { ?>
                    <div>
                        <input type="checkbox" class="checkbox" name="newsletter" value="y" <?=($newsletter || (!$newsletter && $_SERVER["REQUEST_METHOD"] != "POST")) ? "checked" : ""?> />
                        <label><?=$signupLabel?></label>
                    </div>
                     <br class="clear" />
                    <? } ?>

                   <br class="clear" />
  
 <?php //non profit
                if(SELECTED_DOMAIN_ID==3){ ?>
                   <table>
                <tr>
                    <th colspan="2" class="standard-tabletitle" style="text-align:left;">			
			<?=system_showText(LANG_CATEGORIES_SUBCATEGS1)?>
			
		</th>
	</tr>
	<tr>
		<td colspan="2" class="standard-tableContent">
                    <p class="warningBOXtext" style=" color: #494949; font-size: 14px;"><?=system_showText(LANG_MSG_ONLY_SELECT_SUBCAUSE)?> <?=system_showText(USER_MAX_CAUSE_ALLOWED);?><?=system_showText(LANG_MSG_ONLY_SELECT_SUBCAUSE1)?><br /></p>
		</td>
	</tr>
                
		<input type="hidden" name="return_categories_nonprofit" value="" />
                <tr>
			<td colspan="2" class="treeView">
				<ul id="nonprofitlisting_categorytree_id_0" class="categoryTreeview">&nbsp;</ul>

				<table width="100%" border="0" cellpadding="2" class="tableCategoriesADDED" cellspacing="0">
					<tr>
						<th colspan="2" class="tableCategoriesTITLE alignLeft"><i><span>Your Choosen Causes</span></i> </th>
					</tr>
					<tr id="msgback2" class="informationMessageShort">
						<td colspan="2" style="width: auto;"><p><img width="16" height="14" src="<?=DEFAULT_URL?>/images/icon_atention.gif" alt="<?=LANG_LABEL_ATTENTION?>" title="<?=LANG_LABEL_ATTENTION?>" /> <?=system_showText(LANG_MSG_CLICKADDTOSELECTCATEGORIES);?></p></td>
					</tr>
					<tr id="msgback" class="informationMessageShort" style="display:none">
						<td colspan="2"><i>  <span>Your Choosen Causes</span></i> <div><img width="16" height="14" src="<?=DEFAULT_URL?>/images/icon_atention.gif" alt="<?=LANG_LABEL_ATTENTION?>" title="<?=LANG_LABEL_ATTENTION?>" /></div><p><?=system_showText(LANG_MSG_CLICKADDTOADDCATEGORIES);?></p></td>
					</tr>
					<tr>
						<td colspan="2" class="tableCategoriesCONTENT"><?=$feedDropDown1?></td>
					</tr>
					<tr id="removeCategoriesButton1" style='display:none'>
						<td class="tableCategoriesBUTTONS" colspan="2">
							<center>
                                                            <button class="input-button-form"  style="background-color: #494949;" type="button" value="<?=system_showText(LANG_BUTTON_REMOVESELECTEDCATEGORY)?>" onclick="JS_removeCategory1(document.add_account.feed1, false);"><?=system_showText(LANG_BUTTON_REMOVESELECTEDCATEGORY)?></button>
							</center>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
                   </table>
                <? } ?>     



                  
                      <div>
                        <input type="checkbox" class="checkbox" name="agree_tou" value="1" <?=($agree_tou) ? "checked" : ""?> />
                        <label><a rel="nofollow" href="<?=DEFAULT_URL?>/popup/popup.php?pop_type=terms" class="fancy_window_iframe"><?=system_showText(LANG_IGREETERMS)?></a></label>
                    </div>

                    <br class="clear" />
                    <p class="standardButton buttoncenter">
                        <button type="submit" value="Submit" onclick="JS_submit()"><?=system_showText(LANG_BUTTON_SUBMIT)?></button>
                    </p>
                </form>
<?php // print_r($_POST); exit;?>
            </div>

            <? if (FACEBOOK_APP_ENABLED == "on" || $openIDEnabled == "on" || $googleEnabled == "on") { ?>

                <div class="divider"></div>

                <div class="cont_40">

                    <? if (FACEBOOK_APP_ENABLED == "on" || $googleEnabled == "on") { ?>
                        <div>
                            <p>
                                <? if (FACEBOOK_APP_ENABLED == "on") {
                                    $summaryForm = true;
                                    $urlRedirect = "?destiny=".urlencode(DEFAULT_URL."/".SOCIALNETWORK_FEATURE_NAME."/");
                                    include(INCLUDES_DIR."/forms/form_facebooklogin.php");
                                } ?>

                                <? if ($googleEnabled == "on") {
                                    $summaryForm = true;
                                    $urlRedirect = "&destiny=".urlencode(DEFAULT_URL."/".SOCIALNETWORK_FEATURE_NAME."/");
                                    include(INCLUDES_DIR."/forms/form_googlelogin.php");
                                } ?>
                            </p>
                        </div>
                    <? } ?>

                    <? if ($openIDEnabled) { ?>
                        <div>
                            <? if (FACEBOOK_APP_ENABLED == "on" || $googleEnabled == "on") { ?>
                                <p><?=system_showText(LANG_LABEL_PROFILE_OPENID);?></p>
                            <? } ?>


                             <form class="form" name="formOpenID" method="post" action="<?=$formloginaction;?>">

                                <input type="hidden" name="userform" value="openid" />
                                                            <p class="standardButton btn-openid">
                                    <? 
                                    $summaryForm = true;
                                    include(INCLUDES_DIR."/forms/form_openidlogin.php");
                                    ?>
                                                            </p>
                            </form>

                        </div>
                    <? } ?>

                </div>

            <? } ?>

        </div>
