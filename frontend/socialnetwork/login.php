<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /frontend/socialnetwork/login.php
	# ----------------------------------------------------------------------------------------------------
   
?>

    <div class="content-full">

        <div class="content">
                   
            <div class="content-main">
                
                <h2><?=system_showText(LANG_LABEL_LOGIN);?></h2>
		
                <? if ($foreignaccount_openid || $foreignaccount_google || FACEBOOK_APP_ENABLED == "on") { ?>
                <select onchange="changeForm<?=$randomId?>(this.value)" class="select">

                    <option value="edirectory"><?=system_showText(LANG_LOGINDIRECTORYUSER);?></option>

                    <? if ($foreignaccount_openid) { ?>
                    <option value="openid"><?=system_showText(LANG_LOGINOPENIDUSER);?></option>
                    <? } ?>

                    <? if (FACEBOOK_APP_ENABLED == "on") { ?>
                    <option value="facebook"><?=system_showText(LANG_LOGINFACEBOOKUSER);?></option>
                    <? } ?>

                    <? if ($foreignaccount_google) { ?>
                    <option value="google"><?=system_showText(LANG_LOGINGOOGLEUSER);?></option>
                    <? } ?>

                </select>
                <? } ?>

                <div id="popLEdirectory<?=$randomId?>" class="isVisible">
                    <form class="form" name="login" method="post" action="<?=((SSL_ENABLED == "on" && FORCE_MEMBERS_SSL == "on") ? SECURE_URL : DEFAULT_URL)?><?=$url?>">
                        <? 
                        $members_section = true;
                        include(INCLUDES_DIR."/forms/form_login.php"); ?>

                        <? if (system_checkEmail(SYSTEM_FORGOTTEN_PASS)) { ?>
                            <p class="forgotpassword"><a href="<?=((SSL_ENABLED == "on" && FORCE_MEMBERS_SSL == "on") ? SECURE_URL : NON_SECURE_URL)?>/<?=MEMBERS_ALIAS?>/forgot.php"><?=system_showText(LANG_LABEL_FORGOT);?></a></p>
                        <? } ?>
                    </form>
                </div>

                <? if ($foreignaccount_openid) { ?>
                    <div id="popLOpenid<?=$randomId?>" class="isHidden">
                        <form class="form" name="formOpenID" method="post" action="<?=((SSL_ENABLED == "on" && FORCE_MEMBERS_SSL == "on") ? SECURE_URL : DEFAULT_URL)?><?=$url?>">

                            <input type="hidden" name="userform" value="openid" />
                            <? include(INCLUDES_DIR."/forms/form_openidlogin.php"); ?>

                        </form>
                    </div>
                <? } ?>

                <? if (FACEBOOK_APP_ENABLED == "on") { ?>
                    <div id="popLFacebook<?=$randomId?>" class="isHidden">
                        <? 
                        $urlRedirect = "?destiny=".urlencode($_SERVER["HTTP_REFERER"]);
                        include(INCLUDES_DIR."/forms/form_facebooklogin.php");
                        ?>
                    </div>
                <? } ?>

                <? if ($foreignaccount_google) { ?>
                    <div id="popLGoogle<?=$randomId?>" class="isHidden">
                        <?
                        $urlRedirect = "&destiny=".urlencode($_SERVER["HTTP_REFERER"]);
                        include(INCLUDES_DIR."/forms/form_googlelogin.php");
                        ?>
                    </div>
                <? } ?>
                
                <? if (SOCIALNETWORK_FEATURE == "on") { ?>
                    <p class="notmember"><a href="<?=SOCIALNETWORK_URL?>/add.php"><?=system_showText(LANG_LABEL_SIGNUPNOW);?></a></p>
                <? } else { ?>
                    <p class="notmember"><a  href="<?=NON_SECURE_URL?>/<?=ALIAS_ADVERTISE_URL_DIVISOR?>.php"><?=system_showText(LANG_LABEL_SIGNUPNOW);?></a></p>
                <? } ?>
                
            </div>
            
        </div>
	
    </div>