<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /profile/login.php
	# ----------------------------------------------------------------------------------------------------

	# ----------------------------------------------------------------------------------------------------
	# LOAD CONFIG
	# ----------------------------------------------------------------------------------------------------

	include("../conf/loadconfig.inc.php");

	# ----------------------------------------------------------------------------------------------------
	# MAINTENANCE MODE
	# ----------------------------------------------------------------------------------------------------
	verify_maintenanceMode();
    
    # ----------------------------------------------------------------------------------------------------
	# VALIDATION
	# ----------------------------------------------------------------------------------------------------  
	include(EDIRECTORY_ROOT."/includes/code/validate_querystring.php");

	include(EDIRECTORY_ROOT."/includes/code/profile_login.php");
    
    # ----------------------------------------------------------------------------------------------------
	# HEADER
	# ----------------------------------------------------------------------------------------------------
	$hide_search = true;
	include(system_getFrontendPath("header.php", "layout"));
    
    # ----------------------------------------------------------------------------------------------------
	# AUX
	# ----------------------------------------------------------------------------------------------------
	require(EDIRECTORY_ROOT."/frontend/checkregbin.php");
    
?>

    <script type="text/javascript">

        function changeForm<?=$randomId?>(value) {
            if (value == "edirectory") {
                $("#popLEdirectory<?=$randomId?>").addClass('isVisible');
                $("#popLEdirectory<?=$randomId?>").removeClass('isHidden');

                $("#popLOpenid<?=$randomId?>").addClass('isHidden');
                $("#popLOpenid<?=$randomId?>").removeClass('isVisible');

                $("#popLFacebook<?=$randomId?>").addClass('isHidden');
                $("#popLFacebook<?=$randomId?>").removeClass('isVisible');

                $("#popLGoogle<?=$randomId?>").addClass('isHidden');
                $("#popLGoogle<?=$randomId?>").removeClass('isVisible');
            } else if (value == "openid") {
                $("#popLEdirectory<?=$randomId?>").addClass('isHidden');
                $("#popLEdirectory<?=$randomId?>").removeClass('isVisible');

                $("#popLOpenid<?=$randomId?>").addClass('isVisible');
                $("#popLOpenid<?=$randomId?>").removeClass('isHidden');

                $("#popLFacebook<?=$randomId?>").addClass('isHidden');
                $("#popLFacebook<?=$randomId?>").removeClass('isVisible');

                $("#popLGoogle<?=$randomId?>").addClass('isHidden');
                $("#popLGoogle<?=$randomId?>").removeClass('isVisible');
            } else if (value == "facebook") {
                $("#popLEdirectory<?=$randomId?>").addClass('isHidden');
                $("#popLEdirectory<?=$randomId?>").removeClass('isVisible');

                $("#popLOpenid<?=$randomId?>").addClass('isHidden');
                $("#popLOpenid<?=$randomId?>").removeClass('isVisible');

                $("#popLFacebook<?=$randomId?>").addClass('isVisible');
                $("#popLFacebook<?=$randomId?>").removeClass('isHidden');

                $("#popLGoogle<?=$randomId?>").addClass('isHidden');
                $("#popLGoogle<?=$randomId?>").removeClass('isVisible');
            } else if (value == "google") {
                $("#popLEdirectory<?=$randomId?>").addClass('isHidden');
                $("#popLEdirectory<?=$randomId?>").removeClass('isVisible');

                $("#popLOpenid<?=$randomId?>").addClass('isHidden');
                $("#popLOpenid<?=$randomId?>").removeClass('isVisible');

                $("#popLFacebook<?=$randomId?>").addClass('isHidden');
                $("#popLFacebook<?=$randomId?>").removeClass('isVisible');

                $("#popLGoogle<?=$randomId?>").addClass('isVisible');
                $("#popLGoogle<?=$randomId?>").removeClass('isHidden');
            }
        }

    </script>
    
    <? 
    
    include(system_getFrontendPath("login.php", "frontend/socialnetwork"));
    
    # ----------------------------------------------------------------------------------------------------
	# FOOTER
	# ----------------------------------------------------------------------------------------------------
	include(system_getFrontendPath("footer.php", "layout"));

?>