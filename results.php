<?

/* ==================================================================*\
  ######################################################################
  #                                                                    #
  # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
  #                                                                    #
  # This file may not be redistributed in whole or part.               #
  # eDirectory is licensed on a per-domain basis.                      #
  #                                                                    #
  # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
  #                                                                    #
  # http://www.edirectory.com | http://www.edirectory.com/license.html #
  ######################################################################
  \*================================================================== */

# ----------------------------------------------------------------------------------------------------
# * FILE: /results.php
# ----------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------
# LOAD CONFIG
# ----------------------------------------------------------------------------------------------------
include("./conf/loadconfig.inc.php");

if (!THEME_GENERAL_RESULTS) {
    front_errorPage();
}

# ----------------------------------------------------------------------------------------------------
# CACHE
# ----------------------------------------------------------------------------------------------------
cachefull_header();

# ----------------------------------------------------------------------------------------------------
# SESSION
# ----------------------------------------------------------------------------------------------------
sess_validateSessionFront();

# ----------------------------------------------------------------------------------------------------
# MAINTENANCE MODE
# ----------------------------------------------------------------------------------------------------
verify_maintenanceMode();

# ----------------------------------------------------------------------------------------------------
# VALIDATION
# ----------------------------------------------------------------------------------------------------
include(EDIRECTORY_ROOT . "/includes/code/validate_querystring.php");
include(EDIRECTORY_ROOT . "/includes/code/validate_frontrequest.php");

# ----------------------------------------------------------------------------------------------------
# SITE CONTENT
# ----------------------------------------------------------------------------------------------------
$sitecontentSection = "Directory Results";
$array_HeaderContent = front_getSiteContent($sitecontentSection);
extract($array_HeaderContent);

# ----------------------------------------------------------------------------------------------------
# RESULTS
# ----------------------------------------------------------------------------------------------------

$search_lock = false;
$search_lock_word = false;
//    echo string_strlen($_GET["where"]);exit;
//    print_r($_GET);exit;
 
if (LISTING_SCALABILITY_OPTIMIZATION == "on") {
    if (!$_GET["keyword"] && !$_GET["where"] && !$_GET["category_id"] && !$_GET["zip"] && !$_GET["id"]) {
        $_GET["id"] = 0;
        $search_lock = true;
        $search_lock_word = true;
    } else {
        if ($_GET["keyword"] && string_strlen($_GET["keyword"]) < (int) FT_MIN_WORD_LEN && !$_GET["where"]) {
            $_GET["id"] = 0;
            $search_lock = true;
            $search_lock_word = true;
        } else if ($_GET["keyword"] && !$_GET["where"]) {
            $aux = explode(" ", $_GET["keyword"]);
            $search_lock = true;
            $search_lock_word = true;
            for ($i = 0; $i < count($aux); $i++) {
                if (string_strlen($aux[$i]) >= (int) FT_MIN_WORD_LEN) {
                    $search_lock = false;
                    $search_lock_word = false;
                }
            }
            if ($search_lock) {
                $_GET["id"] = 0;
            }
        }

        if ($_GET["where"] && string_strlen($_GET["where"]) < (int) FT_MIN_WORD_LEN && !$_GET["keyword"]) {
            $_GET["id"] = 0;
            $search_lock = true;
            $search_lock_word = true;
        } else if ($_GET["where"] && !$_GET["keyword"]) {
            $aux = explode(" ", $_GET["where"]);
            $search_lock = true;
            $search_lock_word = true;
            for ($i = 0; $i < count($aux); $i++) {
                if (string_strlen($aux[$i]) >= (int) FT_MIN_WORD_LEN) {
                    $search_lock = false;
                    $search_lock_word = false;
                }
            }
            if ($search_lock) {
                $_GET["id"] = 0;
            }
        }

        if ($_GET["keyword"] && string_strlen($_GET["keyword"]) < (int) FT_MIN_WORD_LEN && $_GET["where"] && string_strlen($_GET["where"]) < (int) FT_MIN_WORD_LEN) {
            $_GET["id"] = 0;
            $search_lock = true;
            $search_lock_word = true;
        }
    }
}

// replacing useless spaces in search by "where"
if ($_GET["where"]) {
                    if(SELECTED_DOMAIN_ID==3){
                     $diff=explode(',',$_GET['where']);
                     $_SESSION['s_city']=str_replace('+',' ',$diff['0']);
		     $_SESSION['s_state']=str_replace('+',' ',$diff['1']);
		    $_SESSION['s_country']=str_replace('+',' ',$diff['2']);
                    $_SESSION['s_state']=trim($_SESSION['s_state']);}
    while (string_strpos($_GET["where"], "  ") !== false) {
        str_replace("  ", " ", $_GET["where"]);
    }
    if ((string_strpos($_GET["where"], ",") !== false) && (string_strpos($_GET["where"], ", ") === false)) {
        str_replace(",", ", ", $_GET["where"]);
    }
}

unset($searchReturn);
if (!$search_lock) {
  //    print_r($_GET);exit;
//   include(EDIRECTORY_ROOT . "/gtst2.php");
   if($_GET["where"]&& !$_GET["zip"]){
    $location=  explode(',', $_GET["where"]);
    $country=trim($location[2]);
    $state=trim($location[1]);
    $city=trim($location[0]);
   
    if($country){
            $ctda = "select id from " . _DIRECTORYDB_NAME . ".Location_1  where name='" .$country . "'";
         
            $country_id = @mysql_result(mysql_query($ctda), 0);
          
           }
      if($state){
            $ctda = "select id from " . _DIRECTORYDB_NAME . ".Location_3  where name='" . $state . "'and location_1=".$country_id;
        
            $state_id = @mysql_result(mysql_query($ctda), 0);
           
             }
            if($city){
           $ctda = "select id from " . _DIRECTORYDB_NAME . ".Location_4  where name='" .$city . "' and location_3=".$state_id ." and location_1=".$country_id;;
     
           
           $city_id = @mysql_result(mysql_query($ctda), 0);
         
            }
            if ($city_id && $state_id) {
            $dataa="select abbreviation from "._DIRECTORYDB_NAME.".Location_3 where id=".$state_id."";
	    $st=@mysql_result(mysql_query($dataa),0);
           
            $sqll = "SELECT Zipcode FROM `zip` WHERE State = '" . $st . "' AND City = '" . $city . "' order by Zipcode desc";
         //echo $sqll;exit;
            $resultzip = mysql_query($sqll);
            $row = mysql_fetch_assoc($resultzip);
            $_GET["zip"]=$row['Zipcode'];
       
                    }} 
                    if(SELECTED_DOMAIN_ID==3){
                         if(! $_GET["dist"] ){
                      $_GET["dist"] = "50";
                    }  
                    }else{
                 $_GET["dist"] = "50";        
                    }
//    if(! $_GET["dist"] ){
   
//    }
    
    $freelevel = getfreelistinlevel();
    $searchReturn = search_frontListingSearch($_GET, "listing_results");
    $searchReturn["where_clause"] = $searchReturn["where_clause"] . " AND Listing_Summary.level<>" . $freelevel;
    if (SELECTED_DOMAIN_ID == 1) {
        $searchReturn["order_by"] = "fbcount DESC";
    }
     if (SELECTED_DOMAIN_ID == 2) {
      zipproximity_getWhereZipCodeProximity($_GET["zip"],$_GET["dist"], $whereZipCodeProximity, $order_by_zipcode_score);
      $output=str_replace("AS zipcode_score", "", $order_by_zipcode_score);
//      print_r($output);exit;
 $searchReturn["order_by"] = $output;
      
     }
//    print_r($searchReturn["where_clause"]);exit;
    $aux_items_per_page = ($_COOKIE["listing_results_per_page"] ? $_COOKIE["listing_results_per_page"] : 10);
    $pageObj = new pageBrowsing($searchReturn["from_tables"], $screen, $aux_items_per_page, $searchReturn["order_by"], "Listing_Summary.title", $letter, $searchReturn["where_clause"], $searchReturn["select_columns"], "Listing_Summary", $searchReturn["group_by"]);
//    $listingsarray = $pageObj->retrievePage("array", $searchReturn["total_listings"]);
//    if (SELECTED_DOMAIN_ID == 1) {
//if(isset($listingsarray) && $listingsarray != null){
//       foreach ($listingsarray as $listing){
//            $url = "http://" . $_SERVER['HTTP_HOST'] . "/listing/" . $listing['friendly_url'] . ".html";
//
//            include(EDIRECTORY_ROOT . "/fb_share.php");
//            $data2 = mysql_query("UPDATE Listing_Summary  SET fbcount=(" . $fbcount . ")  where friendly_url = ('" . $listing['friendly_url'] . "')");
//        
//            //echo "UPDATE Listing_Summary  SET fbcount=(" . $fbcount . ")  where friendly_url = ('" . $listing['friendly_url'] . "')";
//       }
//}
//    }
    $listings = $pageObj->retrievePage("array", $searchReturn["total_listings"]);


 $get_requests = $_GET;

    if (!($_GET['id'] || $_GET['keyword'])) {
        
        if ($_GET['letter']) {

            $get_requests['letter'] = $letterb = $_GET['letterb'];
        }
    }

    $bronzeSearchReturn = search_frontListingSearch($get_requests, "listing_results");
   
    $bronzeSearchReturn["where_clause"] = $bronzeSearchReturn["where_clause"] . " AND Listing_Summary.level=" . $freelevel;
   $bronzePageObj = new pageBrowsing($bronzeSearchReturn["from_tables"], (MODREWRITE_FEATURE == "on" && $get_requests["url_full"] ? $pageb : $screen), 30, $bronzeSearchReturn["order_by"], "Listing_Summary.title", $letterb, $bronzeSearchReturn["where_clause"], $bronzeSearchReturn["select_columns"], "Listing_Summary", $bronzeSearchReturn["group_by"]);
   $bronze_listings = $bronzePageObj->retrievePage("array", $bronzeSearchReturn["total_listings"]);


    /*
     * Will be used on:
     * /frontend/results_info.php
     * /frontend/results_filter.php
     * /frontend/results_maps.php
     * functions/script_funct.php
     */
    $aux_module_per_page = "listing";
    $aux_module_items = $listings;
    $aux_module_itemRSSSection = "listing";

    $levelsWithReview = system_retrieveLevelsWithInfoEnabled("has_review");

    $array_search_params = array();
    $array_search_params_map = array();

    $paging_url = DEFAULT_URL . "/results.php";
    foreach ($_GET as $name => $value) {
        if ($name != "screen" && $name != "letter") {
            if ($name == "keyword" || $name == "where")
                $array_search_params[] = $name . "=" . urlencode($value);
            else
                $array_search_params[] = $name . "=" . $value;
        } else {
            $array_search_params_map[] = $name . "=" . $value;
        }
    }
    $url_search_params = implode("&amp;", $array_search_params);
    $url_search_params_map = implode("&amp;", $array_search_params_map);

    /*
     * Preparing Pagination
     */
    unset($letters_menu);
    $letters_menu = system_prepareLetterToPagination($pageObj, $searchReturn, $paging_url, $url_search_params, $letter, "title", false, false, false, LISTING_SCALABILITY_OPTIMIZATION);

    $array_pages_code = system_preparePagination($paging_url, $url_search_params, $pageObj, $letter, $screen, $aux_items_per_page, true);
    if (!($_GET['id'] || $_GET['keyword'])) {
        unset($letters_menu_bronze);
        $letters_menu_bronze = system_prepareLetterToPagination($bronzePageObj, $bronzeSearchReturn, $paging_url, $url_search_params, $letterb, "title", false, false, false, LISTING_SCALABILITY_OPTIMIZATION, 'letterb', '#content-main-bronze');

       $array_pages_code_bronze = system_preparePagination($paging_url, $url_search_params, $bronzePageObj, $letterb, (MODREWRITE_FEATURE == "on" && $_GET["url_full"] ? $pageb : $screen), 30, (MODREWRITE_FEATURE == "on" && $_GET["url_full"] ? false : true), 'letterb', 'pageb', '#content-main-bronze');
    }

    $user = true;
    $showLetterBronze = true;
    $showLetter = true;
    setting_get('commenting_edir', $commenting_edir);
    setting_get("review_listing_enabled", $review_enabled);
    $db = db_getDBObject();
    $sql = "SELECT count(*) as nunberOfReviews FROM Review WHERE item_type = 'listing'";
    $result = $db->query($sql);
    $result = mysql_fetch_assoc($result);
    $numberOfReviews = $result['nunberOfReviews'];

    if ($review_enabled && $commenting_edir && $numberOfReviews) {
        $orderBy = array(LANG_PAGING_ORDERBYPAGE_ALPHABETICALLY, LANG_PAGING_ORDERBYPAGE_LASTUPDATE, LANG_PAGING_ORDERBYPAGE_DATECREATED, LANG_PAGING_ORDERBYPAGE_POPULAR, LANG_PAGING_ORDERBYPAGE_RATING);
    } else {
        $orderBy = array(LANG_PAGING_ORDERBYPAGE_ALPHABETICALLY, LANG_PAGING_ORDERBYPAGE_LASTUPDATE, LANG_PAGING_ORDERBYPAGE_DATECREATED, LANG_PAGING_ORDERBYPAGE_POPULAR);
    }

    if (LISTING_ORDERBY_PRICE) {
        array_unshift($orderBy, LANG_PAGING_ORDERBYPAGE_PRICE);
    }

    $orderbyDropDown = search_getOrderbyDropDown($_GET, $paging_url, $orderBy, system_showText(LANG_PAGING_ORDERBYPAGE) . " ", "this.form.submit();", $parts);
}
# --------------------------------------------------------------------------------------------------------------
//Prepare tab "Map view"
$listViewURL = $paging_url;
$listViewURL .= ($url_search_params ? "?" . str_replace("openMap=1", "", $url_search_params) : "") . ($url_search_params_map ? "&" . $url_search_params_map : "");

setting_get("gmaps_max_markers", $maxMarkers);
$maxMarkers = ($maxMarkers ? $maxMarkers : GOOGLE_MAPS_MAX_MARKERS);

$hideTabMap = false;

if (($array_pages_code["total"] > $maxMarkers) || ($array_pages_code["total"] == 0)) {
    $hideTabMap = true;
    unset($openMap);
    unset($_GET["openMap"]);
}

# ----------------------------------------------------------------------------------------------------
# HEADER
# ----------------------------------------------------------------------------------------------------
$headertag_title = $headertagtitle;
$headertag_description = $headertagdescription;
$headertag_keywords = $headertagkeywords;
$activeMenuHome = true;
include(system_getFrontendPath("header.php", "layout"));

# ----------------------------------------------------------------------------------------------------
# AUX
# ----------------------------------------------------------------------------------------------------
require(EDIRECTORY_ROOT . "/frontend/checkregbin.php");
$showLetter = true;
if (!$listings && !$letter)
    $showLetter = false;

# ----------------------------------------------------------------------------------------------------
# BODY
# ----------------------------------------------------------------------------------------------------

include(THEMEFILE_DIR . "/" . EDIR_THEME . "/body/results.php");

# ----------------------------------------------------------------------------------------------------
# FOOTER
# ----------------------------------------------------------------------------------------------------
include(system_getFrontendPath("footer.php", "layout"));

# ----------------------------------------------------------------------------------------------------
# CACHE
# ----------------------------------------------------------------------------------------------------
cachefull_footer();
?>
