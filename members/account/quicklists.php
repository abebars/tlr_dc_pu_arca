<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /members/account/quicklists.php
	# ----------------------------------------------------------------------------------------------------

	# ----------------------------------------------------------------------------------------------------
	# LOAD CONFIG
	# ----------------------------------------------------------------------------------------------------
	include("../../conf/loadconfig.inc.php");

	# ----------------------------------------------------------------------------------------------------
	# SESSION
	# ----------------------------------------------------------------------------------------------------
	sess_validateSession();

	# ----------------------------------------------------------------------------------------------------
	# AUX
	# ----------------------------------------------------------------------------------------------------
	extract($_GET);
	extract($_POST);
	$levelObj = new ListingLevel();
	setting_get('commenting_edir', $commenting_edir);
	setting_get('review_listing_enabled', $review_enabled);
	setting_get('review_article_enabled', $review_article_enabled);
	setting_get('review_promotion_enabled', $review_promotion_enabled);

	// required because of the cookie var
	$username = "";

	// Default CSS class for message box
	$message_style = "errorMessage";

	# ----------------------------------------------------------------------------------------------------
	# HEADER
	# ----------------------------------------------------------------------------------------------------
	include(MEMBERS_EDIRECTORY_ROOT."/layout/header.php");

	# ----------------------------------------------------------------------------------------------------
	# NAVBAR
	# ----------------------------------------------------------------------------------------------------
	include(MEMBERS_EDIRECTORY_ROOT."/layout/navbar.php");

?>

	<div class="content">

		<? require(EDIRECTORY_ROOT."/".SITEMGR_ALIAS."/registration.php"); ?>
		<? require(EDIRECTORY_ROOT."/includes/code/checkregistration.php"); ?>
		<? require(EDIRECTORY_ROOT."/frontend/checkregbin.php"); ?>

		<? if (SOCIALNETWORK_FEATURE == "on") { ?>
			<h2><?=system_showText(LANG_LABEL_ACCOUNT_INFORMATION)?></h2>
		<? } else { ?>
			<h2><?=system_showText(LANG_LABEL_ACCOUNT_AND_CONTACT_INFO)?></h2>
		<? } ?>

		<?

		$contentObj = new Content();
		$content = $contentObj->retrieveContentByType("Manage Account");
		if ($content) {
			echo "<blockquote>";
				echo "<div class=\"dynamicContent\">".$content."</div>";
			echo "</blockquote>";
		}
		?>

		<? if($message){ ?>
			<p class="<?=$message_style?>"><?=$message?></p>
		<? } ?>

		<table cellpadding="0" cellspacing="0" border="0" class="standard-table tabsTable">
			<tr>
				<th class="tabsBase">
					<ul class="tabs">
						<? if (($review_enabled == "on" || $review_article_enabled == "on" || $review_promotion_enabled == "on") && $commenting_edir) { ?>
							<li id="tab_3">
								<a href="<?=DEFAULT_URL;?>/<?=MEMBERS_ALIAS?>/account/reviews.php"><?=system_showText(LANG_REVIEW_PLURAL)?></a>
							</li>
						<? } ?>
						<li id="tab_4" class="tabActived">
							<a href="javascript:void(0);"><?=system_showText(LANG_LABEL_QUICKLIST)?></a>
						</li>
                        <? if (PROMOTION_FEATURE == "on" && CUSTOM_PROMOTION_FEATURE == "on" && CUSTOM_HAS_PROMOTION == "on") { ?>
						<li id="tab_5">
							<a href="<?=DEFAULT_URL;?>/<?=MEMBERS_ALIAS?>/account/deals.php"><?=system_showText(LANG_LABEL_ACCOUNT_DEALS)?></a>
						</li>
                        <? } ?>
					</ul>
				</th>
			</tr>
		</table>

		<? $members = true; ?>
		<div class="content-profile">
			<? include(system_getFrontendPath("socialnetwork/user_favorites.php")); ?>
		</div>
		<?
		$contentObj = new Content();
		$content = $contentObj->retrieveContentByType("Manage Account Bottom");
		if ($content) {
			echo "<blockquote>";
				echo "<div class=\"dynamicContent\">".$content."</div>";
			echo "</blockquote>";
		}
		?>

	</div>

<?
	# ----------------------------------------------------------------------------------------------------
	# FOOTER
	# ----------------------------------------------------------------------------------------------------
	include(MEMBERS_EDIRECTORY_ROOT."/layout/footer.php");
?>

<script language="javascript" type="text/javascript">

	function redirect (url) {
		window.location = url;
	}

	$('document').ready(function() {
		$('#results_per_page').removeAttr('disabled');
		$('#results_per_page').change(function(){
			$.cookie('profilefavorites_per_page', $('#results_per_page').val(), {path: '<?=EDIRECTORY_FOLDER?>/'}); 
			$(location).attr('href','<?=$_SERVER["REQUEST_URI"]?>');
		});
	});
</script>