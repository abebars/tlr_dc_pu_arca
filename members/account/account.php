<?


	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /members/account/account.php
	# ----------------------------------------------------------------------------------------------------

	# ----------------------------------------------------------------------------------------------------
	# LOAD CONFIG
	# ----------------------------------------------------------------------------------------------------
	include("../../conf/loadconfig.inc.php");

	# ----------------------------------------------------------------------------------------------------
	# SESSION
	# ----------------------------------------------------------------------------------------------------
	sess_validateSession();

	# ----------------------------------------------------------------------------------------------------
	# CODE
	# ----------------------------------------------------------------------------------------------------
	if (SOCIALNETWORK_FEATURE == "on") {
		include(EDIRECTORY_ROOT."/includes/code/profile.php");
	}
    if (MAIL_APP_FEATURE == "on") {
        arcamailer_checkSubscriber();
    }

	# ----------------------------------------------------------------------------------------------------
	# AUX
	# ----------------------------------------------------------------------------------------------------
	extract($_GET);
	extract($_POST);

	// required because of the cookie var
	$username = "";

	// Default CSS class for message box
	$message_style = "errorMessage";

	# ----------------------------------------------------------------------------------------------------
	# Unlink Facebook
	# ----------------------------------------------------------------------------------------------------
	if (isset($_GET['signoffFacebook'])) {
		$facebookMessage = system_showText(LANG_LABEL_FB_ACT_DISC).'.';

		$accountObj = new Account(sess_getAccountIdFromSession());
		$accountObj->setString("facebook_username", "");
		$accountObj->setString("foreignaccount", "n");
		$accountObj->setString("foreignaccount_done", "n");
		$accountObj->setString("foreignaccount_auth", "");
		$accountObj->Save();


		$profileObj = new Profile(sess_getAccountIdFromSession());
		$profileObj->setString("facebook_uid","");
		$profileObj->setString("usefacebooklocation","0");
		$profileObj->Save();

		$fbpost_checked = '';

	}
	
	$expire = 60*60*24*30*12;
	setcookie("fb_attachOption", "facebook_import", time() + $expire, EDIRECTORY_FOLDER? EDIRECTORY_FOLDER: "/");

	# ----------------------------------------------------------------------------------------------------
	# SUBMIT
	# ----------------------------------------------------------------------------------------------------

	if ($_SERVER['REQUEST_METHOD'] == "POST") {

		if ($_POST["hiddenValue"]) {
			$reviewObj = new Review($_POST["hiddenValue"]);
			$reviewObj->Delete();
			header("Location: ".DEFAULT_URL."/".MEMBERS_ALIAS."/account/account.php?type=".$_POST["type"]."&id=".sess_getAccountIdFromSession()."&scrren=".$_POST["screen"]."");
			exit;
		}

		$validate_demodirectoryDotCom = true;
		if (DEMO_LIVE_MODE) {
			$validate_demodirectoryDotCom = validate_demodirectoryDotCom($_POST["username"], $message_demoDotCom);
		}

		if ($validate_demodirectoryDotCom) {
			if (SOCIALNETWORK_FEATURE == "off") {
				$_POST["publish_contact"] = 'n';
			} else {
				if ($_POST['publish_contact'] == "on") {
					$_POST["publish_contact"] = 'y';
				} else {
					$_POST["publish_contact"] = 'n';
				}
			}
            $_POST['notify_traffic_listing'] = ($_POST['notify_traffic_listing'] ? 'y' : 'n');

			if ((string_strlen($_POST["password"])) || (string_strlen($_POST["retype_password"]))) {
				$validate_membercurrentpassword = validate_memberCurrentPassword($_POST, sess_getAccountIdFromSession(), $message_member);
			} else {
				$validate_membercurrentpassword = true;
			}

			if ($validate_demodirectoryDotCom) {
				if ((string_strlen($_POST["password"])) || (string_strlen($_POST["retype_password"]))) {
					$validate_membercurrentpassword = validate_memberCurrentPassword($_POST, sess_getAccountIdFromSession(), $message_member);
				} else {
					$validate_membercurrentpassword = true;
				}

				$account = new Account($account_id);
				$validate_account = validate_MEMBERS_account($_POST, $message_account, sess_getAccountIdFromSession());
				$validate_contact = validate_form("contact", $_POST, $message_contact);
			}

			if ($validate_demodirectoryDotCom && $validate_membercurrentpassword && $validate_account && $validate_contact && !$message_profile) {
				$account = new Account($account_id);
                $lastNewsletter = $account->getString("newsletter");
				if ($account->getString("foreignaccount") == "y") {
					$account->setString("foreignaccount_done", "y");
					$account->save();
				}
                
                $notifyUser = false;
				if ($_POST["password"]) {
                    $notifyUser = true;
					$account->setString("password", $_POST["password"]);
					$account->updatePassword();
				}
                if ($_POST["username"]) {
                    if ($account->getString("username") != $_POST["username"]) {
                        $notifyUser = true;
                    }
                    $account->setString("username", $_POST["username"]);
                }
				
				$account->setString("notify_traffic_listing", $_POST['notify_traffic_listing']);
				$account->setString("publish_contact", $_POST["publish_contact"]);
                
                if ($_POST["newsletter"]) {
                    $actualNewsletter = "y";
                } else {
                    $actualNewsletter = "n";
                }
                
                $account->setString("newsletter", $actualNewsletter);
                
				$account->Save();

				$contact = new Contact($_POST);
				$contact->Save();
                
                if ($actualNewsletter != $lastNewsletter) {

                    //Subscribe
                    if ($actualNewsletter == "y") {
                      
                        $fields["name"] = $contact->getString("first_name")." ".$contact->getString("last_name");
                        $fields["type"] = "sponsor";
                        $fields["email"] = $contact->getString("email");
                        arcamailer_addSubscriber($fields, $success, $account->getNumber("id"));
                        
                    //Unsubscribe
                    } else {
                        arcamailer_Unsubscribe($contact->getString("email"), $account->getNumber("id"));
                    }
                    
                }

				$accDomain = new Account_Domain($account->getNumber("id"), SELECTED_DOMAIN_ID);
				$accDomain->Save();
				$accDomain->saveOnDomain($account->getNumber("id"), $account, $contact);
                 if (SELECTED_DOMAIN_ID == 3) {
                $ProfileContact = new AccountProfileContact(sess_getAccountIdFromSession());
                $return_categories_nonprofit = $_POST["return_categories_nonprofit"];
                $return_categories_array1 = explode(",", $return_categories_nonprofit);

                $ProfileContact->setCategoriesNonprofit($return_categories_array1, $account->getNumber("id"));
            }
                if (system_checkEmail(SYSTEM_SPONSOR_ACCOUNT_UPDATE) && $_POST["type"] == "tab_2" && $notifyUser) {
                    system_sendPassword(SYSTEM_SPONSOR_ACCOUNT_UPDATE, $_POST['email'], $_POST['username'], $_POST['password'], $_POST['first_name']." ".$_POST['last_name']);
                }

				$message = system_showText(LANG_MSG_ACCOUNT_SUCCESSFULLY_UPDATED);
				$message_style = "successMessage";
			} else {
				$message = "";
				$message_style = "";
			}
		} else {
			$message = "";
			$message_style = "";
		}

	    // removing slashes added if required
	    $_POST = format_magicQuotes($_POST);
	    $_GET  = format_magicQuotes($_GET);

		extract($_GET);
	    extract($_POST);
	}

	# ----------------------------------------------------------------------------------------------------
	# FORMS DEFINES
	# ----------------------------------------------------------------------------------------------------
	if (sess_getAccountIdFromSession()) {
        
		$account = new Account(sess_getAccountIdFromSession());
		$contact = new Contact(sess_getAccountIdFromSession());
        
        if ($_SERVER['REQUEST_METHOD'] != "POST") {
            $account->extract();
            $contact->extract();
        }
	} else {
		header("Location: ".DEFAULT_URL."/".MEMBERS_ALIAS."/index.php");
		exit;
	}

	# ----------------------------------------------------------------------------------------------------
	# HEADER
	# ----------------------------------------------------------------------------------------------------
	include(MEMBERS_EDIRECTORY_ROOT."/layout/header.php");

	# ----------------------------------------------------------------------------------------------------
	# NAVBAR
	# ----------------------------------------------------------------------------------------------------
	include(MEMBERS_EDIRECTORY_ROOT."/layout/navbar.php");

?>

    <div class="content">

		<? if (($account->getString("foreignaccount") == "y") && ($account->getString("foreignaccount_done") == "n")) { ?>
			<p class="warningMessage"><?=system_showText(LANG_MSG_FOREIGNACCOUNTWARNING);?></p>
			<? $type = "tab_2";
        }
        
        require(EDIRECTORY_ROOT."/".SITEMGR_ALIAS."/registration.php");
        require(EDIRECTORY_ROOT."/includes/code/checkregistration.php");
        require(EDIRECTORY_ROOT."/frontend/checkregbin.php");
        
        if (SOCIALNETWORK_FEATURE == "on") { ?>
            
			<h2><?=system_showText(LANG_LABEL_ACCOUNT_INFORMATION)?></h2>
            
		<? } else { ?>
            
			<h2><?=system_showText(LANG_LABEL_ACCOUNT_AND_CONTACT_INFO)?></h2>
            
		<? }
        
		$contentObj = new Content();
		$content = $contentObj->retrieveContentByType("Manage Account");
		if ($content) {
			echo "<blockquote>";
				echo "<div class=\"dynamicContent\">".$content."</div>";
			echo "</blockquote>";
		} 
        
        include(INCLUDES_DIR."/forms/form_members_messages.php");
        
        if (SOCIALNETWORK_FEATURE == "on") { ?>
            <table class="standard-table tabsTable members-table">
                <tr>
                    <th class="tabsBase">
                        <ul class="tabs">

                            <li id="tab_1" class="<?=($type== "tab_1" || !$type)? "tabActived" : ""?>">
                                <a href="<?=DEFAULT_URL;?>/<?=MEMBERS_ALIAS?>/account/account.php?type=tab_1&id=<?=sess_getAccountIdFromSession();?>"><?=system_showText(LANG_LABEL_PERSONAL_PAGE)?></a>
                            </li>

                            <li id="tab_2"  class="<?=$type == "tab_2" || SOCIALNETWORK_FEATURE == "off" ? "tabActived" : ""?>">
                                <a href="<?=DEFAULT_URL;?>/<?=MEMBERS_ALIAS?>/account/account.php?type=tab_2&id=<?=sess_getAccountIdFromSession();?>"><?=system_showText(LANG_LABEL_ACCOUNT_SETTINGS)?></a>
                            </li>
                        </ul>
                    </th>
                </tr>
            </table>
        <? } ?>
            
        <div class="member-form">
            
            <form name="account" id="account" method="post" action="<?=system_getFormAction($_SERVER["PHP_SELF"])?>" enctype="multipart/form-data">
                
                <input type="hidden" name="type" id="type" value="<?=$type ? $type: (SOCIALNETWORK_FEATURE == "on" ? "tab_1" : "tab_2");?>" />
                <input type="hidden" name="account_id" value="<?=$account_id?>" />
                
                <div id="personal_pageinfo" style="<?=($type == 'tab_1' || (!$type && SOCIALNETWORK_FEATURE == "on")) ? '' : 'display:none'?>">
                
                    <? include(INCLUDES_DIR."/forms/form_profile.php"); ?>

                    <div id="form-action">

                        <div class="left "></div>

                        <div class="right">

                            <div id="btnSubmit">
                                <p class="standardButton">
                                    <button type="submit" value="Submit"><?=system_showText(LANG_BUTTON_SUBMIT)?></button>
                                    <button type="reset" value="Cancel" onclick="redirect('<?=DEFAULT_URL?>/<?=MEMBERS_ALIAS?>/');"><?=system_showText(LANG_BUTTON_CANCEL)?></button>
                                </p>
                            </div>

                        </div>

                    </div>

                </div>

                <div id="profile_info" style="<?=($type == 'tab_2' || SOCIALNETWORK_FEATURE == "off") ? '' : 'display:none'?>">
                        
                    <? include(INCLUDES_DIR."/forms/form_account_members.php"); ?>
                    
                    <? include(INCLUDES_DIR."/forms/form_contact_members.php"); ?>

                    <div id="form-action">
                        
                        <div class="left "></div>
                        
                        <div class="right">
                            <p class="standardButton">
                                <button type="submit" onclick="JS_submit()" value="Submit"><?=system_showText(LANG_BUTTON_SUBMIT)?></button>
                                <button type="button" value="Cancel" onclick="redirect('<?=DEFAULT_URL?>/<?=MEMBERS_ALIAS?>/');"><?=system_showText(LANG_BUTTON_CANCEL)?></button>
                            </p>
                        </div>
                    </div>

                </div>
                
            </form>
            
        </div>
 
		<?
		$contentObj = new Content();
		$content = $contentObj->retrieveContentByType("Manage Account Bottom");
		if ($content) {
			echo "<blockquote>";
				echo "<div class=\"dynamicContent\">".$content."</div>";
			echo "</blockquote>";
		}
		?>

	</div>
 
	<script language="javascript" type="text/javascript">

		function redirect (url) {
			window.location = url;
		}

		function showTabs(tab) {
			$('#type').val(tab);
			if (tab == 'tab_1') {
				$('#tab_1').addClass('tabActived');
				$('#tab_2').removeClass('tabActived');
				$('#profile_info').css('display', 'none');
				$('#personal_pageinfo').css('display', 'block');
			} else if (tab == 'tab_2') {
				$('#tab_1').removeClass('tabActived');
				$('#tab_2').addClass('tabActived');
				$('#profile_info').css('display', 'block');
				$('#personal_pageinfo').css('display', 'none');
			}
		}

		function formSubmit(form, module) {
			$('#changePage').val(module);
			form.submit();
		}

	</script>
 
<?
	# ----------------------------------------------------------------------------------------------------
	# FOOTER
	# ----------------------------------------------------------------------------------------------------
	include(MEMBERS_EDIRECTORY_ROOT."/layout/footer.php");
?>