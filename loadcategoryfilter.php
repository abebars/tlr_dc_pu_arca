<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /loadcategoryfilter.php
	# ----------------------------------------------------------------------------------------------------
    
    if (!$filterApi) {
        
	# ----------------------------------------------------------------------------------------------------
	# LOAD CONFIG
	# ----------------------------------------------------------------------------------------------------
	include("./conf/loadconfig.inc.php");

	# ----------------------------------------------------------------------------------------------------
	# VALIDATION
	# ----------------------------------------------------------------------------------------------------
	include(EDIRECTORY_ROOT."/includes/code/validate_querystring.php");
	include(EDIRECTORY_ROOT."/includes/code/validate_frontrequest.php");
       
	# ----------------------------------------------------------------------------------------------------
	# CODE
	# ----------------------------------------------------------------------------------------------------

	header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", FALSE);
	header("Pragma: no-cache");
	header("Content-Type: text/html; charset=".EDIR_CHARSET, TRUE);

	$_POST["prefix"] = system_denyInjections($_POST["prefix"]);
	$_POST["category"] = system_denyInjections($_POST["category"]);
    
    }
      
    //query string without "categories"
    $postCategs = array();
    
    if ($arrayGet) {
        foreach ($arrayGet as $key => $value) {
            $valInfo = explode(",", $value);
            if (get_magic_quotes_gpc()) {
                $valInfo[1] = stripslashes($valInfo[1]);
            }
            if ($valInfo[0] != "categories") {
                if ($valInfo[0] != "url_full" && $valInfo[0] != "category_id") {
                    $postCategs[$valInfo[0]] = $valInfo[1];
                }
            } else {
                $categories = $valInfo[1];
            }
        }
        unset($_POST["arrayGet"]);
    }
    
    //Array with all parameters within the query string, except for "categories"
    $parametersCategories = array();

    foreach ($postCategs as $key => $value) {
        //Removing useless parameters
        if ($key != "screen" && $key != "letter" && $key != "filter_item") {
            $parametersCategories[] = $key."=".$value;
        }
    }
    
    if ($filter_item == LISTING_FEATURE_FOLDER) {

        //Search with all parameters except for "categories"
        $searchReturnCategories = search_frontListingSearch($postCategs, "listing_results");

        //URL to concat filters with all parameters except for "categories"
        $filterLinkCategoriesJS = ($actual_module == "root" ? DEFAULT_URL : LISTING_DEFAULT_URL)."/results.php?".str_replace("'", "\'", implode("&", $parametersCategories));

        //Get categories
        $db = db_getDBObject();
        $sub_sql = "SELECT id FROM ".$searchReturnCategories["from_tables"]." WHERE ".$searchReturnCategories["where_clause"];
        $sql = "SELECT category_id FROM Listing_Category WHERE listing_id IN (".$sub_sql.")  AND category_id != category_root_id";
        $result = $db->query($sql);
    
    } elseif ($filter_item == PROMOTION_FEATURE_FOLDER) {
        
        //Search with all parameters except for "categories"
        $searchReturnCategories = search_frontPromotionSearch($postCategs, "promotion_results");

        //URL to concat filters with all parameters except for "categories"
        $filterLinkCategoriesJS = PROMOTION_DEFAULT_URL."/results.php?".str_replace("'", "\'", implode("&", $parametersCategories));

        //Get categories
        $db = db_getDBObject();
        $sub_sql = "SELECT listing_id FROM ".$searchReturnCategories["from_tables"]." WHERE ".$searchReturnCategories["where_clause"];
        $sql = "SELECT category_id FROM Listing_Category WHERE listing_id IN (".$sub_sql.")  AND category_id != category_root_id";
        $result = $db->query($sql);
        
    } elseif ($filter_item == EVENT_FEATURE_FOLDER || $filter_item == CLASSIFIED_FEATURE_FOLDER || $filter_item == ARTICLE_FEATURE_FOLDER) {
        
        if ($filter_item == EVENT_FEATURE_FOLDER) {
            
            //Search with all parameters except for "categories"
            $searchReturnCategories = search_frontEventSearch($postCategs, "event");
            //URL to concat filters with all parameters except for "categories"
            $filterLinkCategoriesJS = EVENT_DEFAULT_URL."/results.php?".str_replace("'", "\'", implode("&", $parametersCategories));
            
        } elseif ($filter_item == CLASSIFIED_FEATURE_FOLDER) {
            
            //Search with all parameters except for "categories"
            $searchReturnCategories = search_frontClassifiedSearch($postCategs, "classified");
            //URL to concat filters with all parameters except for "categories"
            $filterLinkCategoriesJS = CLASSIFIED_DEFAULT_URL."/results.php?".str_replace("'", "\'", implode("&", $parametersCategories));
            
        } elseif ($filter_item == ARTICLE_FEATURE_FOLDER) {
            
            //Search with all parameters except for "categories"
            $searchReturnCategories = search_frontArticleSearch($postCategs, "article");
            //URL to concat filters with all parameters except for "categories"
            $filterLinkCategoriesJS = ARTICLE_DEFAULT_URL."/results.php?".str_replace("'", "\'", implode("&", $parametersCategories));
            
        }

        //Get categories
        $db = db_getDBObject();
        $sql = "SELECT 
                        cat_1_id,
                        parcat_1_level1_id,
                        parcat_1_level2_id,
                        parcat_1_level3_id,
                        parcat_1_level4_id,
                        cat_2_id, 
                        parcat_2_level1_id,
                        parcat_2_level2_id,
                        parcat_2_level3_id,
                        parcat_2_level4_id,
                        cat_3_id,
                        parcat_3_level1_id,
                        parcat_3_level2_id,
                        parcat_3_level3_id,
                        parcat_3_level4_id,
                        cat_4_id, 
                        parcat_4_level1_id,
                        parcat_4_level2_id,
                        parcat_4_level3_id,
                        parcat_4_level4_id,
                        cat_5_id,
                        parcat_5_level1_id,
                        parcat_5_level2_id,
                        parcat_5_level3_id,
                        parcat_5_level4_id

                FROM ".$searchReturnCategories["from_tables"]." WHERE ".$searchReturnCategories["where_clause"];
        
        $result = $db->query($sql);
        
    }

    $categoryObj = new ListingCategory();
    $arrayCategories = array();
    $arrayTotal = array();
    
    while ($row = mysql_fetch_assoc($result)) {
              
        if ($filter_item == LISTING_FEATURE_FOLDER || $filter_item == PROMOTION_FEATURE_FOLDER) {
            
            if (!in_array($row["category_id"], $arrayCategories)) {
                unset($parentCategories);
                $parentCategories = $categoryObj->getHierarchy($row["category_id"], true);
                $parentCategories = explode(",", $parentCategories);
                foreach ($parentCategories as $catP) {
                    $arrayCategories[] = $catP;
                }
                $arrayCategories[] = $row["category_id"];
            }
            $arrayTotal[$row["category_id"]]++; //total items on father category
        
        } else {
            
            for ($k = 1; $k <= MAX_CATEGORY_ALLOWED; $k++) {
                
                if ($row["cat_{$k}_id"] && !in_array($row["cat_{$k}_id"], $arrayCategories)) {
                    $arrayCategories[] = $row["cat_{$k}_id"];
                }
                
                for ($j = MAX_CATEGORY_ALLOWED - 1; $j >= 1; $j--) {
                    
                    if ($row["parcat_{$k}_level{$j}_id"] && !in_array($row["parcat_{$k}_level{$j}_id"], $arrayCategories)) {
                        $arrayCategories[] = $row["parcat_{$k}_level{$j}_id"];
                    }
                    
                }
                
            }
            
        }

    }
    mysql_free_result($result);

    if (!$filterApi) {
    
        $return = system_buildCategoriesFilter($arrayCategories, $arrayTotal, $categories, $filterLinkCategoriesJS, $postCategs, true, $category_id, $filter_item);

        echo $return;
    
    } else {
        
        $filters[0]["filters"] = system_buildCategoriesFilter($arrayCategories, $arrayTotal, $categories, $filterLinkCategoriesJS, $postCategs, true, $father_category, $filter_item, true);
        
    }

?>