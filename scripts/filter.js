function filter_redirect(obj, filter_name, filter_value, url, doubleOption) {
    var destinyURL;
    var queryOptions = "";
    var i;
   
    if (doubleOption) {
        
        if (filter_name == "rating") {
        
            for (i = 0; i < 5; i++) {
                if ($("#"+filter_name+"_"+i).val()) {
                    if ($("#"+filter_name+"_"+i).is(':checked')) {
                        if (queryOptions) {
                            queryOptions = queryOptions + "-";
                        }
                        queryOptions = queryOptions + $("#"+filter_name+"_"+i).val();
                    }
                }
            }
            
        } else {
            if (filter_name == "filter_price") {
                $('#list_price input:checked').each(function() {

                    if ($(this).is(':checked')) {
                        if (queryOptions) {
                            queryOptions = queryOptions + "-";
                        }
                        queryOptions = queryOptions + this.value;
                    }

                });
            } else if (filter_name == "categories") {
                
                $('#list_category input:checked').each(function() {

                    if ($(this).is(':checked')) {
                        if (queryOptions) {
                            queryOptions = queryOptions + "-";
                        }
                        queryOptions = queryOptions + this.value;
                    }

                });

            }  else if (filter_name == "categories1") {
              
                $('#list_nonprofitcategory input:checked').each(function() {

                    if ($(this).is(':checked')) {
                        if (queryOptions) {
                            queryOptions = queryOptions + "-";
                        }
                        queryOptions = queryOptions + this.value;
                    }

                });

            }else {
            
                $('#list_location input:checked').each(function() {

                    if ($(this).is(':checked')) {
                        if (queryOptions) {
                            queryOptions = queryOptions + "-";
                        }
                        queryOptions = queryOptions + this.value;
                    }

                });
            }
            
        }
       
        if (queryOptions) {
            destinyURL = url + "&" + filter_name + "=" + queryOptions
        } else {
            destinyURL = url;
        }
    } else {
        if (obj.checked) {
            destinyURL = url + "&" + filter_name + "=" + filter_value;
        } else {
            destinyURL = url.replace("&" + filter_name + "=" + filter_value, "");
        }
    }
    if ($("#control_openMap").val() == "true") {
        destinyURL = destinyURL.replace("&openMap=1", "");
        destinyURL = destinyURL + "&openMap=1";
    }

    location.href = destinyURL;
}

function filter_openCateg(prefix, category, category_id, module) {
    if ($("#"+module+"_opencategorytree_id_"+category_id)) {
        filter_loadCategory(prefix, category, category_id, module);
    }
}
function filter_openCateg1(prefix, category, category_id, module) {
    if ($("#"+module+"_opencategorytree_id_"+category_id)) {
        filter_loadCategory_nonprofit(prefix, category, category_id, module);
    }
}
function filter_loadCategory(prefix, category, category_id, module) {
   
    var getInfo = [];
    $('#postCats input:hidden').each(function(index) {
        getInfo[index] = [$(this).attr('name'),$(this).attr('value')];
    });
    
    var parameters = {
        "arrayGet[]"    :   getInfo,
        "prefix"        :   prefix,
        "category"      :   category,
        "category_id"   :   category_id,
        "filter_item"   :   module,
        "actual_module" :   ACTUAL_MODULE_FOLDER
    };
    
    $("#"+prefix+"categorytree_id_"+category_id).css("display", "");
    $("#"+prefix+"categorytree_id_"+category_id).html("<li><label id=\"loadingDots\" style=\"cursor: default;\">"+showText(LANG_JS_LOADCATEGORYTREE)+"</label></li>");

    $.post(DEFAULT_URL + "/loadcategoryfilter.php", parameters, function (ret) {
        if (category_id > 0) {
            $("#"+prefix+"opencategorytree_id_"+category_id).css("display", "none");
            $("#"+prefix+"opencategorytree_title_id_"+category_id).css("display", "none");
            $("#"+prefix+"closecategorytree_id_"+category_id).css("display", "");
            $("#"+prefix+"closecategorytree_title_id_"+category_id).css("display", "");
        }
        $("#"+prefix+"categorytree_id_"+category_id).html(ret);
        $("#"+prefix+"categorytree_id_"+category_id).slideDown("slow");
    });
    
}

///
function filter_loadCategory_nonprofit(prefix, category, category_id, module) {
  
    var getInfo = [];
    $('#postCats1 input:hidden').each(function(index) {
        getInfo[index] = [$(this).attr('name'),$(this).attr('value')];
    });
    
    var parameters = {
        "arrayGet[]"    :   getInfo,
        "prefix"        :   prefix,
        "category"      :   category,
        "category_id"   :   category_id,
        "filter_item"   :   module,
        "actual_module" :   ACTUAL_MODULE_FOLDER
    };
    
    $("#"+prefix+"categorytree_id_"+category_id).css("display", "");
    $("#"+prefix+"categorytree_id_"+category_id).html("<li><label id=\"loadingDots\" style=\"cursor: default;\">"+showText(LANG_JS_LOADCATEGORYTREE)+"</label></li>");

    $.post(DEFAULT_URL + "/loadcategoryfilter_nonprofit.php", parameters, function (ret) {
        if (category_id > 0) {
            $("#"+prefix+"opencategorytree_id_"+category_id).css("display", "none");
            $("#"+prefix+"opencategorytree_title_id_"+category_id).css("display", "none");
            $("#"+prefix+"closecategorytree_id_"+category_id).css("display", "");
            $("#"+prefix+"closecategorytree_title_id_"+category_id).css("display", "");
        }
        $("#"+prefix+"categorytree_id_"+category_id).html(ret);
        $("#"+prefix+"categorytree_id_"+category_id).slideDown("slow");
    });
    
}

///
function filter_closeCategory(prefix, category_id) {
	if (category_id > 0) {
        $("#"+prefix+"closecategorytree_id_"+category_id).css("display", "none");
        $("#"+prefix+"closecategorytree_title_id_"+category_id).css("display", "none");
        $("#"+prefix+"opencategorytree_id_"+category_id).css("display", "");
        $("#"+prefix+"opencategorytree_title_id_"+category_id).css("display", "");
    }

    $("#"+prefix+"categorytree_id_"+category_id).slideUp("slow", function() {
        $("#"+prefix+"categorytree_id_"+category_id).html("");
    });
    
}

function filter_loadLocation(prefix, location_id, location_level, module) {
   
    var getInfo = [];
    $('#postLocations input:hidden').each(function(index) {
        getInfo[index] = [$(this).attr('name'),$(this).attr('value')];
    });
    
    var parameters = {
        "arrayGet[]"                :   getInfo,
        "_location_father_level_id" :   location_id,
        "_location_level"           :   location_level,
        "filter_item"               :   module
    };
    
    $("#"+prefix+"locationtree_id_"+location_id).css("display", "");
    $("#"+prefix+"locationtree_id_"+location_id).html("<li><label id=\"loadingDots\" style=\"cursor: default;\">"+showText(LANG_JS_LOADLOCATIONTREE)+"</label></li>");

    $.post(DEFAULT_URL + "/loadlocationfilter.php", parameters, function (ret) {
        if (location_id > 0) {
            $("#"+prefix+"openlocationtree_id_"+location_id).css("display", "none");
            $("#"+prefix+"openlocationtree_title_id_"+location_id).css("display", "none");
            $("#"+prefix+"closelocationtree_id_"+location_id).css("display", "");
            $("#"+prefix+"closelocationtree_title_id_"+location_id).css("display", "");
        }
        $("#"+prefix+"locationtree_id_"+location_id).html(ret);
        $("#"+prefix+"locationtree_id_"+location_id).slideDown("slow");
    });
    
}

function filter_closeLocation(prefix, location_id) {
	if (location_id > 0) {
        $("#"+prefix+"closelocationtree_id_"+location_id).css("display", "none");
        $("#"+prefix+"closelocationtree_title_id_"+location_id).css("display", "none");
        $("#"+prefix+"openlocationtree_id_"+location_id).css("display", "");
        $("#"+prefix+"openlocationtree_title_id_"+location_id).css("display", "");
    }

    $("#"+prefix+"locationtree_id_"+location_id).slideUp("slow", function() {
        $("#"+prefix+"locationtree_id_"+location_id).html("");
    });
    
}