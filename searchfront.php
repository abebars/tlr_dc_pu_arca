<?
$aux_array_url = explode("/", $_SERVER["REQUEST_URI"]);
//  print_r($aux_array_url);exit;

$searchPos_2 = 2;
$searchPos_3 = 3;
$searchPos_4 = 4;

if (EDIRECTORY_FOLDER) {
    $auxFolder = explode("/", EDIRECTORY_FOLDER);
    $searchPos = count($auxFolder) - 1;
    $searchPos_2 += $searchPos;
    $searchPos_3 += $searchPos;
    $searchPos_4 += $searchPos;
}
 

/* ==================================================================*\
  ######################################################################
  #                                                                    #
  # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
  #                                                                    #
  # This file may not be redistributed in whole or part.               #
  # eDirectory is licensed on a per-domain basis.                      #
  #                                                                    #
  # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
  #                                                                    #
  # http://www.edirectory.com | http://www.edirectory.com/license.html #
  ######################################################################
  \*================================================================== */

# ----------------------------------------------------------------------------------------------------
# * FILE: /searchfront.php
# ----------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------
# AUX
# ----------------------------------------------------------------------------------------------------
require(EDIRECTORY_ROOT . "/frontend/checkregbin.php");

# ----------------------------------------------------------------------------------------------------
# CODE
# ----------------------------------------------------------------------------------------------------

$action = (!THEME_GENERAL_RESULTS ? LISTING_DEFAULT_URL : NON_SECURE_URL) . "/results.php";
$searchByKeywordTip = system_showText(LANG_LABEL_SEARCHKEYWORDTIP);
$autocomplete_keyword_url = AUTOCOMPLETE_KEYWORD_URL . '?module=listing';
$hasAdvancedSearch = false;
$hasWhereSearch = true;
$hasPriceSearch = false;
$hasRatingSearch = false;
setting_get("commenting_edir", $commenting_edir);
if (!defined("ACTUAL_MODULE_FOLDER") && string_strpos($_SERVER["REQUEST_URI"], "/results.php") !== false) {
$hasAdvancedSearch = true;
    
}
if (THEME_ADVSEARCH_HOME && (ACTUAL_MODULE_FOLDER == "" || (!defined("ACTUAL_MODULE_FOLDER") && string_strpos($_SERVER["REQUEST_URI"], "/results.php") !== false))) {

    $action_adv = LISTING_DEFAULT_URL . "/results.php";
    $moduleURL = LISTING_DEFAULT_URL;
    $hasAdvancedSearch = true;
    $advancedSearchItem = "listing";
    $advancedSearchPath = EDIRECTORY_FOLDER . str_replace(NON_SECURE_URL, "", LISTING_DEFAULT_URL);
    setting_get("review_listing_enabled", $review_enabled);
} elseif (ACTUAL_MODULE_FOLDER == LISTING_FEATURE_FOLDER) {

    $searchByKeywordTip = system_showText(LANG_LABEL_SEARCHKEYWORDTIP_LISTING);
    $autocomplete_keyword_url = AUTOCOMPLETE_KEYWORD_URL . '?module=listing';
    $action = LISTING_DEFAULT_URL . "/results.php";
    $action_adv = LISTING_DEFAULT_URL . "/results.php";
    $moduleURL = LISTING_DEFAULT_URL;
    $hasAdvancedSearch = true;
    $advancedSearchItem = "listing";
    $advancedSearchPath = EDIRECTORY_FOLDER . str_replace(NON_SECURE_URL, "", LISTING_DEFAULT_URL);
    setting_get("review_listing_enabled", $review_enabled);
} elseif (ACTUAL_MODULE_FOLDER == PROMOTION_FEATURE_FOLDER) {

    $searchByKeywordTip = system_showText(LANG_LABEL_SEARCHKEYWORDTIP_PROMOTION);
    $autocomplete_keyword_url = AUTOCOMPLETE_KEYWORD_URL . '?module=promotion';
    $action = PROMOTION_DEFAULT_URL . "/results.php";
    $action_adv = PROMOTION_DEFAULT_URL . "/results.php";
    $moduleURL = PROMOTION_DEFAULT_URL;
    $hasAdvancedSearch = true;
    $advancedSearchItem = "promotion";
    $advancedSearchPath = EDIRECTORY_FOLDER . str_replace(NON_SECURE_URL, "", PROMOTION_DEFAULT_URL);
    setting_get("review_promotion_enabled", $review_enabled);
} elseif (ACTUAL_MODULE_FOLDER == EVENT_FEATURE_FOLDER) {

    $searchByKeywordTip = system_showText(LANG_LABEL_SEARCHKEYWORDTIP_EVENT);
    $autocomplete_keyword_url = AUTOCOMPLETE_KEYWORD_URL . '?module=event';
    $action = EVENT_DEFAULT_URL . "/results.php";
    $action_adv = EVENT_DEFAULT_URL . "/results.php";
    $moduleURL = EVENT_DEFAULT_URL;
    $hasAdvancedSearch = true;
    $advancedSearchItem = "event";
    $advancedSearchPath = EDIRECTORY_FOLDER . str_replace(NON_SECURE_URL, "", EVENT_DEFAULT_URL);
} elseif (ACTUAL_MODULE_FOLDER == CLASSIFIED_FEATURE_FOLDER) {

    $searchByKeywordTip = system_showText(LANG_LABEL_SEARCHKEYWORDTIP_CLASSIFIED);
    $autocomplete_keyword_url = AUTOCOMPLETE_KEYWORD_URL . '?module=classified';
    $action = CLASSIFIED_DEFAULT_URL . "/results.php";
    $action_adv = CLASSIFIED_DEFAULT_URL . "/results.php";
    $moduleURL = CLASSIFIED_DEFAULT_URL;
    $hasAdvancedSearch = true;
    $advancedSearchItem = "classified";
    $advancedSearchPath = EDIRECTORY_FOLDER . str_replace(NON_SECURE_URL, "", CLASSIFIED_DEFAULT_URL);
} elseif (ACTUAL_MODULE_FOLDER == ARTICLE_FEATURE_FOLDER) {

    $searchByKeywordTip = system_showText(LANG_LABEL_SEARCHKEYWORDTIP_ARTICLE);
    $autocomplete_keyword_url = AUTOCOMPLETE_KEYWORD_URL . '?module=article';
    $action = ARTICLE_DEFAULT_URL . "/results.php";
    $action_adv = ARTICLE_DEFAULT_URL . "/results.php";
    $hasAdvancedSearch = true;
    $hasWhereSearch = false;
    $advancedSearchItem = "article";
    $advancedSearchPath = EDIRECTORY_FOLDER . str_replace(NON_SECURE_URL, "", ARTICLE_DEFAULT_URL);
    setting_get("review_article_enabled", $review_enabled);
} elseif (ACTUAL_MODULE_FOLDER == BLOG_FEATURE_FOLDER) {
    $searchByKeywordTip = system_showText(LANG_LABEL_SEARCHKEYWORDTIP_POST);
    $autocomplete_keyword_url = AUTOCOMPLETE_KEYWORD_URL . '?module=blog';
    $action = BLOG_DEFAULT_URL . "/results.php";
    $action_adv = BLOG_DEFAULT_URL . "/results.php";
    $hasAdvancedSearch = true;
    $hasWhereSearch = false;
    $advancedSearchItem = "blog";
    $advancedSearchPath = EDIRECTORY_FOLDER . str_replace(NON_SECURE_URL, "", BLOG_DEFAULT_URL);
}

if ($review_enabled == "on" && $commenting_edir) {
    $hasRatingSearch = true;
}

if (THEME_LISTING_PRICE && $advancedSearchItem == "listing") {
    $hasPriceSearch = true;
}

if (!$browsebylocation && !$browsebycategory) {

    /*
     * Social network options
     */
    $useSocialNetworkLocation = false;
    if (sess_getAccountIdFromSession() && !$where) {
        $profileObj = new Profile(sess_getAccountIdFromSession());
        if ($profileObj->getString("location") && $profileObj->getString("usefacebooklocation")) {
            $where = $profileObj->getString("location");
            $useSocialNetworkLocation = true;
        }
    }

    /*
     * GeoIP
     */
    $waitGeoIP = false;
//print_r($autocomplete_keyword_url);
    if (!$useSocialNetworkLocation && !$where && GEOIP_FEATURE == "on" && $advancedSearchItem != "article" && $advancedSearchItem != "blog" && (!$screen || string_strpos($_SERVER["PHP_SELF"], "profile") > 0) && !$letter && (string_strpos($_SERVER["REQUEST_URI"], "results.php") === false)
    ) {

        $waitGeoIP = true;

        $where = system_showText(LANG_LABEL_WAIT_LOCATION);

        $js_fileLoader = system_scriptColectorOnReady('

				$.ajax({
				   type: "GET",
				   url: "' . DEFAULT_URL . '/getGeoIP.php",
				   success: function(msg){
					    $("#where, #where_resp").removeClass("ac_loading");
					    $("#where, #where_resp").removeAttr("disabled");
                                            //alert(msg.split("/")[1])
//                                            $("#zip").val(msg.split("/")[1]);
                                           $("#where, #where_resp").val(msg); 
                                           $("#where, #where_resp").val($("#where, #where_resp").val().replace(/[^\x20-\xFF]/g, ""));
                                         
				   }
				 });

			', $js_fileLoader);
    }
}

if (ACTUAL_MODULE_FOLDER != PROMOTION_FEATURE_FOLDER || (ACTUAL_MODULE_FOLDER == PROMOTION_FEATURE_FOLDER && PROMOTION_SCALABILITY_USE_AUTOCOMPLETE == "on")) {

    $js_fileLoader = system_scriptColectorOnReady("

                        $('#keyword, #keyword_resp').autocomplete(
                            '$autocomplete_keyword_url',
                                    {
                                        delay:1000,
                                        dataType: 'html',
                                        minChars:" . AUTOCOMPLETE_MINCHARS . ",
                                        matchSubset:0,
                                        selectFirst:0,
                                        matchContains:1,
                                        cacheLength:" . AUTOCOMPLETE_MAXITENS . ",
                                        autoFill:false,
                                        maxItemsToShow:" . AUTOCOMPLETE_MAXITENS . ",
                                        max:" . AUTOCOMPLETE_MAXITENS . "
                                    }
                            );

                ", $js_fileLoader);
}

if ($hasWhereSearch) {

    $js_fileLoader = system_scriptColectorOnReady("

            $('#where, #where_resp').autocomplete(
                '" . AUTOCOMPLETE_LOCATION_URL . "',
                    {
                        delay:1000,
                        minChars:" . AUTOCOMPLETE_MINCHARS . ",
                        matchSubset:0,
                        selectFirst:0,
                        matchContains:1,
                        cacheLength:" . AUTOCOMPLETE_MAXITENS . ",
                        autoFill:false,
                        maxItemsToShow:" . AUTOCOMPLETE_MAXITENS . ",
                        max:" . AUTOCOMPLETE_MAXITENS . "
                    }
                );

        ", $js_fileLoader);
}
include(system_getFrontendPath("search.php"));

if (SELECTED_DOMAIN_ID == 1) {
//    print_r($aux_array_url);exit;
    if (empty($aux_array_url[$searchPos_1]) || ($aux_array_url[$searchPos_1] == "index.php")) {
        ?>
        <div id="form-wrapper" style=" background: none repeat scroll 0 0 #3E3F41;
             border: 1px solid #CCCCCC;
             clear: both;
             color: #FFFFFF;
             font-size: 16px;
             height: auto;
             margin-left: 18px;
             padding: 20px 24px 10px;
             position: absolute;
             top: 285px;">
            <div id="formwrap">
                <h1 style="color: rgb(185, 209, 50); font-size: 28px;">Find the Top Local Businesses Ranked by Customer Satisfaction</h1>
                <? $action ?>
                <? $method ?>
                <form method="get" action="http://<?= $_SERVER['HTTP_HOST'] ?>/results.php" style="margin: 0px auto; display: inline;width: 650px;" > 
                    <ul style="display: inline">  
                        <!--<li style="display: inline; float: left;">
                        <? if (true) { ?>	
                                <div class="labeltext">

                                        <label>What?</label>

                                        <div id="advanced_search_category_dropdown">
                                         <script>
                                             $( document ).ready(function() {
                                                 showAdvancedSearch('<? //=$advancedSearchItem    ?>', '<? //=$action    ?>', '<? //=$action_adv    ?>','<? //=$aux_template_id    ?>', true);
                                               });
                                         </script>
                                        </div>

                                </div>

                        <? } ?>	


                        </li>-->


                        <?php ?><li style="  display: table-cell;float: left;margin-left: 111px;">
                            What?  <input type="text" placeholder="Category"  style=" margin-right: 29px;width: 230px" name="keyword" id="keyword" value="<?= $keyword ?>" onkeypress="return submitenter(this, event)" <? if ((MODREWRITE_FEATURE == "on" && !$searchNotFriendly)) echo "onkeyup=\"changeFormAction('" . $action . "', this.value, " . $auxScript . ")\"" ?>  />
                            <?
                            if (string_strpos($_SERVER["PHP_SELF"], PROMOTION_FEATURE_FOLDER) === false || (string_strpos($_SERVER["PHP_SELF"], PROMOTION_FEATURE_FOLDER) !== false && PROMOTION_SCALABILITY_USE_AUTOCOMPLETE == "on")) {
//htmlencde($autocomplete_keyword_url,1);
                                $js_fileLoader = system_scriptColectorOnReady("

                        $('#keyword, #keyword_resp').autocomplete(

                            '$autocomplete_keyword_url',

                                    {
                                        delay:100,

                                        dataType: 'html',

                                        minChars:" . AUTOCOMPLETE_MINCHARS . ",

                                        matchSubset:0,

                                        selectFirst:0,

                                        matchContains:1,

                                        cacheLength:" . AUTOCOMPLETE_MAXITENS . ",

                                        autoFill:false,

                                        maxItemsToShow:" . AUTOCOMPLETE_MAXITENS . ",

                                        max:" . AUTOCOMPLETE_MAXITENS . "

                                    }

                            );



                ", $js_fileLoader);
                            }
                            ?>
                        </li><?php ?>
                        <li style="display: table-cell;">
                            <?php if ($hasWhereSearch) { ?>
                                Where? 
                                <input type="text" style="display: inline;width: 230px; float: inherit;" name="where" id="where" value="" onfocus="emptyzip();"<?= ($waitGeoIP ? "class=\"ac_loading\" disabled=\"disabled\"" : "") ?>  />
<!--                                <input type="hidden"  name="zip" id="zip" value="" />-->
                                    <?
                                $js_fileLoader = system_scriptColectorOnReady("


					$('#where, #where_resp').autocomplete(

						'" . AUTOCOMPLETE_LOCATION_URL . "',

							{

								delay:100,

								minChars:" . AUTOCOMPLETE_MINCHARS . ",

								matchSubset:0,

								selectFirst:0,

								matchContains:1,

								cacheLength:" . AUTOCOMPLETE_MAXITENS . ",

								autoFill:false,

								maxItemsToShow:" . AUTOCOMPLETE_MAXITENS . ",

								max:" . AUTOCOMPLETE_MAXITENS . "

							}

					 );



				", $js_fileLoader);
                            }
                            ?>
                        </li>
                        <!--		<li style="display: inline; float: left; margin-left: 20px">
                                        Miles
                                        <select name="dist">
                                                <option <?php echo ($_SESSION['dist'] == '5') ? 'selected' : ''; ?> value="5">5 Miles</option>
                        <?php if (empty($_SESSION['dist'])) { ?>
                                                                <option  value="20" selected="selected">20 Miles</option>
                        <?php } ?>
                                                <option <?php echo ($_SESSION['dist'] == '20') ? 'selected' : ''; ?> value="20" selected="selected">20 Miles</option>
                                                <option <?php echo ($_SESSION['dist'] == '50') ? 'selected' : ''; ?> value="50">50 Miles</option>
                                                <option <?php echo ($_SESSION['dist'] == '100') ? 'selected' : ''; ?> value="100">100 Miles</option>
                                        </select>
                                        </li>-->
                    </ul>
                    <? if (!$keepFormOpen) { ?>

                        <div class="search_box" style="margin-left: 20px; float: right;">
                            <button style="cursor: pointer; color: rgb(255, 255, 255); border-radius: 3px 3px 3px 3px; padding: 5px; font-weight: bold; background: none repeat scroll 0% 0% rgb(185, 209, 50);" type="submit" id="buttonSearch" ><?= system_showText(LANG_BUTTON_SEARCH); ?></button>
                            <?
                            if ($hasAdvancedSearch) {

                                $aux_template_id = $template_id;
                                ?>

                                <a id="advanced-search-button" href="javascript:void(0);" onclick="showAdvancedSearch('<?= $advancedSearchItem ?>', '<?= $action ?>', '<?= $action_adv ?>', '<?= $aux_template_id ?>', true);">
                                </a>

                            <? } ?>
                        </div>

                    <? } ?>
                    <? if (!$keepFormOpen) { ?>

                        <script type="text/javascript">
            <? if ($_SESSION["s_zip"] == "" && false) { ?>
                                        closeAdvancedSearch('<?= $advancedSearchItem ?>', '<?= $action ?>', '<?= $action_adv ?>', '<?= $aux_template_id ?>');
            <? } else { ?>
                                        showAdvancedSearch('<?= $advancedSearchItem ?>', '<?= $action ?>', '<?= $action_adv ?>', '<?= $aux_template_id ?>', true);
            <? } ?>

                                    /*     function submitformSearch(action){
                                     
                                     var keyword = document.getElementById("keyword").value;
                                     
                                     var where = '';
                                     
                                     
                                     
                                     keyword = keyword.replace(/\//g,"|2F");
                                     
                                     keyword = keyword.replace(/\\/g,"|3F");
                                     
                                     
                                     
                                     if (document.getElementById("where")){
                                     
                                     where = document.getElementById("where").value;
                                     
                                     where = where.replace(/\//g,"|2F");
                                     
                                     where = where.replace(/\\/g,"|3F");
                                     
                                     if (where){
                                     
                                     where = "where/"+urlencode(where);
                                     
                                     } else {
                                     
                                     where = "where/empty";
                                     
                                     }
                                     
                                     } else{
                                     
                                     where = "";
                                     
                                     }
                                     
                                     
                                     
                                     if (keyword){
                                     
                                     where = "/"+where;
                                     
                                     } else {
                                     
                                     keyword = "empty";
                                     
                                     where = "/"+where;
                                     
                                     }
                                     
                                     
                                     
                                     
                                     
            <? if ((MODREWRITE_FEATURE == "on" && !$searchNotFriendly)) { ?>
                                         
                                         window.location = action+urlencode(keyword)+where;
                                         
            <? } else { ?>
                                         
                                         document.search_form.submit();
                                         
            <? } ?>
                                     
                                     } */

                                    $(document).ready(function() {

                                        $('#where').bind('keyup blur', function() {
                                            $(this).val($(this).val().replace(/[^\x20-\xFF]/g, ''));
                                        }
                                        );
                                    });
                                    function showAdvancedSearch(item_type, template_id, load_cat, category_id) {

                                        var aux_data = "category_id=" + category_id + "&fnct=categories&type=" + item_type;

                                        if (load_cat) {
                                            /*
                                             * Load dropdown using ajax
                                             */
                                            if (template_id > 0) {
                                                aux_data += "&template_id=" + template_id;
                                            }

                                            $.ajax({
                                                url: DEFAULT_URL + "/advancedsearch_categories.php",
                                                context: document.body,
                                                data: aux_data,
                                                success: function(html) {
                                                    $("#advanced_search_category_dropdown").html(html);
                                                    if ($().selectpicker) {
                                                        $('.selectpicker .select').selectpicker();
                                                    }
                                                }
                                            });
                                        }

                                        if (document.getElementById("locations_default_where")) {
                                            if (document.getElementById("locations_default_where").value) {
                                                if (document.getElementById("locations_default_where_replace").value == "yes") {
                                                    $("#where, #where_resp").attr("value", $("#locations_default_where").val());
                                                }
                                            }
                                        }

                                        //    document.getElementById("advanced-search-button").onclick = function() {
                                        //		closeAdvancedSearch(item_type, template_id, category_id);
                                        //	}

                                        $('#advanced-search').slideDown('slow');
                                        $('#advanced-search-label').hide();
                                        $('#advanced-search-label-close').show();
                                    }
                                    function closeAdvancedSearch(item_type, template_id, category_id) {

                                        $('#advanced-search').slideUp('slow', function() {
                                            document.getElementById("advanced-search-button").onclick = function() {
                                                showAdvancedSearch(item_type, template_id, false, category_id);
                                            }
                                        });
                                        $('#advanced-search-label').show();
                                        $('#advanced-search-label-close').hide();

                                    }
                                    function submitenter(myfield, e) {
                                        var keycode;

                                        if (window.event) {

                                            keycode = window.event.keyCode;

                                        } else if (e) {

                                            keycode = e.which;

                                        } else {

                                            return true;

                                        }



                                        if (keycode == 13) {

                                            submitformSearch('<?= $action ?>');

                                            return false;

                                        } else
                                            return true;

                                    }

                        </script>
                    <? } ?>	
                </form>
            </div>
        </div>
        <?php
    }
}
?>