<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /classes/class_AccountProfileContact.php
	# ----------------------------------------------------------------------------------------------------

	/**
	 * <code>
	 *		$accountObj = new AccountProfileContact($domain_id, $account_id);
	 * <code>
	 * @copyright Copyright 2005 Arca Solutions, Inc.
	 * @author Arca Solutions, Inc.
	 * @version 8.0.00
	 * @package Classes
	 * @name AccountProfileContact
	 * @method AccountProfileContact
	 * @method makeFromRow
	 * @method Save
	 * @method Delete
	 * @access Public
	 */
	class AccountProfileContact extends Handle {

		/**
		 * @var integer
		 * @access Private
		 */
		var $account_id;
        /**
		 * @var varchar
		 * @access Private
		 */
		var $username;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $first_name;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $last_name;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $nickname;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $friendly_url;
		/**
		 * @var integer
		 * @access Private
		 */
		var $image_id;
		/**
		 * @var varchar
		 * @access Private
		 */
		var $facebook_image;
		/**
		 * @var integer
		 * @access Private
		 */
		var $facebook_image_width;
		/**
		 * @var integer
		 * @access Private
		 */
		var $facebook_image_height;
		/**
		 * @var char
		 * @access Private
		 */
		var $has_profile;
		/**
		 * @var integer
		 * @access Private
		 */
		var $domain_id;
//                var $nonprofit_cate;
//                var $nonprofit_sub_cate;
		/**
		 * <code>
		 *		$accountObj = new AccountProfileContact($domain_id, $account_id);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name AccountProfileContact
		 * @access Public
		 * @param integer $domain_id
		 * @param integer $var
		 */
		function AccountProfileContact($domain_id, $var='') {
			if (is_numeric($var) && ($var)) {
				$this->domain_id = $domain_id;
				$main_db = db_getDBObject(DEFAULT_DB, true);
				$db = db_getDBObjectByDomainID($this->domain_id, $main_db);
				$sql = "SELECT * FROM AccountProfileContact WHERE account_id = $var";
				$row = mysql_fetch_array($db->query($sql));
				$this->makeFromRow($row);
			} else {
                if (!is_array($var)) {
                    $var = array();
                }
				$this->makeFromRow($var);
			}
		}

		/**
		 * <code>
		 *		$this->makeFromRow($row);
		 * <code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name makeFromRow
		 * @access Public
		 * @param array $row
		 */
		function makeFromRow($row='') {
			if ($row['account_id']) { $this->account_id = $row['account_id']; $this->has_account = 1; }
			else if (!$this->account_id) { $this->account_id = 0; $this->has_account = 0; }
            if ($row['username']) $this->username = $row['username'];
			else if (!$this->username) $this->username = "";
			if ($row['first_name']) $this->first_name = $row['first_name'];
			else if (!$this->first_name) $this->first_name = "";
			if ($row['last_name']) $this->last_name = $row['last_name'];
			else if (!$this->last_name) $this->last_name = "";
			if ($row['nickname']) $this->nickname = $row['nickname'];
			else if (!$this->nickname) $this->nickname = "";
			if ($row['friendly_url']) $this->friendly_url = $row['friendly_url'];
			else if (!$this->friendly_url) $this->friendly_url = "";
			if ($row['image_id']) $this->image_id = $row['image_id'];
			else if (!$this->image_id) $this->image_id = 0;
			if ($row['facebook_image']) $this->facebook_image = $row['facebook_image'];
			else if (!$this->facebook_image) $this->facebook_image = "";
			if ($row['facebook_image_width']) $this->facebook_image_width = $row['facebook_image_width'];
			else if (!$this->facebook_image_width) $this->facebook_image_width = 0;
			if ($row['facebook_image_height']) $this->facebook_image_height = $row['facebook_image_height'];
			else if (!$this->facebook_image_height) $this->facebook_image_height = 0;
			if ($row['has_profile']) $this->has_profile = $row['has_profile'];
			else if (!$this->has_profile) $this->has_profile = "n";
//                        if ($row["nonprofit_cate"]) $this->nonprofit_cate = $row["nonprofit_cate"];
//			else if (!$this->nonprofit_cate) $this->nonprofit_cate = 0;
//		
//                        if ($row["nonprofit_sub_cate"]) $this->nonprofit_sub_cate = $row["nonprofit_sub_cate"];
//			else if (!$this->nonprofit_sub_cate) $this->nonprofit_sub_cate = 0;
		}

		/**
		 * <code>
		 *		//Using this in forms or other pages.
		 *		$accountObj->Save();
		 * <br /><br />
		 *		//Using this in AccountProfileContact() class.
		 *		$this->Save();
		 * </code>
		 * @copyright Copyright 2005 Arca Solutions, Inc.
		 * @author Arca Solutions, Inc.
		 * @version 8.0.00
		 * @name Save
		 * @access Public
		 */
		function Save() {
			$this->prepareToSave();

			$main_db = db_getDBObject(DEFAULT_DB, true);
			$dbObj = db_getDBObjectByDomainID($this->domain_id, $main_db);
			if ($this->has_account) {
				$sql  = "UPDATE AccountProfileContact SET"
					. " username = $this->username,"
					. " first_name = $this->first_name,"
					. " last_name = $this->last_name,"
					. " nickname = $this->nickname,"
					. " friendly_url = $this->friendly_url,"
					. " image_id = $this->image_id,"
					. " facebook_image = $this->facebook_image,"
					. " has_profile = $this->has_profile";
//                                         if(SELECTED_DOMAIN_ID==3){
//                                    $sql.= ",nonprofit_cate = $this->nonprofit_cate,"
//					. " nonprofit_sub_cate = $this->nonprofit_sub_cate";
//                                }
					$sql.=" WHERE account_id = $this->account_id";
//                                if(SELECTED_DOMAIN_ID==3){
//                                system_countActiveUserByCategory($this->account_id, "dec");
//                                                           }
				$dbObj->query($sql);
			} else {
//                            if(SELECTED_DOMAIN_ID==3){
//                            $sql = "INSERT INTO AccountProfileContact"
//					. " (account_id, username, first_name, last_name, nickname, friendly_url, image_id, facebook_image, facebook_image_width, facebook_image_height, has_profile,nonprofit_cate,nonprofit_sub_cate)"
//					. " VALUES"
//					. " ($this->account_id, $this->username, $this->first_name, $this->last_name, $this->nickname, $this->friendly_url, $this->image_id, $this->facebook_image, $this->facebook_image_width, $this->facebook_image_height, $this->has_profile, $this->nonprofit_cate, $this->nonprofit_sub_cate)";
//    
//                            }else{
                             $sql = "INSERT INTO AccountProfileContact"
					. " (account_id, username, first_name, last_name, nickname, friendly_url, image_id, facebook_image, facebook_image_width, facebook_image_height, has_profile)"
					. " VALUES"
					. " ($this->account_id, $this->username, $this->first_name, $this->last_name, $this->nickname, $this->friendly_url, $this->image_id, $this->facebook_image, $this->facebook_image_width, $this->facebook_image_height, $this->has_profile)";
   
//                            }
				
				$dbObj->query($sql);
                               
			}
//                        echo $sql;exit;
//                         if(SELECTED_DOMAIN_ID==3){
//                               system_countActiveUserByCategory($this->account_id);
//                                }

			$this->prepareToUse();
		}

		function Delete() {
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if ($this->domain_id) {
				$dbObj = db_getDBObjectByDomainID($this->domain_id, $dbMain);
			} else {
				if (defined("SELECTED_DOMAIN_ID")) {
					$dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
				} else {
					$dbObj = db_getDBObject();
				}
				unset($dbMain);
			}
                        
//                         if(SELECTED_DOMAIN_ID==3){
//                               system_countActiveUserByCategory($this->account_id, "dec");
//                                }
			$sql = "DELETE FROM AccountProfileContact WHERE account_id = $this->account_id";
                       
			$dbObj->query($sql);
		}
                
                
    function setCategoriesNonProfit($array,$account_id) {

        $dbMain = db_getDBObject(DEFAULT_DB, true);
        if (defined("SELECTED_DOMAIN_ID")) {
            $dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
        } else {
            $dbObj = db_getDBObject();
        }
        unset($dbMain);

        if ($account_id) {
            $user_profit="SELECT category_id from NonProfitUser_Category where account_id = " . $account_id;
            $r = $dbObj->query($user_profit);
            while ($row = mysql_fetch_array($r)) {
                    $nonprofit_categories[] = $row['category_id'];
                }								
//           $nonprofit_categories= $this->getNonProfitCategories($have_data, $data, $account_id);
//           print_r($nonprofit_categories[0]['id']);exit;
           $sql = "DELETE FROM NonProfitUser_Category WHERE account_id = " . $account_id;
           $dbObj->query($sql);
//          print_r($nonprofit_categories);exit;
           if ($nonprofit_categories) {
		for ($i=0; $i<count($nonprofit_categories); $i++) {
                    system_countActiveUserByCategory($account_id,$nonprofit_categories[$i]);
			
		}
	             
	} 
          //  system_countActiveUserByCategory($account_id);

         
           
         

            if ($array) {
                foreach ($array as $category) {
                    if ($category) {

                        $lCatObj = new NonProfitListingCategory($category);
                        unset($root_id, $left, $right);
                        $root_id = $lCatObj->getNumber("root_id");
                        $left = $lCatObj->getNumber("left");
                        $right = $lCatObj->getNumber("right");
                        unset($l_catObj);
                        $l_catObj = new NonProfitUser_Category();
                        $l_catObj->setNumber("account_id", $account_id);
                        $l_catObj->setNumber("category_id", $category);
                        $l_catObj->setNumber("category_root_id", $root_id);
                        $l_catObj->setNumber("category_node_left", $left);
                        $l_catObj->setNumber("category_node_right", $right);

                        $l_catObj->Save();
                        system_countActiveUserByCategory($account_id);
                    }
                }
            }

           
            
        }
    }
    
    
    function getNonProfitCategories($have_data = false, $data = false, $id = false, $getAll = false, $object = false, $bulk = false) {

        $dbMain = db_getDBObject(DEFAULT_DB, true);
        if (defined("SELECTED_DOMAIN_ID")) {
            $dbObj = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
        } else {
            $dbObj = db_getDBObject();
        }

        unset($dbMain);

        if ($have_data) {
            if ($data["cat_1_id"])
                $ids[] = $data["cat_1_id"];
            if ($data["cat_2_id"])
                $ids[] = $data["cat_2_id"];
            if ($data["cat_3_id"])
                $ids[] = $data["cat_3_id"];
            if ($data["cat_4_id"])
                $ids[] = $data["cat_4_id"];
            if ($data["cat_5_id"])
                $ids[] = $data["cat_5_id"];

            if (is_array($ids)) {
                $ids = array_unique($ids);
                $sql = "SELECT * FROM NonProfitListingCategory WHERE id IN (" . implode(",", $ids) . ")";
                $r = $dbObj->query($sql);
                while ($row = mysql_fetch_array($r)) {
                    $categories[] = new NonProfitListingCategory($row);
                }
            }
        } else {

            if (!$id) {
                $id = $this->account_id;
            }
            if ($id) {

                $sql_main = "SELECT category.root_id,
										NonProfitUser_Category.category_id
										FROM NonProfitUser_Category NonProfitUser_Category
										INNER JOIN NonProfitListingCategory category ON category.id = NonProfitUser_Category.category_id
										WHERE NonProfitUser_Category.account_id = " . $id . " AND root_id > 0";


               
                $result_main = $dbObj->unbuffered_query($sql_main);

                if ($result_main) {

                    $aux_array_categories = array();
                    while ($row = mysql_fetch_assoc($result_main)) {
                        if (!$object && !$bulk) {
                            $aux_array_categories[] = $row["root_id"];
                        }
                        if ($getAll) {
                            $aux_array_categories[] = $row["category_id"];
                        }
                    }

                    if (count($aux_array_categories) > 0) {

                        $sql = "SELECT	id,
											title,
											page_title,
											friendly_url,
											enabled,
											category_id
										FROM NonProfitListingCategory
										WHERE id IN (" . implode(",", $aux_array_categories) . ")";

                        if (!$object) {
                            $result = $dbObj->unbuffered_query($sql);
                        } else {
                            $result = $dbObj->query($sql);
                        }

                        //if(mysql_num_rows($result) > 0){
                        if ($result) {
                            $categories = array();
                            while ($row = mysql_fetch_assoc($result)) {
                                if ($object) {
                                    $categories[] = new NonProfitListingCategory($row);
                                } else {
                                    $categories[] = $row;
                                }
                            }
                        }
                    }
                }
            }
        }

        if (count($categories) > 0) {
            return $categories;
        } else {
            return false;
        }
    }

	}
?>