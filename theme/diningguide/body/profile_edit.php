<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /theme/diningguide/body/profile_edit.php
	# ----------------------------------------------------------------------------------------------------

?>
	
    <div class="content-center profile-edit">

        <div class="sidebar">
            <? include(system_getFrontendPath("socialnetwork/user_info.php")); ?>
        </div>
    
        <div class="content">
                       
            <? include(system_getFrontendPath("socialnetwork/page_tabs.php")); ?>
            
            <div class="member-form">
                <? include(system_getFrontendPath("socialnetwork/edit_account.php")); ?>
            </div>
            
        </div>
        
	</div>