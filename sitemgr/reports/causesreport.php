<?
	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

    # ----------------------------------------------------------------------------------------------------
    # * FILE: sitemgr/reports/statisticreport.php
    # ----------------------------------------------------------------------------------------------------

    # ----------------------------------------------------------------------------------------------------
    # LOAD CONFIG
    # ----------------------------------------------------------------------------------------------------
    include("../../conf/loadconfig.inc.php");

    # ----------------------------------------------------------------------------------------------------
    # SESSION
    # ----------------------------------------------------------------------------------------------------
    sess_validateSMSession();
	permission_hasSMPerm();

	extract($_POST);
	extract($_GET);

	
    # ----------------------------------------------------------------------------------------------------
    # GET DATABASE
    # ----------------------------------------------------------------------------------------------------
    $db = db_getDBObject();

    # ----------------------------------------------------------------------------------------------------
    # HEADER
    # ----------------------------------------------------------------------------------------------------
    include(SM_EDIRECTORY_ROOT."/layout/header.php");

    # ----------------------------------------------------------------------------------------------------
    # NAVBAR
    # ----------------------------------------------------------------------------------------------------
    include(SM_EDIRECTORY_ROOT."/layout/navbar.php");

?>


<div id="main-right">

    <div id="top-content">
        <div id="header-content">
            <h1><?=string_ucwords(system_showText(LANG_SITEMGR_NAVBAR_CAUSES))?></h1>
        </div>
    </div>

    <br />
   

   
   


    <div id="content-content">
        <div class="default-margin">

        <? require(EDIRECTORY_ROOT."/".SITEMGR_ALIAS."/registration.php"); ?>

        <?
			$dbMain = db_getDBObject(DEFAULT_DB, true);
			if (defined("SELECTED_DOMAIN_ID")) {
				$db = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
			} else {
				$db = db_getDBObject();
			}
$NonProfitsql="select title,id,friendly_url,active_listing_web,active_users from NonProfitListingCategory where enabled='y'  order by title asc";
	
$result=mysql_query($NonProfitsql);            
           

            if($result){ ?>
   <table cellpadding='0' cellspacing='0' class='highlight-report'>
        <tr>
                                        <th class='highlight-title-center'>
                                         Non Profit Subcategory
                                           
                                        </th>
                                        <th class='highlight-title-center'>
# Users                                        </th>
                                        <th class='highlight-title-center'>
# Active Listing                                    </th>
                                    </tr>
<?php
                while($row = mysql_fetch_array($result)) {
     $NonProfitsql1="select count(id) num from NonProfitListingCategory where category_id =".$row['id']." and enabled='y'  order by title asc";
//	echo $NonProfitsql1;
    $result1=mysql_query($NonProfitsql1);  
    $row_counter = mysql_fetch_assoc($result1);
    if($row_counter['num']==0){?>
                 <tr>
                                            <td class='rightColumn center'>

                                                <?=$row['title'];?>

                                               

                                            </td>
                                            <td class='rightColumn center'>
                                                <?=$row['active_users']; ?>
                                            </td>
                                             <td class='rightColumn center'>
                                                <?=$row['active_listing_web']; ?>
                                            </td>
                                        </tr> 
    <?php }  }?>
   </table>  
            <?php }
        ?>

               
       

        </div>
    </div>

    <div id="bottom-content">
        &nbsp;
    </div>

</div>
<?
    # ----------------------------------------------------------------------------------------------------
    # FOOTER
    # ----------------------------------------------------------------------------------------------------
    include(SM_EDIRECTORY_ROOT."/layout/footer.php");
?>